package utils;

import android.app.Application;
import android.os.Environment;
import android.util.Xml;
import android.view.Gravity;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;

import com.demo.devicesreport.ReportData;
import com.hjq.toast.ToastUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class MyApplication extends Application {
    public static final String LOG_OUTPUT_HEADER     = ">>>>>";
    public static final String SERVER_BLUETOOTH_NAME = "HFP-WIN10-PC";

    // Variables
    private static final String Log_Tag     = "MyApplication";
    private AppCrashHandler appCrashHandler = null;

    public static String DB_PATH = null; // 数据库路径
    public static int USER_ID    = -1;   // 当前登录用户ID
    public static int REPORT_ID  = -1;   // 当前用户上报ID(重新跳转回到登录页后重置)

    // TODO.临时解决 设备填报 "下一条"的问题
    public static boolean REPORT_ID_INITIALED = false;

    // 当前用户未上报IDs、数据 Data IDs
    public static List<Integer> UNREPORT_ID_LST       = new ArrayList<Integer>();
    public static List<Integer> UNREPORT_DATA_ID_LST  = new ArrayList<Integer>();

    // TODO.当前用户未上报，且新添加数据 report_id初始化标识
    public static boolean UNREPORT_NEW_DATA_INITIALED = false;
    public static int UNREPORT_OLD_DATA_SIZE = -1;

    // 填报过的 设备id(point_id + device_type_id)
    public static boolean IS_ALL_DEVICE_REPORTED    = false;

    // 填报过保存过的 saved_area_id、saved_device_type_id、point_id
    // 第一层 为 区域 与 设备类型 的一对多集合，Integer 为 area_id
    // 第二层 为 设备类型 与 点位 的一对多集合，Integer 为 device_type_id
    // 最后 List 为 点位 id
    public static Map<Integer, Map<Integer, List<Integer>>> SAVED_IDS = new HashMap<>();

    // 程序文件、日志 路径
    public static String APP_DIR_PATH    = null;
    public static String APP_LOGS_PATH   = null;
    public static String APPLICATION_DIR = null;

    public static String APP_IMAGES_PATH = null;
    public static String APP_VIDEOS_PATH = null;
    public static String APP_DATA_PATH   = null;

    // App状态配置文件
    private File APP_STATUS_CONFIG_FILE = null;

    // 数据
    public static List<ReportData> DATALST = new ArrayList<ReportData>();

    // Constructor
    public MyApplication() {
        super();
        System.out.println(LOG_OUTPUT_HEADER + Log_Tag + ": Constructor");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println(LOG_OUTPUT_HEADER + Log_Tag + ": onCreate()");

        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            System.out.println("SD卡path: " + Environment.getExternalStorageDirectory());

            // package包 应用文件夹
            APPLICATION_DIR      = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator +
                                   "Android/data/" + this.getPackageName();
            File application_dir = new File(APPLICATION_DIR);

            if(!application_dir.exists()) {
                if(application_dir.mkdir()) { System.out.println("包应用文件夹创建成功: " + APPLICATION_DIR); }
                else { System.out.println("包应用文件夹创建失败"); }
            } else { System.out.println("包应用文件夹已存在: " + APPLICATION_DIR); }

            // 创建 应用文件夹
            APP_DIR_PATH = Environment.getExternalStorageDirectory() + File.separator + "设备填报";
            File appDir  = new File(APP_DIR_PATH);

            if(!appDir.exists()) {
                if(appDir.mkdir()) {
                    System.out.println("应用文件夹创建成功：" + appDir.getAbsolutePath());
                } else { System.out.println("未知错误，应用文件夹创建失败"); }
            } else { System.out.println("应用文件夹已存在：" + appDir.getAbsolutePath()); }

            // 图片文件夹
            APP_IMAGES_PATH = APP_DIR_PATH + File.separator + "图片";
            File imagesDir  = new File(APP_IMAGES_PATH);

            if(!imagesDir.exists()) {
                if(imagesDir.mkdir()) { System.out.println("应用图片文件夹创建成功：" + imagesDir.getAbsolutePath()); }
                else { System.out.println("未知错误，应用图片文件夹创建失败"); }
            } else { System.out.println("应用图片文件夹已存在：" + imagesDir.getAbsolutePath()); }

            // 视频文件夹
            APP_VIDEOS_PATH = APP_DIR_PATH + File.separator + "视频";
            File videosDir  = new File(APP_VIDEOS_PATH);

            if(!videosDir.exists()) {
                if(videosDir.mkdir()) { System.out.println("应用视频文件夹创建成功：" + videosDir.getAbsolutePath()); }
                else { System.out.println("未知错误，应用视频文件夹创建失败"); }
            } else { System.out.println("应用视频文件夹已存在：" + videosDir.getAbsolutePath()); }

            // Data文件夹
            APP_DATA_PATH = APP_DIR_PATH + File.separator + "data";
            File dataDir  = new File(APP_DATA_PATH);

            if(!dataDir.exists()) {
                if(dataDir.mkdir()) { System.out.println("应用data文件夹创建成功：" + dataDir.getAbsolutePath()); }
                else { System.out.println("未知错误，应用data文件夹创建失败"); }
            } else { System.out.println("应用data文件夹已存在：" + dataDir.getAbsolutePath()); }

            // 创建 日志文件夹
            APP_LOGS_PATH = APP_DIR_PATH + File.separator + "Logs";
            File logDir   = new File(APP_LOGS_PATH);

            if(!logDir.exists()) {
                if(logDir.mkdir()) { System.out.println("应用日志文件夹创建成功：" + logDir.getAbsolutePath()); }
                else { System.out.println("未知错误，应用日志文件夹创建失败"); }
            } else { System.out.println("应用日志文件夹已存在：" + logDir.getAbsolutePath()); }

            // App状态配置文件
            String APP_STATUS_CONFIG_FILE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "王曲变电站数据/config.xml";
            APP_STATUS_CONFIG_FILE = new File(APP_STATUS_CONFIG_FILE_PATH);

            if(!APP_STATUS_CONFIG_FILE.exists()) { createXml(APP_STATUS_CONFIG_FILE); }
            else { System.out.println("App状态配置文件已存在: " + APP_STATUS_CONFIG_FILE.getAbsolutePath()); }
        }

        // Toast Tool Init
        ToastUtils.init(this);
        ToastUtils.setGravity(Gravity.BOTTOM, 0, 100);

        // 程序异常崩溃handler
        appCrashHandler = AppCrashHandler.GetInstance();
        appCrashHandler.Init(getApplicationContext());
    }

    // 创建App状态配置文件
    private void createXml(File f) {
        System.out.println(LOG_OUTPUT_HEADER + Log_Tag + ": createXml()");

        try {
            XmlSerializer serializer = Xml.newSerializer();
            File file = new File(f.getAbsolutePath());

            FileOutputStream fos = new FileOutputStream(file);
            serializer.setOutput(fos, "UTF-8");
            serializer.startDocument("UTF-8", true);

            /// package
            serializer.startTag(null, "package");

            // security
            serializer.startTag(null, "com.demo.securityreport");
            serializer.attribute(null, "isbackground", "0");
            serializer.endTag(null, "com.demo.securityreport");
            // devices
            serializer.startTag(null, this.getPackageName());
            serializer.attribute(null, "isbackground", "0");
            serializer.endTag(null, this.getPackageName());

            serializer.endTag(null, "package");
            serializer.endDocument();

            fos.flush();
            fos.close();
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    private void parseXml(int tag, File f) {
        System.out.println(LOG_OUTPUT_HEADER + Log_Tag + ": parseXml()");

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder        = factory.newDocumentBuilder();

            Document doc = builder.parse(f);
            Node root    = doc.getDocumentElement();

            if(root.hasChildNodes()) {
                NodeList nodeLst = root.getChildNodes();

                for(int i = 0; i < nodeLst.getLength(); i++) {
                    if(nodeLst.item(i).getNodeType() == Node.ELEMENT_NODE) {
                        Element e = (Element)nodeLst.item(i);

                        if(e.getTagName().equals(this.getPackageName())) {
                            e.setAttribute("isbackground", String.valueOf(tag));
                        }
                    }
                }
                // 保存
                saveXml(doc, f.getAbsolutePath());
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    private void saveXml(Document doc, String path) {
        System.out.println(LOG_OUTPUT_HEADER + Log_Tag + ": saveXml()");

        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer former         = factory.newTransformer();

            former.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            former.setOutputProperty(OutputKeys.VERSION, doc.getXmlVersion());

            DOMSource source     = new DOMSource(doc);
            FileOutputStream fos = new FileOutputStream(path);
            StreamResult sr      = new StreamResult(fos);

            former.transform(source, sr);

            fos.flush();
            fos.close();
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        System.out.println(LOG_OUTPUT_HEADER + Log_Tag + ": onAppBackgrounded()");
        parseXml(1, APP_STATUS_CONFIG_FILE);
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        System.out.println(LOG_OUTPUT_HEADER + Log_Tag + ": onAppForegrounded()");
        parseXml(0, APP_STATUS_CONFIG_FILE);
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onAppDestroyed() {
        System.out.println(LOG_OUTPUT_HEADER + Log_Tag + ": onAppDestroyed()");
        parseXml(0, APP_STATUS_CONFIG_FILE);
    }
}