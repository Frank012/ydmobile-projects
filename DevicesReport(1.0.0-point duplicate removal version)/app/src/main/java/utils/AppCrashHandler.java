package utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class AppCrashHandler implements Thread.UncaughtExceptionHandler {
    private static MyApplication App    = null;
    private static final String Log_Tag = "AppCrashHandler";

    // Variables
    private Context context = null;
    private static AppCrashHandler appCrashHandlerInstance = null;
    private Thread.UncaughtExceptionHandler uncaughtExceptionHandler = null;

    private String logInfoStr = null;

    private DateFormat dateFormat = null;
    private Map<String, String> crashInfo = null;

    // Constructor
    private AppCrashHandler() {
        App = new MyApplication();
        System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": Constructor");
    }

    // 单例模式
    public static AppCrashHandler GetInstance() {
        System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": GetInstance()");

        if(appCrashHandlerInstance == null) { appCrashHandlerInstance = new AppCrashHandler(); }
        return appCrashHandlerInstance;
    }

    // 初始化
    public void Init(Context context) {
        System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": Init()");

        this.context = context;
        crashInfo  = new HashMap<String, String>();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

        uncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    // 错误信息
    private boolean HandleException(final Throwable throwable) {
        System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": HandleException()");

        if(throwable == null) { return false; }

        // 收集设备参数信息
        CollectDeviceInfo(context);
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(context, "程序出现异常，即将退出(" + throwable.getMessage() + ")", Toast.LENGTH_LONG).show();
                Looper.loop();
            }
        }.start();

        // 保存程序崩溃日志文件
        SaveCrashInfoLog(throwable);

        return true;
    }

    // 保存日志文件
    private String SaveCrashInfoLog(Throwable throwable) {
        System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": SaveCrashInfoLog()");

        StringBuffer strBuffer = new StringBuffer();
        for(Map.Entry<String, String> entry : crashInfo.entrySet()) {
            String key   = entry.getKey();
            String value = entry.getValue();
            strBuffer.append(key + "=" + value + "\n");
        }

        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        throwable.printStackTrace(printWriter);

        Throwable cause = throwable.getCause();
        while(cause != null) {
            cause.printStackTrace(printWriter);
            cause = cause.getCause();
        }
        printWriter.close();

        String result = writer.toString();
        strBuffer.append(result);

        try {
            Date date       = new Date(System.currentTimeMillis());
            String fileName = context.getApplicationInfo().packageName + "_" + dateFormat.format(date) + ".log";

            if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                FileOutputStream fos = new FileOutputStream(App.APP_LOGS_PATH + File.separator + fileName);
                logInfoStr = strBuffer.toString();
                fos.write(strBuffer.toString().getBytes());

                // 将程序崩溃信息发送给开发人员(邮件形式)
                SendLogInfoEmail(App.APP_LOGS_PATH + File.separator + fileName);
                fos.close();
            }

            return fileName;
        } catch(Exception e) {
            System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": SaveCrashInfoLog() Exception: " + e.getMessage());
        }

        return null;
    }

    // 发送邮件联网耗时
    class SendEmailAsyncTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected void onPreExecute() { super.onPreExecute(); }
        @Override
        protected String doInBackground(Integer... integers) {
            Properties pros = new Properties();
            pros.put("mail.debug", "true");
            pros.put("mail.smtp.auth", "true");
            pros.put("mail.smtp.host", "smtp.163.com");
            pros.put("mail.transport.protocol", "smtp");

            EmailAuthenticator auth = new EmailAuthenticator();
            Session session     = Session.getInstance(pros, auth);
            MimeMessage message = new MimeMessage(session);

            try {
                Address addrFrom = new InternetAddress("frank012@163.com", "frank012");
                Address addrTo   = new InternetAddress("frank012@163.com", "frank012");
                Address addrCopy = new InternetAddress("frank012@163.com", "frank012");

                message.setText(logInfoStr);
                message.setSubject(context.getApplicationInfo().packageName + " Crash Info");
                message.setFrom(addrFrom);
                message.addRecipient(Message.RecipientType.TO, addrTo);
                message.addRecipient(Message.RecipientType.CC, addrCopy);
                message.saveChanges();

                Transport transport = session.getTransport("smtp");
                // TODO.需要开始smtp服务，获取"授权码"
                transport.connect("frank012", "zorcs16326auth");

                transport.send(message);
                transport.close();
            } catch(UnsupportedEncodingException e) {
                e.printStackTrace();
                System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": SendLogInfoEmail() UnsupportedEncodingException: " + e.getMessage());
            } catch (MessagingException e) {
                e.printStackTrace();
                System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": SendLogInfoEmail() MessagingException: " + e.getMessage());
            }

            return null;
        }
    }

    // 发送程序崩溃Log信息
    private void SendLogInfoEmail(String fileName) {
        System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": SendLogInfoEmail()");

        if(!new File(fileName).exists()) {
            Toast.makeText(context, "日志文件不存在！", Toast.LENGTH_LONG).show();
            return;
        }

        new SendEmailAsyncTask().execute();
    }

    // 设备参数信息
    public void CollectDeviceInfo(Context context) {
        System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": CollectDeviceInfo()");

        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo packageInfo = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);

            if(packageInfo != null) {
                String vsName = packageInfo.versionName == null ? "null" : packageInfo.versionName;
                String vsCode = "vs." + packageInfo.versionCode;

                crashInfo.put("version_name", vsName);
                crashInfo.put("version_code", vsCode);
            }
        } catch (PackageManager.NameNotFoundException e) {
            System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": CollectDeviceInfo() NameNotFoundException: " + e.getMessage());
        }

        Field[] fields = Build.class.getDeclaredFields();
        for(Field field : fields) {
            try {
                field.setAccessible(true);
                crashInfo.put(field.getName(), field.get(null).toString());
            } catch(Exception e) {
                System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": CollectDeviceInfo() Exception: " + e.getMessage());
            }
        }
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": uncaughtException()");

        if(!HandleException(throwable) && uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(thread, throwable);
        } else {
            try { Thread.sleep(5000); }
            catch(InterruptedException e) {
                System.out.println(App.LOG_OUTPUT_HEADER + Log_Tag + ": uncaughtException() InterruptedException: " + e.getMessage());
            }

            // 退出程序
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }
    }
}