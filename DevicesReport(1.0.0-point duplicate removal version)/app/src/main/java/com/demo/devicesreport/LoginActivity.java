package com.demo.devicesreport;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.hjq.toast.ToastUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import utils.MyApplication;
import utils.SqliteUtil;

public class LoginActivity extends Activity {
    // Utils
    private boolean isDBOpened    = false;
    private SqliteUtil sqliteUtil = null;

    // Variables
    private MyApplication myApplication = null;
    private static final String Log_Tag = "LoginActivity";

    private static long mExitTime = 0;

    private SQLiteDatabase db = null;

    private float density    = 0f;
    private boolean isExpand = false;

    public List<View> convertViewLst    = new ArrayList<View>();
    private final List<String> usersLst = new ArrayList<String>();

    // 展开列表
    private LinearLayout userNameInputLayout = null;

    private LinearLayout userSelectBlockLayout = null;
    private TextView expandIconTextView        = null;

    private TextView userName = null;

    private TextView newUserBtn = null;

    // 加载
    public LoadingLayout loadingLayout = null;

    // 登录
    private Button loginBtn = null;

    // 添加用户
    private RelativeLayout newUserLayout = null;
    private EditText newUserEditText     = null;

    private RelativeLayout userDeleteNoteBlock = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApplication = new MyApplication();
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onCreate()");
        setContentView(R.layout.activity_login);

        sqliteUtil = new SqliteUtil();

        // 全屏
        LoginActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View view = this.getWindow().getDecorView();
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        density = getResources().getDisplayMetrics().density;

        SqliteInitial();    // 数据库初始化
        UsersListInitial(); // 用户数据初始化

        // UI初始化
        UIInitial();
    }

    // Sqlite Initial
    private void SqliteInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": SqliteInitial()");

        myApplication.DB_PATH = myApplication.APPLICATION_DIR + File.separator + "devices_report.db";
        System.out.println("数据库路径: " + myApplication.DB_PATH);

        File dbFile = new File(myApplication.DB_PATH);
        // 数据库不存在 从assets目录中将资源文件拷贝到 DB_PATH
        if(!dbFile.exists()) {
            try {
                InputStream is       = this.getResources().getAssets().open("devices_report.db");
                FileOutputStream fos = new FileOutputStream(myApplication.DB_PATH);

                int count  = 0;
                byte[] buf = new byte[1024];

                while((count = is.read(buf, 0, buf.length)) > 0) {
                    fos.write(buf, 0, count);
                    fos.flush();
                }
                is.close();

                fos.flush();
                fos.close();
            } catch(Exception e) {
                System.out.println("Read And Copy db file From Assets to DB_PATH Exception: " + e.getMessage());
                e.printStackTrace();
                ToastUtils.show("读取数据库文件异常: " + e.getMessage());
            }
        }
        // 存在 则直接从目录读取
        try {
            FileInputStream fis = new FileInputStream(dbFile);
            long size = fis.available();

            DecimalFormat df = new DecimalFormat("#.00");
            System.out.println("数据库文件大小：" + df.format((double) size / 1024) + "KB");

            db = SQLiteDatabase.openDatabase(myApplication.DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
            if(db != null) {
                System.out.println("数据库打开成功");
                isDBOpened = true;
            } else { ToastUtils.show("数据库打开失败"); }
        } catch(Exception e) {
            System.out.println("Open db Exception: " + e.getMessage());
            e.printStackTrace();
            ToastUtils.show("打开数据库失败:" + e.getMessage());
        }
    }

    // 用户数据初始化
    private void UsersListInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": UsersListInitial()");

        if(isDBOpened) {
            @SuppressLint("Recycle") Cursor cr = db.rawQuery("SELECT * FROM users", null);
            while(cr.getCount() != 0 && cr.moveToNext()) { usersLst.add(cr.getString(cr.getColumnIndex("user_name"))); }
        }
    }

    // Users List Adapter
    private UsersListAdapter usersListAdapter = null;
    public class UsersListAdapter extends BaseAdapter {
        private LayoutInflater inflater = null;

        // Constructor
        public UsersListAdapter(Context context) {
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() { return usersLst.size(); }
        @Override
        public Object getItem(int position) {
            return position;
        }
        @Override
        public long getItemId(int id) {
            return id;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(position + 1 > convertViewLst.size()) {
                convertView = inflater.inflate(R.layout.users_list_item, parent, false);
            } else { convertView = convertViewLst.get(position); }

            TextView userNameTextView     = convertView.findViewById(R.id.user_name);
            RelativeLayout userDeleteIcon = convertView.findViewById(R.id.user_delete);

            userNameTextView.setText(usersLst.get(position).toString());
            // 删除用户 监听
            userDeleteIcon.setOnClickListener(v1 -> runOnUiThread(() -> {
                String userNameValue = usersLst.get(position);

                AlertDialog d = new AlertDialog.Builder(LoginActivity.this).setTitle("删除员工").create();
                d.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (dialog, which) -> {
                    if(checkDataReported(userNameValue)) {
                        long deleteResult = db.delete("users", "user_name=?", new String[]{ userNameValue });

                        if(deleteResult != -1) {
                            loadingAction(50, "正在删除，请稍候...", View.VISIBLE);

                            usersLst.remove(position);
                            userName.setText("请选择姓名");
                            userName.setTextColor(Color.parseColor("#c0c0c0"));

                            notifyDataSetChanged();

                            isExpand = false;

                            ValueAnimator ani = createExpandAni(userSelectBlockLayout, (int) (density * 200 + 0.5), 0);
                            ani.start();
                            userSelectBlockLayout.setVisibility(View.GONE);

                            loginBtn.setVisibility(View.VISIBLE);

                            expandIconTextView.setText("▼");
                            expandIconTextView.setTextColor(Color.parseColor("#c0c0c0"));

                            userNameInputLayout.setClickable(true);

                            loadingAction(100, "删除成功", View.VISIBLE);
                            new Handler(Looper.getMainLooper()).postDelayed(() -> loadingLayout.setVisibility(View.GONE), 1000);
                        } else { ToastUtils.show("用户删除失败(" + String.valueOf(deleteResult) + ")"); }
                    } else {
                        runOnUiThread(() -> {
                            d.dismiss();
                            userDeleteNoteBlock.setVisibility(View.VISIBLE);
                        });
                    }
                });
                d.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (dialogInterface, i) -> d.dismiss());
                d.setMessage("确定删除用户 " + userNameValue + " 吗？");
                d.show();
            }));

            return convertView;
        }
    }

    // UI Initial
    @SuppressLint({"ClickableViewAccessibility", "UseCompatLoadingForDrawables"})
    private void UIInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": UIInitial()");

        // rootLayout(点击控件外部区域隐藏 展开列表)
        /// UI
        RelativeLayout rootLayout = findViewById(R.id.root_layout);
        rootLayout.setOnTouchListener((v, e) -> {
            v.performClick();

            if(isExpand) {
                isExpand = false;

                ValueAnimator ani = createExpandAni(userSelectBlockLayout, (int)(density * 200 + 0.5), 0);
                ani.start();
                userSelectBlockLayout.setVisibility(View.GONE);

                loginBtn.setVisibility(View.VISIBLE);

                expandIconTextView.setText("▼");
                expandIconTextView.setTextColor(Color.parseColor("#c0c0c0"));

                userNameInputLayout.setClickable(true);
            }

            return true;
        });

        // Loading Layout
        loadingLayout = super.findViewById(R.id.loading_layout);
        loadingLayout.setClickCallback(() -> { /* todo sth.*/ });

        // 用户名
        userName = findViewById(R.id.user_name_text_view);

        userNameInputLayout = findViewById(R.id.user_name_input_block);
        userNameInputLayout.setOnClickListener(v -> {
            if(!isExpand) {
                isExpand = true;
                userNameInputLayout.setClickable(false);

                loginBtn.setVisibility(View.INVISIBLE);

                userSelectBlockLayout.setVisibility(View.VISIBLE);
                ValueAnimator ani = createExpandAni(userSelectBlockLayout, 0, (int)(density * 200 + 0.5));
                ani.start();

                expandIconTextView.setText("取消");
                expandIconTextView.setTextColor(Color.parseColor("#1e90ff"));
            }
        });

        expandIconTextView = findViewById(R.id.expand_icon);
        expandIconTextView.setOnClickListener(v -> {
            if(isExpand) {
                isExpand = false;
                userNameInputLayout.setClickable(true);

                ValueAnimator ani = createExpandAni(userSelectBlockLayout, (int)(density * 200 + 0.5), 0);
                ani.start();
                userSelectBlockLayout.setVisibility(View.INVISIBLE);

                loginBtn.setVisibility(View.VISIBLE);

                expandIconTextView.setText("▼");
                expandIconTextView.setTextColor(Color.parseColor("#c0c0c0"));
            }
        });

        // 用户选择
        userSelectBlockLayout = findViewById(R.id.users_select_layout);
        userSelectBlockLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // 用户列表
        ListView userListView = findViewById(R.id.users_list);
        usersListAdapter = new UsersListAdapter(this);
        userListView.setAdapter(usersListAdapter);

        userListView.setOnItemClickListener((adapterView, v, position, id) -> runOnUiThread(() -> {
            userName.setText(usersLst.get(position));
            userName.setTextColor(Color.BLACK);

            isExpand = false;
            userNameInputLayout.setClickable(true);

            ValueAnimator ani = createExpandAni(userSelectBlockLayout, (int)(density * 200 + 0.5), 0);
            ani.start();
            userSelectBlockLayout.setVisibility(View.INVISIBLE);

            loginBtn.setVisibility(View.VISIBLE);

            expandIconTextView.setText("▼");
            expandIconTextView.setTextColor(Color.parseColor("#c0c0c0"));
        }));

        // 添加新用户
        newUserBtn   = findViewById(R.id.new_user_btn);
        newUserBtn.setOnTouchListener((v, e) -> {
            v.performClick();
            userNameInputLayout.setBackgroundResource(R.drawable.input_normal_style);

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> newUserBtn.setBackgroundColor(Color.parseColor("#498bdd")));
                    break;
                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        newUserBtn.setBackgroundColor(Color.parseColor("#f5f5f5"));

                        // 隐藏 选择列表
                        isExpand = false;
                        userNameInputLayout.setClickable(true);

                        ValueAnimator ani = createExpandAni(userSelectBlockLayout, (int)(density * 200 + 0.5), 0);
                        ani.start();
                        userSelectBlockLayout.setVisibility(View.INVISIBLE);

                        loginBtn.setVisibility(View.VISIBLE);

                        expandIconTextView.setText("▼");
                        expandIconTextView.setTextColor(Color.parseColor("#c0c0c0"));

                        // 显示 添加用户对话框
                        newUserLayout.setVisibility(View.VISIBLE);
                    });

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 登录
        loginBtn = findViewById(R.id.login_btn);
        loginBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        loginBtn.setBackgroundColor(Color.parseColor("#498bdd"));
                        loadingAction(50, "正在登录，请稍候...", View.VISIBLE);
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> { loginBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style)); });

                    if(userName.getText().toString().equals("请选择姓名")) {
                        ToastUtils.show("请选择您的姓名进行登录");
                        loadingLayout.setVisibility(View.GONE);
                    } else {
                        // 获取当前用户ID
                        @SuppressLint("Recycle") Cursor cr = db.rawQuery("SELECT * FROM users WHERE user_name = '" + userName.getText().toString() + "';", null);

                        if(cr.getCount() != 0 && cr.moveToNext()) {
                            myApplication.USER_ID = cr.getInt(0);
                            System.out.println("当前用户ID：" + String.valueOf(myApplication.USER_ID));

                            // 判断用户上次是否有未上传数据
                            if(checkDataReported(userName.getText().toString())) {
                                loadingAction(99, "登录成功，即将跳转到首页...", View.VISIBLE);

                                // 跳转到 主页
                                new Handler().postDelayed(() -> {
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.putExtra("FLAG", "0");

                                    startActivity(intent);
                                    overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                                    // 关闭数据库
                                    db.close();
                                    loadingAction(100, "登录成功，即将跳转到首页...", View.GONE);

                                    LoginActivity.this.finish();
                                }, 1000);
                            }
                            // 有 跳转到 上报页 上报数据
                            else {
                                /*
                                loadingAction(99, "登录成功，您有未上报数据，即将跳转到上报页...", View.VISIBLE);

                                // 跳转到 上报页
                                new Handler().postDelayed(() -> {
                                    Intent intent = new Intent(LoginActivity.this, ReportActivity.class);
                                    intent.putExtra("FLAG", "3");

                                    startActivity(intent);
                                    overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                                    // 关闭数据库
                                    db.close();
                                    loadingAction(100, "登录成功，您有未上报数据，即将跳转到上报页...", View.GONE);

                                    LoginActivity.this.finish();
                                }, 1000);
                                */

                                // TODO.弹出对话框，提示用户 是否现在上报
                                loadingLayout.setVisibility(View.GONE);

                                AlertDialog dialog = new AlertDialog.Builder(this).setTitle("上报数据").create();
                                dialog.setMessage("登录成功，您有未上报数据，是否立即前往上报页上报？");
                                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                                    loadingAction(100, "正在跳转到上报页...", View.VISIBLE);
                                    // 前往  上报页 或 首页
                                    goNextPage(false);
                                });
                                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {
                                    loadingAction(100, "即将跳转到首页...", View.VISIBLE);
                                    goNextPage(true);
                                });
                                dialog.show();
                            }
                        }
                    }
                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 添加新用户
        newUserLayout   = findViewById(R.id.new_user_layout);
        newUserEditText = findViewById(R.id.new_user_name_edit_text);
        // 确定
        TextView newUserOKBtn = findViewById(R.id.new_user_ok_btn);
        newUserOKBtn.setOnClickListener(v -> {
            String newUserNameValue = newUserEditText.getText().toString();
            System.out.println("新添加用户名：" + newUserNameValue);

            if(!newUserNameValue.equals("")) {
                // 格式是否正确
                boolean isContainDigit  = false;
                boolean isContainLetter = false;

                for(int i = 0; i < newUserNameValue.length(); ++i) {
                    if(Character.isDigit(newUserNameValue.charAt(i))) { isContainDigit = true; }
                    if(Character.isLetter(newUserNameValue.charAt(i))) {
                        // 过滤 中文UniCode
                        int charV = (int)newUserNameValue.charAt(i);
                        isContainLetter = charV < 19968;
                    }
                }

                if(isContainDigit || isContainLetter) { ToastUtils.show("用户名格式错误"); }
                else {
                    if(newUserNameValue.length() > 1) {
                        if(!isContainDigit && !isContainLetter) {
                            ContentValues cv  = new ContentValues();
                            long insertResult = 0;
                            cv.put("user_name", newUserNameValue);

                            loadingAction(90, "正在添加新用户，请稍候...", View.VISIBLE);

                            insertResult = db.insert("users", null, cv);

                            if(insertResult != -1) {
                                usersLst.add(newUserNameValue);
                                usersListAdapter.notifyDataSetChanged();

                                new Handler().postDelayed(() -> {
                                    loadingAction(100, "添加成功，用户列表已更新", View.VISIBLE);

                                    newUserLayout.setVisibility(View.GONE);
                                    userName.setText(newUserEditText.getText().toString());
                                    newUserEditText.setText("");

                                    new Handler().postDelayed(() -> loadingLayout.setVisibility(View.GONE), 500);
                                }, 1000);
                            } else {
                                loadingLayout.setVisibility(View.GONE);
                                ToastUtils.show("添加用户失败(" + String.valueOf(insertResult) + ")");
                            }
                        }
                    } else { ToastUtils.show("请输入正确的用户名"); }
                }
            } else { ToastUtils.show("用户名不能为空"); }
        });
        // 取消
        TextView newUserCancelBtn = findViewById(R.id.new_user_cancel_btn);
        newUserCancelBtn.setOnClickListener(v -> newUserLayout.setVisibility(View.INVISIBLE));

        //用户删除警告
        userDeleteNoteBlock = findViewById(R.id.user_delete_note_block);
        userDeleteNoteBlock.setOnTouchListener((v, e) -> {
            v.performClick();

            runOnUiThread(() -> {
                userDeleteNoteBlock.setVisibility(View.GONE);

                isExpand = false;
                userNameInputLayout.setClickable(true);

                ValueAnimator ani = createExpandAni(userSelectBlockLayout, (int)(density * 200 + 0.5), 0);
                ani.start();
                userSelectBlockLayout.setVisibility(View.INVISIBLE);

                loginBtn.setVisibility(View.VISIBLE);

                expandIconTextView.setText("▼");
                expandIconTextView.setTextColor(Color.parseColor("#c0c0c0"));
            });

            return true;
        });

        RelativeLayout userDeleteNoteLayout = findViewById(R.id.user_delete_note_layout);
        userDeleteNoteLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        TextView userDeleteNoteBtn = findViewById(R.id.user_delete_note_btn);
        userDeleteNoteBtn.setOnClickListener(view -> {
            userDeleteNoteBlock.setVisibility(View.GONE);

            isExpand = false;
            userNameInputLayout.setClickable(true);

            ValueAnimator ani = createExpandAni(userSelectBlockLayout, (int)(density * 200 + 0.5), 0);
            ani.start();
            userSelectBlockLayout.setVisibility(View.INVISIBLE);

            loginBtn.setVisibility(View.VISIBLE);

            expandIconTextView.setText("▼");
            expandIconTextView.setTextColor(Color.parseColor("#c0c0c0"));
        });
    }

    // 跳转到上报页 或 首页
    private void goNextPage(boolean go_home) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": goNextPage()");

        new Handler().postDelayed(() -> {
            Intent intent = new Intent(LoginActivity.this, go_home ? MainActivity.class : ReportActivity.class);
            intent.putExtra("FLAG", go_home ? "0" : "3");

            startActivity(intent);
            overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

            // 关闭 数据库
            if(db != null && db.isOpen()) { db.close(); }
            loadingLayout.setVisibility(View.GONE);

            LoginActivity.this.finish();
        }, 1000);
    }

    // 用户列表展开动画效果
    private ValueAnimator createExpandAni(final View v, int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(valueAnimator -> {
            int value = (int)valueAnimator.getAnimatedValue();

            ViewGroup.LayoutParams params = v.getLayoutParams();
            params.height = value;
            v.setLayoutParams(params);
        });

        return animator;
    }

    // LoadingLayout显示与隐藏
    private void loadingAction(int step, String note, int visibility) {
        loadingLayout.setProgressStep(step);
        loadingLayout.setProgressNote(note);
        loadingLayout.setVisibility(visibility);
    }

    // 检查用户是否有未上传数据
    private boolean checkDataReported(String name) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": checkDataReported()");

        boolean isDataReported = true;

        // 判断当前用户是否有未上传数据
        Cursor cr = db.rawQuery("SELECT id FROM users WHERE user_name = '" + name + "';", null);
        if(cr.getCount() != 0 && cr.moveToNext()) {
            int userId = cr.getInt(cr.getColumnIndex("id"));

            Cursor cr2 = db.rawQuery("SELECT * FROM user_report WHERE user_id = " + String.valueOf(userId) + ";", null);

            if(cr2.getCount() != 0) {
                List<Integer> reportIDs = new ArrayList<Integer>();

                while(cr2.moveToNext()) { reportIDs.add(cr2.getInt(cr2.getColumnIndex("report_id"))); }
                cr2.close();

                for(int i = 0; i < reportIDs.size(); ++i) {
                    Cursor cr3 = db.rawQuery("SELECT is_reported FROM reports WHERE id = " + String.valueOf(reportIDs.get(i)) + ";", null);

                    if(cr3.getCount() != 0 && cr3.moveToNext()) {
                        // 上报失败(未上报)
                        if(cr3.getInt(cr3.getColumnIndex("is_reported")) == 0) {
                            isDataReported = false;
                            cr3.close();
                            break;
                        }
                    }
                }
            } else { System.out.println("该用户尚未上报过任何数据"); }
        }
        cr.close();

        return isDataReported;
    }

    /***********************************Activity Override Start***********************************/
    @Override
    protected void onStart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStart()");
        super.onStart();
    }
    @Override
    protected void onResume() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onResume()");
        super.onResume();
    }
    @Override
    protected void onPause() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onPause()");
        super.onPause();
    }
    @Override
    protected void onStop() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStop()");
        super.onStop();
    }
    @Override
    protected void onRestart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onRestart()");
        super.onRestart();
    }
    @Override
    protected void onDestroy() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDestroy()");
        super.onDestroy();

        // 关闭数据库
        if(db != null && db.isOpen()) { db.close(); }
    }
    /***********************************Activity Override End***********************************/

    // 1s内 按两次返回键 退出程序
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(System.currentTimeMillis() - mExitTime > 2000) {
                ToastUtils.show("再按一次退出程序");
                mExitTime = System.currentTimeMillis();
            } else {
                db.close();
                LoginActivity.this.finish();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        }

        return false;
    }
}