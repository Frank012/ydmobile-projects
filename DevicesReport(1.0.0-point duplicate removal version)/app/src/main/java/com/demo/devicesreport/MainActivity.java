package com.demo.devicesreport;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.LayoutDirection;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;

import com.bigkoo.pickerview.TimePickerView;
import com.hjq.toast.ToastUtils;

import org.salient.artplayer.MediaPlayerManager;
import org.salient.artplayer.VideoView;
import org.salient.artplayer.ui.ControlPanel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import photoview.PhotoView;
import utils.MyApplication;
import utils.SqliteUtil;

public class MainActivity extends Activity {
    private float density = 0f;

    private static String FLAG     = null; // Intent标识
    private static int DATA_INDEX  = -1;   // 数据data index
    private static int SELECT_TYPE = -1;   // 选择value标识

    private static int CUR_AREA_ID        = -1;
    private static int CUR_DEVICE_TYPE_ID = -1;
    private static int CUR_POINT_ID       = -1;

    // 修改 保存(之前的点位数据，用以重新 更新 SAVED_IDS)
    private String MODIFY_AREA_BEFORE        = null;
    private String MODIFY_DEVICE_TYPE_BEFORE = null;
    private String MODIFY_POSITION_BEFORE    = null;

    // Utils
    private SqliteUtil sqliteUtil = null;

    // 请求码
    private static final int CAMERA_CAPTURE = 5;
    private static final int CAMERA_VIDEO   = 6;

    // Variables
    private MyApplication myApplication = null;
    private static final String Log_Tag = "MainActivity";

    private boolean isDBOpened = false;
    private SQLiteDatabase db  = null;

    private String cachePhotoTime = null;
    private String cacheVideoTime = null;
    private boolean isMenuClick   = false;

    private boolean isHaveAlarm = false;

    // 数据
    public List<String> areasLst           = new ArrayList<String>();
    public List<String> deviceTypeLst      = new ArrayList<String>();
    public List<String> positionNameLst    = new ArrayList<String>();
    public List<String> recognitionTypeLst = new ArrayList<String>();
    public List<String> unitLst            = new ArrayList<String>();
    public List<String> alarmTypeLst       = new ArrayList<String>();

    public String photoName = "";
    public String videoPath = "";

    // 拍照、视频
    private Uri captureImageUri = null;
    private Uri captureVideoUri = null;

    private Bitmap photoBitmap = null;

    private boolean isCaptured = false;
    private boolean isVideoed  = false;

    // UI
    private RelativeLayout rootLayout = null;

    private TextView areaValue            = null;
    private TextView deviceTypeValue      = null;
    private TextView positionNameValue    = null;
    private TextView recognitionTypeValue = null;
    private EditText resultValue          = null;
    private TextView unitValue            = null;
    private TextView recognitionTimeValue = null;
    private TextView alarmTypeValue       = null;
    private EditText alarmDescValue       = null;

    private RelativeLayout selectItemLayout = null;

    public int selectIndex                  = -1;
    private ListView selectItemList         = null;

    private TextView selectTitle = null;

    // 异常 Switch
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private Switch alarmSelectSwitch      = null;
    private RelativeLayout alarmInfoBlock = null;

    // 对话框
    private RelativeLayout dialogBlock  = null;
    private TextView dialogTitle        = null;
    private TextView dialogMessage      = null;
    private TextView dialogOKBtn        = null;
    private TextView dialogCancelBtn    = null;

    // Capture & Video
    private RelativeLayout captureLayout = null;
    private RelativeLayout videoLayout   = null;

    private ImageView captureImageView = null;
    private ImageView videoImageView   = null;

    private TextView captureNote = null;
    private TextView videoNote   = null;

    private ImageView photoDeleteIcon = null;
    private ImageView videoDeleteIcon = null;

    // 预览
    private RelativeLayout imagePreviewLayout = null;
    private RelativeLayout videoPreviewLayout = null;

    private ImageView videoPreviewBack = null;
    private TextView videoFileName     = null;

    private boolean isVideoPreviewShow = false;

    private PhotoView photoPreview = null;
    private VideoView videoPreview = null;

    // Save Layout
    private Button nextBtn         = null;
    private Button saveOrCancelBtn = null;

    public LoadingLayout loadingLayout = null;

    // 区域选择
    private final List<RadioButton> radioBtnLst = new ArrayList<>();
    // 不使用 ViewHolder
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private final List<View> convertViewLst = new ArrayList<View>();

    private RelativeLayout areaSelectItemLayout = null;
    private LinearLayout areaSelectList         = null;

    private RelativeLayout alarmLevelSelectItemLayout = null;

    // Select List Adapter
    private SelectListAdapter selectListAdapter = null;

    public class SelectListAdapter extends BaseAdapter {
        private LayoutInflater inflater = null;
        private List<String> valuesLst  = null;

        // Constructor
        public SelectListAdapter(Context context, List<String> list) {
            this.inflater  = LayoutInflater.from(context);
            this.valuesLst = list;
        }

        @Override
        public int getCount() { return valuesLst.size(); }
        @Override
        public Object getItem(int position) { return position; }
        @Override
        public long getItemId(int id) { return id; }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(position + 1 > convertViewLst.size()) {
                convertView = inflater.inflate(R.layout.select_list_item, null);
            } else { convertView = convertViewLst.get(position); }

            if(position % 2 == 0) { convertView.setBackgroundColor(Color.WHITE); }

            AbsListView.LayoutParams param = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);
            convertView.setLayoutParams(param);

            RadioButton radioBtn = convertView.findViewById(R.id.radio_btn);
            radioBtn.setClickable(false);

            radioBtn.setChecked(selectIndex == position);

            TextView valueTextView = convertView.findViewById(R.id.value_text_view);
            valueTextView.setText(valuesLst.get(position));

            return convertView;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApplication = new MyApplication();
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onCreate()");
        setContentView(R.layout.activity_main);

        // 获取屏幕 密度
        density = getResources().getDisplayMetrics().density;

        sqliteUtil = new SqliteUtil();

        // 数据库初始化
        db = SQLiteDatabase.openDatabase(MyApplication.DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
        if(db != null) {
            System.out.println("数据库打开成功");
            isDBOpened = true;
        } else { ToastUtils.show("数据库打开失败"); }

        // 获取数据
        DataInit();

        // 页面操作标识
        FLAG = getIntent().getStringExtra("FLAG");
        System.out.println("FLAG: " + FLAG);

        // 数据操作标识
        DATA_INDEX = getIntent().getIntExtra("DATA_INDEX", -1);
        System.out.println("DATA_INDEX: " + DATA_INDEX);

        // 全屏
        MainActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View view = this.getWindow().getDecorView();
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        // UI初始化
        UIInitial();

        // 修改 操作，根据 data_index 数据 初始化
        if(DATA_INDEX != -1) {
            areaValue.setText(myApplication.DATALST.get(DATA_INDEX).getArea());
            deviceTypeValue.setText(myApplication.DATALST.get(DATA_INDEX).getDeviceType());
            positionNameValue.setText(myApplication.DATALST.get(DATA_INDEX).getPositionName());
            recognitionTypeValue.setText(myApplication.DATALST.get(DATA_INDEX).getRecognitionType());
            resultValue.setText(myApplication.DATALST.get(DATA_INDEX).getResult());
            unitValue.setText(myApplication.DATALST.get(DATA_INDEX).getUnit());
            recognitionTimeValue.setText(myApplication.DATALST.get(DATA_INDEX).getRecognitionTime());

            if(myApplication.DATALST.get(DATA_INDEX).getHaveAlarm() == 0) { alarmSelectSwitch.setChecked(false); }
            else {
                alarmSelectSwitch.setChecked(true);

                alarmInfoBlock.setVisibility(View.VISIBLE);
                ValueAnimator ani = createExpandAni(alarmInfoBlock, 0, (int)(density * 281 + 0.5));
                ani.start();

                alarmTypeValue.setText(myApplication.DATALST.get(DATA_INDEX).getAlarmType());
                alarmDescValue.setText(myApplication.DATALST.get(DATA_INDEX).getAlarmDesc());

                // 照片、视频
                if(!myApplication.DATALST.get(DATA_INDEX).getPhoto().equals("")) {
                    captureImageView.setImageBitmap(BitmapFactory.decodeFile(myApplication.DATALST.get(DATA_INDEX).getPhoto()));
                    photoDeleteIcon.setVisibility(View.VISIBLE);
                    isCaptured = true;
                }
                if(!myApplication.DATALST.get(DATA_INDEX).getVideo().equals("")) {
                    videoImageView.setImageBitmap(getVideoThumbnailBmp(MediaStore.Images.Thumbnails.MINI_KIND));
                    videoDeleteIcon.setVisibility(View.VISIBLE);
                    isVideoed = true;
                }
            }

            MODIFY_AREA_BEFORE        = myApplication.DATALST.get(DATA_INDEX).getArea();
            MODIFY_DEVICE_TYPE_BEFORE = myApplication.DATALST.get(DATA_INDEX).getDeviceType();
            MODIFY_POSITION_BEFORE    = myApplication.DATALST.get(DATA_INDEX).getPositionName();
        }
    }

    // Data Init
    private void DataInit() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": DataInit()");

        if(isDBOpened) {
            // 获取区域Areas
            Cursor cr = db.rawQuery("SELECT name FROM areas;", null);
            while(cr.getCount() != 0 && cr.moveToNext()) { areasLst.add(cr.getString(cr.getColumnIndex("name"))); }
            cr.close();
            // 获取 默认(区域一)区域下的 设备类型
            Cursor cr2 = db.rawQuery("SELECT name FROM device_type WHERE area_id = 1;", null);
            while(cr2.getCount() != 0 && cr2.moveToNext()) { deviceTypeLst.add(cr2.getString(cr2.getColumnIndex("name"))); }
            cr2.close();
            // 获取 默认(区域一)区域下的 点位
            Cursor cr3 = db.rawQuery("SELECT name FROM points WHERE device_type_id = 1;", null);
            while(cr3.getCount() != 0 && cr3.moveToNext()) { positionNameLst.add(cr3.getString(cr3.getColumnIndex("name"))); }
            cr3.close();
            // 获取识别类型
            Cursor cr4 = db.rawQuery("SELECT name FROM recognition_type;", null);
            while(cr4.getCount() != 0 && cr4.moveToNext()) { recognitionTypeLst.add(cr4.getString(cr3.getColumnIndex("name"))); }
            cr4.close();
            // 获取单位
            Cursor cr5 = db.rawQuery("SELECT name FROM unit;", null);
            while(cr5.getCount() != 0 && cr5.moveToNext()) { unitLst.add(cr5.getString(cr5.getColumnIndex("name"))); }
            cr5.close();
            // 获取 告警类型
            Cursor cr6 = db.rawQuery("SELECT name FROM alarm_type;", null);
            while(cr6.getCount() != 0 && cr6.moveToNext()) { alarmTypeLst.add(cr6.getString(cr6.getColumnIndex("name"))); }
            cr6.close();
        }
    }

    // 设备异常 警告填报 展开动画效果
    private ValueAnimator createExpandAni(final View v, int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(valueAnimator -> {
            int value = (int)valueAnimator.getAnimatedValue();

            ViewGroup.LayoutParams params = v.getLayoutParams();
            params.height = value;
            v.setLayoutParams(params);
        });

        return animator;
    }

    // UI Initial
    @SuppressLint({"ClickableViewAccessibility", "UseCompatLoadingForDrawables"})
    private void UIInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": UIInitial()");

        rootLayout = findViewById(R.id.root);

        // Title Layout
        ImageView menu = findViewById(R.id.menu);
        if(FLAG.equals("1")) { menu.setVisibility(View.VISIBLE); }
        else { menu.setVisibility(View.GONE); }

        menu.setOnClickListener(v -> {
            runOnUiThread(() -> {
                dialogTitle.setText("保存");
                dialogMessage.setText("需要保存已添加信息吗？");
                dialogBlock.setVisibility(View.VISIBLE);
            });

            isMenuClick = true;
        });

        // Operation Note
        TextView operationNote = findViewById(R.id.operation_note);
        if(FLAG.equals("2")) { operationNote.setText("修改"); }

        // Select Layout
        areaValue            = findViewById(R.id.area_value);
        deviceTypeValue      = findViewById(R.id.device_type_value);
        positionNameValue    = findViewById(R.id.position_name_value);
        recognitionTypeValue = findViewById(R.id.recognition_type_value);
        resultValue          = findViewById(R.id.result_value);
        unitValue            = findViewById(R.id.unit_value);
        recognitionTimeValue = findViewById(R.id.recognition_time_value);
        alarmTypeValue       = findViewById(R.id.alarm_type_value);
        alarmDescValue       = findViewById(R.id.alarm_desc_value);

        // 从上报页 跳转到 添加 时，清空所有选项
        if(FLAG.equals("1")) {
            areaValue.setText("");
            deviceTypeValue.setText("");
            positionNameValue.setText("");
            recognitionTypeValue.setText("");
            unitValue.setText("℃");
        }

        // 区域选择
        areaSelectItemLayout = findViewById(R.id.area_select_item_layout);
        RelativeLayout areaSelectItemContainer = findViewById(R.id.area_select_item_container);
        areaSelectList       = findViewById(R.id.area_select_list);

        areaSelectItemLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            areaSelectItemLayout.setVisibility(View.GONE);
            return true;
        });
        areaSelectItemContainer.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // 区域选择初始化
        areaSelectListInitial();

        // 违章行为类别选择
        alarmLevelSelectItemLayout = findViewById(R.id.alarm_level_select_item_layout);
        RelativeLayout alarmLevelSelectItemContainer = findViewById(R.id.alarm_level_select_item_container);

        alarmLevelSelectItemLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            alarmLevelSelectItemLayout.setVisibility(View.GONE);
            return true;
        });
        alarmLevelSelectItemContainer.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // Select Item Layout
        selectTitle = findViewById(R.id.select_title);

        selectItemLayout = findViewById(R.id.select_item_layout);
        selectItemLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            selectItemLayout.setVisibility(View.GONE);
            return true;
        });

        selectItemList   = findViewById(R.id.select_item_list);
        selectItemList.setOnItemClickListener((adapterView, view, position, id) -> {
            selectIndex = position;
            selectItemLayout.setVisibility(View.GONE);

            // 设备类型、点位名称、识别类型、单位、告警类型 Select
            switch(SELECT_TYPE) {
                case 1:
                    deviceTypeValue.setText(selectListAdapter.valuesLst.get(position));
                    // 更新 点位 并过滤
                    updatePositionNameLst(CUR_AREA_ID);
                    break;

                case 2: positionNameValue.setText(selectListAdapter.valuesLst.get(position)); break;
                case 3: recognitionTypeValue.setText(selectListAdapter.valuesLst.get(position)); break;
                case 4: unitValue.setText(selectListAdapter.valuesLst.get(position)); break;
                case 5: alarmTypeValue.setText(selectListAdapter.valuesLst.get(position)); break;

                default: break;
            }
        });

        RelativeLayout selectItemContainer = findViewById(R.id.select_item_container);
        selectItemContainer.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // Alarm Switch
        alarmSelectSwitch = findViewById(R.id.alarm_select_switch);
        alarmSelectSwitch.setOnCheckedChangeListener((btn, checked) -> {
            isHaveAlarm = checked;

            if(checked) {
                alarmInfoBlock.setVisibility(View.VISIBLE);
                ValueAnimator ani = createExpandAni(alarmInfoBlock, 0, (int)(density * 281 + 0.5));
                ani.start();
            } else {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm.isActive()) {
                    // 强制隐藏
                    View focv = MainActivity.this.getCurrentFocus();
                    if(focv != null) { imm.hideSoftInputFromWindow(focv.getWindowToken(), 0); }
                }

                ValueAnimator ani = createExpandAni(alarmInfoBlock, (int)(density * 281 + 0.5), 0);
                ani.start();
                alarmInfoBlock.setVisibility(View.GONE);
            }
        });
        alarmInfoBlock = findViewById(R.id.alarm_info_block);

        // Capture & Video
        captureImageView = findViewById(R.id.capture_icon);
        videoImageView   = findViewById(R.id.video_icon);

        captureNote = findViewById(R.id.capture_note);
        videoNote   = findViewById(R.id.video_note);

        photoDeleteIcon = findViewById(R.id.photo_delete_icon);
        videoDeleteIcon = findViewById(R.id.video_delete_icon);

        // 拍照
        captureLayout = findViewById(R.id.capture_layout);
        captureLayout.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        captureNote.setTextColor(Color.parseColor("#1e90ff"));
                        captureLayout.setBackgroundColor(Color.parseColor("#dcdcdc"));
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        captureNote.setTextColor(Color.BLACK);
                        captureLayout.setBackgroundColor(Color.TRANSPARENT);
                    });

                    // 未拍照，则 前往拍照页面
                    if(!isCaptured) {
                        ToastUtils.show("拍照");

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                        Date date = new Date(System.currentTimeMillis());

                        cachePhotoTime    = sdf.format(date);
                        File captureImage = new File(getExternalCacheDir(), cachePhotoTime + ".jpg");

                        if(!captureImage.exists()) {
                            try { if(captureImage.createNewFile()) { System.out.println("创建图片Uri成功"); } }
                            catch(IOException ex) {
                                System.out.println(ex.getMessage());
                                ex.printStackTrace();
                                ToastUtils.show("创建图片Uri失败:" + ex.getMessage());
                            }
                        }

                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            captureImageUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.concat(".provider"), captureImage);
                        } else {
                            captureImageUri = Uri.fromFile(captureImage);
                        }

                        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureImageUri);

                        startActivityForResult(captureIntent, CAMERA_CAPTURE);
                    }
                    // 已拍照，则 查看预览图
                    else {
                        if(DATA_INDEX != -1) {
                            photoPreview.setImageDrawable(new BitmapDrawable(BitmapFactory.decodeFile(myApplication.DATALST.get(DATA_INDEX).getPhoto())));
                        } else { photoPreview.setImageDrawable(new BitmapDrawable(photoBitmap)); }

                        imagePreviewLayout.setVisibility(View.VISIBLE);
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });
        // 拍摄视频
        videoLayout = findViewById(R.id.video_layout);
        videoLayout.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        videoNote.setTextColor(Color.parseColor("#1e90ff"));
                        videoLayout.setBackgroundColor(Color.parseColor("#dcdcdc"));
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        videoNote.setTextColor(Color.BLACK);
                        videoLayout.setBackgroundColor(Color.TRANSPARENT);
                    });

                    if(!isVideoed) {
                        ToastUtils.show("拍摄");

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                        Date date = new Date(System.currentTimeMillis());

                        cacheVideoTime    = sdf.format(date);
                        File captureVideo = new File(getExternalCacheDir(), cacheVideoTime + ".mp4");

                        if(!captureVideo.exists()) {
                            try { if(captureVideo.createNewFile()) { System.out.println("创建视频Uri成功"); } }
                            catch(IOException ex) {
                                System.out.println(ex.getMessage());
                                ex.printStackTrace();
                                ToastUtils.show("创建视频Uri失败:" + ex.getMessage());
                            }
                        }

                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            captureVideoUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.concat(".provider"), captureVideo);
                        } else {
                            captureVideoUri = Uri.fromFile(captureVideo);
                        }

                        Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        videoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10); // 限制录制时间10秒
                        videoIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureVideoUri);

                        startActivityForResult(videoIntent, CAMERA_VIDEO);
                    } else {
                        // ①调用系统播放器播放视频有两个问题：
                        //   1.必须使用getContentResolver()，播放uri路径下的视频，但是会报没有权限的错误；视频不在sdcard上
                        //   2.使用sdcard路径，会有exposed uri的问题(两者好像互相矛盾)
                        // ②使用videoview的问题，屏幕一直黑屏，暂时找不到原因
                        // ③使用MediaPlayer + SurfaceView 再次点击预览，surfaceview无法清空

                        // 使用 ArtPlayer插件
                        videoPreviewLayout.setVisibility(View.VISIBLE);

                        if(DATA_INDEX != -1) {
                            videoPreview.setUp(myApplication.DATALST.get(DATA_INDEX).getVideo());
                            runOnUiThread(() -> videoFileName.setText(new File(myApplication.DATALST.get(DATA_INDEX).getVideo()).getName()));
                        } else {
                            videoPreview.setUp(videoPath);
                            runOnUiThread(() -> videoFileName.setText(new File(videoPath).getName()));
                        }

                        videoPreview.setControlPanel(new ControlPanel(this));
                        videoPreview.start();

                        isVideoPreviewShow = true;
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        /// 预览
        // 图片
        imagePreviewLayout = findViewById(R.id.image_preview_layout);
        imagePreviewLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            imagePreviewLayout.setVisibility(View.GONE);
            return true;
        });
        photoPreview = findViewById(R.id.photo_preview);

        // 视频
        videoPreviewLayout = findViewById(R.id.video_preview_layout);
        videoPreview       = findViewById(R.id.video_preview);

        videoPreviewBack   = findViewById(R.id.video_preview_back);
        videoFileName      = findViewById(R.id.video_file_name);

        videoPreviewBack.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> videoPreviewBack.setBackgroundColor(Color.parseColor("#dcdcdc")));
                    break;
                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> videoPreviewBack.setBackgroundColor(Color.TRANSPARENT));

                    MediaPlayerManager.instance().releasePlayerAndView(this);

                    runOnUiThread(() -> {
                        videoFileName.setText("");
                        videoPreviewLayout.setVisibility(View.GONE);
                    });

                    isVideoPreviewShow = false;

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 下一个(添加)-修改(修改)
        nextBtn = findViewById(R.id.next_btn);
        if(!FLAG.equals("2")) { nextBtn.setText("下一个"); }
        else { nextBtn.setText("修改"); }

        nextBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                        runOnUiThread(() -> { nextBtn.setBackgroundColor(Color.parseColor("#498bdd")); });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> { nextBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style)); });

                    if(nextBtn.getText().toString().equals("下一个")) {
                        // 必填项判断
                        if(checkKeyInfoComplete()) {
                            loadingAction(50, "正在保存数据，请稍候...");

                            saveDataToDB();              // 数据库保存数据(datas表)
                            saveDataToReportDataClass(); // ReportData 保存数据
                            saveDevice();                // 保存 设备信息(为SelectList移除做判断，这里开始初始化 CUR_AREA_ID 等信息)

                            // 判断当前 设备的 点位 是否填完，是 则进入下一个设备的 第1个点位 填报，否 则进入当前设备 下一个 点位 填报，直至全电站设备点位填报完毕为止
                            Map<String, Object> m = new HashMap<String, Object>();
                            m = checkNextPoint();

                            final String areaStr       = (String)m.get("area");
                            final String deviceTypeStr = (String)m.get("device_type");
                            final String pointNameStr  = (String)m.get("point_name");

                            if((boolean)m.get("have_next")) {
                                runOnUiThread(() -> { loadingAction(100, "数据保存成功，开始填报下一项..."); });

                                // TODO-2 选择列表 移除已填报的点位
                                removeSavedDeviceFromSelectList();

                                // 默认 填好下一个点位 相关信息
                                new Handler().postDelayed(() -> {
                                    if(areaStr != null && deviceTypeStr != null && pointNameStr != null) {
                                        areaValue.setText(areaStr);
                                        deviceTypeValue.setText(deviceTypeStr);
                                        positionNameValue.setText(pointNameStr);

                                        recognitionTypeValue.setText("");
                                        resultValue.setText("");
                                        unitValue.setText("℃");
                                        recognitionTimeValue.setText("");

                                        if(alarmSelectSwitch.isChecked()) {
                                            alarmSelectSwitch.setChecked(false);

                                            ValueAnimator ani = createExpandAni(alarmInfoBlock, (int)(density * 281 + 0.5), 0);
                                            ani.start();
                                            alarmInfoBlock.setVisibility(View.GONE);

                                            alarmTypeValue.setText("告警类型一");
                                            alarmDescValue.setText("");

                                            photoDeleteIcon.setVisibility(View.GONE);
                                            captureImageView.setImageBitmap(null);
                                            captureImageView.setBackgroundResource(R.drawable.capture_icon);
                                            isCaptured = false;

                                            videoDeleteIcon.setVisibility(View.GONE);
                                            videoImageView.setImageBitmap(null);
                                            videoImageView.setBackgroundResource(R.drawable.video_icon);
                                            isVideoed = false;
                                        }
                                    } else { ToastUtils.show("获取下一个点位错误"); }

                                    loadingLayout.setVisibility(View.GONE);
                                }, 500);
                            }
                            // TODO.test 如果 最后 整个变电站设备 全部都填报了，则 提示没有 下一个 填报项，并跳转到 上报页
                            // 这个逻辑 需要知道当前 已经 保存填报过的 设备id，但是目前暂时不做考虑
                            else {
                                myApplication.IS_ALL_DEVICE_REPORTED = true;

                                runOnUiThread(() -> { loadingAction(100, "数据保存成功，当前电站设备已全部填报完毕，即将跳转到上报页..."); });
                                new Handler().postDelayed(() -> {
                                    Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                                    startActivity(intent);
                                    overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                                    db.close();
                                    loadingLayout.setVisibility(View.GONE);

                                    MainActivity.this.finish();
                                }, 1000);
                            }
                        } else { ToastUtils.show("请填报完整信息"); }
                    }
                    // 修改(保存)
                    else {
                        if(checkKeyInfoComplete()) {
                            loadingAction(50, "正在保存数据，请稍候...");

                            // 更新数据库数据
                            ContentValues cv = new ContentValues();
                            int updateResult = -1;

                            StringBuffer deviceIdStr          = new StringBuffer("");
                            StringBuffer deviceNameStr        = new StringBuffer("");
                            StringBuffer pointIdStr           = new StringBuffer("");
                            StringBuffer recognitionTypeIdStr = new StringBuffer("");
                            getDeviceId(deviceIdStr, deviceNameStr, pointIdStr, recognitionTypeIdStr);

                            cv.put("area", areasLst.indexOf(areaValue.getText().toString()) + 1);

                            if(!deviceIdStr.toString().equals("")) {
                                cv.put("device_id", Integer.parseInt(deviceIdStr.toString()));
                            }
                            if(!deviceNameStr.toString().equals("")) {
                                cv.put("device_name", deviceNameStr.toString());
                            }
                            if(!pointIdStr.toString().equals("")) {
                                cv.put("point_id", Integer.parseInt(pointIdStr.toString()));
                            }

                            cv.put("point_name", positionNameValue.getText().toString());

                            if(!recognitionTypeIdStr.toString().equals("")) {
                                cv.put("recognition_type_id", Integer.parseInt(recognitionTypeIdStr.toString()));
                            }

                            cv.put("result", resultValue.getText().toString());
                            cv.put("unit_id", unitLst.indexOf(unitValue.getText().toString()) + 1);
                            cv.put("recognition_time", recognitionTimeValue.getText().toString());
                            cv.put("alarm_status", isHaveAlarm ? 1 : 0);

                            if(isHaveAlarm) {
                                cv.put("alarm_type_id", alarmTypeLst.indexOf(alarmTypeValue.getText().toString()) + 1);
                                cv.put("alarm_description", alarmDescValue.getText().toString());

                                if(!photoName.equals("")) { cv.put("image", photoName); }
                                if(!videoPath.equals("")) { cv.put("video", videoPath); }
                            } else {
                                cv.put("alarm_type_id", "");
                                cv.put("alarm_description", "");
                                cv.put("image", "");
                                cv.put("video", "");
                            }

                            // 未上报数据 DATA_INDEX 为实际未上报 数据 data_id
                            if(myApplication.UNREPORT_ID_LST.size() > 0) {
                                updateResult = db.update("datas", cv, "id=?", new String[]{ String.valueOf(myApplication.UNREPORT_DATA_ID_LST.get(DATA_INDEX)) });
                            } else {
                                updateResult = db.update("datas", cv, "id=?", new String[]{ String.valueOf(DATA_INDEX + 1) });
                            }

                            if(updateResult != -1) {
                                System.out.println("DATAS数据更新成功");
                                cv.clear();
                                cv = null;
                            } else { ToastUtils.show("datas数据更新失败(" + String.valueOf(updateResult) + ")"); }

                            // 更新 DATALST
                            myApplication.DATALST.get(DATA_INDEX).setArea(areaValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setDeviceType(deviceTypeValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setPositionName(positionNameValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setRecognitionType(recognitionTypeValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setResult(resultValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setUnit(unitValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setRecognitionTime(recognitionTimeValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setHaveAlarm(isHaveAlarm ? 1 : 0);
                            if(isHaveAlarm) {
                                myApplication.DATALST.get(DATA_INDEX).setAlarmType(alarmTypeValue.getText().toString());
                                myApplication.DATALST.get(DATA_INDEX).setAlarmDesc(alarmDescValue.getText().toString());

                                if(!photoName.equals("")) {
                                    myApplication.DATALST.get(DATA_INDEX).setPhoto(photoName);
                                    photoName = "";
                                }
                                if(!videoPath.equals("")) {
                                    myApplication.DATALST.get(DATA_INDEX).setVideo(videoPath);
                                    videoPath = "";
                                }
                            } else {
                                myApplication.DATALST.get(DATA_INDEX).setAlarmType("");
                                myApplication.DATALST.get(DATA_INDEX).setAlarmDesc("");
                                myApplication.DATALST.get(DATA_INDEX).setPhoto("");
                                myApplication.DATALST.get(DATA_INDEX).setVideo("");
                            }

                            // TODO.修改更新 SAVED_IDS
                            modifyUpdateSavedIDs();

                            runOnUiThread(() -> { loadingAction(100, "数据修改成功"); });
                            new Handler().postDelayed(() -> {
                                Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                                startActivity(intent);
                                overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                                db.close();
                                loadingLayout.setVisibility(View.GONE);

                                MainActivity.this.finish();
                            }, 500);
                        } else { ToastUtils.show("请填报完整信息"); }
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });
        // 保存(添加)-取消(修改)
        saveOrCancelBtn = findViewById(R.id.save_or_cancel_btn);
        if(!FLAG.equals("2")) { saveOrCancelBtn.setText("保存"); }
        else { saveOrCancelBtn.setText("取消"); }

        saveOrCancelBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> { saveOrCancelBtn.setBackgroundColor(Color.parseColor("#498bdd")); });
                break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> { saveOrCancelBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.cancel_btn_normal_style)); });

                    if(saveOrCancelBtn.getText().toString().equals("保存")) {
                        if(checkKeyInfoComplete()) {
                            loadingAction(50, "正在保存数据，请稍候...");

                            saveDataToDB();
                            saveDataToReportDataClass();
                            saveDevice();

                            runOnUiThread(() -> { loadingAction(100, "数据保存成功，即将跳转到上报页..."); });
                            // 跳转到 上报页
                            new Handler().postDelayed(() -> {
                                Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                                startActivity(intent);
                                overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                                db.close();
                                loadingLayout.setVisibility(View.GONE);

                                MainActivity.this.finish();
                            }, 500);
                        } else { ToastUtils.show("请填报完整信息"); }
                    }
                    // 修改(取消)
                    else {
                        /* 弹窗 是否确定取消保存
                        dialogTitle.setText("修改取消");
                        dialogMessage.setText("确定放弃修改吗？");
                        dialogBlock.setVisibility(View.VISIBLE);
                        */
                        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setTitle("放弃修改").create();
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                            dialog.dismiss();

                            Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.activity_slide_out_left, R.anim.activity_slide_in_right);

                            db.close();
                            MainActivity.this.finish();
                        });
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {dialog.dismiss();});
                        dialog.setMessage("确定放弃当前修改吗？");
                        dialog.show();
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 对话框
        dialogBlock = findViewById(R.id.dialog_block);
        dialogBlock.setOnTouchListener((v, e) -> {
            v.performClick();
            //dialogBlock.setVisibility(View.GONE);
            return true;
        });
        RelativeLayout dialogLayout = findViewById(R.id.dialog_layout);
        dialogLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });
        ImageView dialogClose = findViewById(R.id.dialog_close);
        dialogClose.setOnClickListener(v -> dialogBlock.setVisibility(View.GONE));

        dialogTitle   = findViewById(R.id.dialog_title);
        dialogMessage = findViewById(R.id.dialog_message);

        dialogOKBtn = findViewById(R.id.dialog_ok_btn);
        dialogOKBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> dialogOKBtn.setTextColor(Color.parseColor("#498bdd")));
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> { dialogOKBtn.setTextColor(Color.parseColor("#1e90ff")); });

                    if(isMenuClick) {
                        if(checkKeyInfoComplete()) {
                            loadingAction(50, "正在保存数据，请稍候...");

                            saveDataToDB();
                            saveDataToReportDataClass();
                            saveDevice();

                            new Handler().postDelayed(() -> {
                                loadingAction(100, "数据保存成功，即将跳转到上报页...");

                                new Handler().postDelayed(() -> {
                                    Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

                                    db.close();
                                    loadingLayout.setVisibility(View.GONE);

                                    MainActivity.this.finish();
                                }, 500);
                            }, 1000);
                        } else { ToastUtils.show("请填报完整信息"); }
                    } else {
                        Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                        db.close();
                        dialogBlock.setVisibility(View.GONE);

                        MainActivity.this.finish();
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });
        dialogCancelBtn = findViewById(R.id.dialog_cancel_btn);
        dialogCancelBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> dialogCancelBtn.setTextColor(Color.parseColor("#498bdd")));
                    break;

                case MotionEvent.ACTION_UP:
                    if(isMenuClick) {
                        Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_slide_out_left, R.anim.activity_slide_in_right);

                        db.close();
                        MainActivity.this.finish();
                    } else {
                        runOnUiThread(() -> {
                            dialogCancelBtn.setTextColor(Color.parseColor("#1e90ff"));
                            dialogBlock.setVisibility(View.GONE);
                        });
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // Loading Layout
        loadingLayout = super.findViewById(R.id.loading_layout);
        loadingLayout.setElevation(1);
        loadingLayout.setClickCallback(() -> { /* todo sth.*/ });
    }

    // 检查 必填项 信息是否填报 完整
    private boolean checkKeyInfoComplete() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": checkKeyInfoComplete()");

        boolean isInfoComplete = false;
        boolean go_next        = false;

        if(!areaValue.getText().toString().equals("") &&
           !deviceTypeValue.getText().toString().equals("") &&
           !positionNameValue.getText().toString().equals("") &&
           !recognitionTypeValue.getText().toString().equals("") &&
           !resultValue.getText().toString().equals("") &&
           !unitValue.getText().toString().equals("") &&
           !recognitionTimeValue.getText().toString().equals("")) {
            isInfoComplete = true;
        }

        if(isHaveAlarm) {
            if(!alarmTypeValue.getText().toString().equals("") &&
               !alarmDescValue.getText().toString().equals("")) {

                if(DATA_INDEX != -1) {
                    if(!myApplication.DATALST.get(DATA_INDEX).getPhoto().equals("")) {
                        go_next = true;
                    } else { if(!photoName.equals("")) { go_next = true; } }
                } else { if(!photoName.equals("")) { go_next = true; } }
            }
        } else { if(isInfoComplete) { go_next = true; } }

        return go_next;
    }

    // 将数据 保存到 ReportData Class
    private void saveDataToReportDataClass() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": saveDataToReportDataClass()");

        ReportData data = new ReportData();

        data.setArea(areaValue.getText().toString());
        data.setDeviceType(deviceTypeValue.getText().toString());
        data.setPositionName(positionNameValue.getText().toString());
        data.setRecognitionType(recognitionTypeValue.getText().toString());
        data.setResult(resultValue.getText().toString());
        data.setUnit(unitValue.getText().toString());
        data.setRecognitionTime(recognitionTimeValue.getText().toString());
        data.setHaveAlarm(isHaveAlarm ? 1 : 0);
        data.setAlarmType(isHaveAlarm ? alarmTypeValue.getText().toString() : "");
        data.setAlarmDesc(isHaveAlarm ? alarmDescValue.getText().toString() : "");
        data.setPhoto(isHaveAlarm ? photoName : "");
        data.setVideo(isHaveAlarm ? videoPath : "");

        myApplication.DATALST.add(data);

        photoName = "";
        videoPath = "";
    }
    // 将数据 保存到 datas 表
    private void saveDataToDB() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": saveDataToDB()");

        ContentValues cv  = new ContentValues();
        long insertResult = -1;

        // AREA
        cv.put("area", areasLst.indexOf((String)areaValue.getText().toString()) + 1);

        StringBuffer deviceIdStr          = new StringBuffer("");
        StringBuffer deviceNameStr        = new StringBuffer("");
        StringBuffer pointIdStr           = new StringBuffer("");
        StringBuffer recognitionTypeIdStr = new StringBuffer("");

        // 获取 device_id
        getDeviceId(deviceIdStr, deviceNameStr, pointIdStr, recognitionTypeIdStr);

        // DEVICE_ID
        if(!deviceIdStr.toString().equals("")) { cv.put("device_id", Integer.parseInt(deviceIdStr.toString())); }
        // DEVICE_NAME
        if(!deviceNameStr.toString().equals("")) { cv.put("device_name", deviceNameStr.toString()); }
        // POINT_ID
        if(!pointIdStr.toString().equals("")) { cv.put("point_id", Integer.parseInt(pointIdStr.toString())); }
        // POINT_NAME
        cv.put("point_name", positionNameValue.getText().toString());
        // RECOGNITION_TYPE_ID
        if(!recognitionTypeIdStr.toString().equals("")) { cv.put("recognition_type_id", Integer.parseInt(recognitionTypeIdStr.toString())); }
        // RESULT
        cv.put("result", resultValue.getText().toString());
        // UNIT
        cv.put("unit_id", unitLst.indexOf(unitValue.getText().toString()) + 1);
        // RECOGNITION_TIME
        cv.put("recognition_time", recognitionTimeValue.getText().toString());
        // ALARM_STATUS
        cv.put("alarm_status", isHaveAlarm ? 1 : 0);
        // ALARM_TYPE_ID
        if(isHaveAlarm) {
            cv.put("alarm_type_id", alarmTypeLst.indexOf(alarmTypeValue.getText().toString()) + 1);
            // ALARM_DESCRIPTION
            cv.put("alarm_description", alarmDescValue.getText().toString());
            // IMAGE
            cv.put("image", photoName);
            // VIDEO
            cv.put("video", videoPath);
        }

        insertResult = db.insert("datas", null, cv);
        if(insertResult != -1) {
            System.out.println("数据库保存data成功");

            // 初始化reports
            if(FLAG.equals("0")) {
                if(!myApplication.REPORT_ID_INITIALED) {
                    myApplication.REPORT_ID_INITIALED = true;

                    // 为当前用户创建 report_id(reports表)
                    cv.clear();
                    insertResult = -1;

                    cv.put("time", "");
                    cv.put("is_reported", 0);

                    insertResult = db.insert("reports", null, cv);
                    if(insertResult != -1) {
                        System.out.println("REPORTS表插入数据成功");

                        // 创建user_report 关联数据
                        // 获取 report_id
                        Cursor cr3 = sqliteUtil.GetLastData(db, "reports");
                        if(cr3.getCount() != 0 && cr3.moveToNext()) {
                            myApplication.REPORT_ID = cr3.getInt(cr3.getColumnIndex("id"));
                            System.out.println("report_id: " + String.valueOf(myApplication.REPORT_ID));
                            cr3.close();

                            cv.clear();
                            insertResult = -1;

                            cv.put("user_id", myApplication.USER_ID);
                            cv.put("report_id", myApplication.REPORT_ID);

                            insertResult = db.insert("user_report", null, cv);
                            if(insertResult != -1) {
                                System.out.println("USER_REPORT表插入数据成功");

                                // 创建data_report 关联数据
                                // 获取data_id
                                Cursor cr4 = sqliteUtil.GetLastData(db, "datas");
                                if(cr4.getCount() != 0 && cr4.moveToNext()) {
                                    int data_id = cr4.getInt(cr4.getColumnIndex("id"));
                                    System.out.println("data_id: " + String.valueOf(data_id));
                                    cr4.close();

                                    cv.clear();
                                    insertResult = -1;

                                    cv.put("data_id", data_id);
                                    cv.put("report_id", myApplication.REPORT_ID);

                                    insertResult = db.insert("data_report", null, cv);
                                    if(insertResult != -1) {
                                        System.out.println("DATA_REPORT表插入数据成功");
                                        cv.clear();
                                        cv = null;
                                    } else { ToastUtils.show("data_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                                } else { ToastUtils.show("获取data_id失败"); }
                            } else { ToastUtils.show("user_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                        } else { ToastUtils.show("获取report_id失败"); }
                    } else { ToastUtils.show("reports插入数据失败(" + String.valueOf(insertResult) + ")"); }
                } else {
                    Cursor cursor = sqliteUtil.GetLastData(db, "datas");

                    if(cursor.getCount() != 0 && cursor.moveToNext()) {
                        int data_id = cursor.getInt(cursor.getColumnIndex("id"));
                        System.out.println("data_id: " + String.valueOf(data_id));
                        cursor.close();

                        cv.clear();
                        insertResult = -1;

                        cv.put("data_id", data_id);
                        cv.put("report_id", myApplication.REPORT_ID);

                        insertResult = db.insert("data_report", null, cv);
                        if(insertResult != -1) {
                            System.out.println("DATA_REPORT表插入数据成功");
                            cv.clear();
                            cv = null;
                        } else { ToastUtils.show("data_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                    } else { ToastUtils.show("获取data_id错误"); }
                }
            }
            // 新添加 数据
            else if(FLAG.equals("1")) {
                // 新添data_report 关联
                Cursor cr5 = sqliteUtil.GetLastData(db, "datas");
                if(cr5.getCount() != 0 && cr5.moveToNext()) {
                    int data_id = cr5.getInt(cr5.getColumnIndex("id"));
                    System.out.println("data_id: " + String.valueOf(data_id));
                    cr5.close();

                    cv.clear();
                    insertResult = -1;

                    cv.put("data_id", data_id);

                    // TODO.用户有未上报 数据，且 新 添加 数据(初始化并获得 myApplication.REPORT_ID)
                    // TODO.这里是否需要 添加过滤? 同一个点位名 再次添加新的数据 填报，或修改成相同 点位
                    // TODO.待测试(目前处理方法：未上报旧数据不可修改)
                    if(myApplication.UNREPORT_DATA_ID_LST.size() > 0) {
                        if(!myApplication.UNREPORT_NEW_DATA_INITIALED) {
                            myApplication.UNREPORT_NEW_DATA_INITIALED = true;
                            System.out.println("用户有未上报数据，且新'添加'数据(初始化myApplication.REPORT_ID)");

                            // 初始化reports表
                            ContentValues cv2 = new ContentValues();
                            cv2.put("time", "");
                            cv2.put("is_reported", 0);

                            long insr = -1;
                            insr = db.insert("reports", null, cv2);
                            if(insr != -1) {
                                Cursor cr = sqliteUtil.GetLastData(db, "reports");

                                if(cr.getCount() != 0 && cr.moveToNext()) {
                                    myApplication.REPORT_ID = cr.getInt(cr.getColumnIndex("id"));
                                    System.out.println("REPORT_ID: " + String.valueOf(myApplication.REPORT_ID));
                                    cr.close();

                                    cv2.clear();
                                    insr = -1;

                                    cv2.put("user_id", myApplication.USER_ID);
                                    cv2.put("report_id", myApplication.REPORT_ID);

                                    // 关联 user_report表
                                    insr = db.insert("user_report", null, cv2);
                                    if(insr != -1) {
                                        cv2.clear();
                                        insr = -1;

                                        // TODO.user_report关联成功之后，将REPORT_ID插入到data_report表，及 UNREPORT_ID_LST
                                        cv.put("report_id", myApplication.REPORT_ID);

                                        myApplication.UNREPORT_ID_LST.add(myApplication.REPORT_ID);
                                        myApplication.UNREPORT_DATA_ID_LST.add(data_id);

                                    } else { ToastUtils.show("user_report插入数据失败(" + String.valueOf(insr) + ")"); }
                                } else { ToastUtils.show("获取report_id错误");   }
                            } else { ToastUtils.show("reports插入数据失败(" + String.valueOf(insr) + ")"); }
                        }
                        // myApplication.REPORT_ID已初始化，正常添加即可
                        else {
                            cv.put("report_id", myApplication.REPORT_ID);
                            myApplication.UNREPORT_DATA_ID_LST.add(data_id);
                        }
                    }
                    // 正常 新添加 数据
                    else { cv.put("report_id", myApplication.REPORT_ID); }

                    insertResult = db.insert("data_report", null, cv);
                    if(insertResult != -1) {
                        System.out.println("DATA_REPORT表插入数据成功");
                        cv.clear();
                        cv = null;
                    } else { ToastUtils.show("data_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                } else { ToastUtils.show("获取data_id错误"); }
            }
        } else { ToastUtils.show("数据库保存数据失败(" + String.valueOf(insertResult) + ")"); }
    }
    // 获取 device_id 和其他相关项
    private void getDeviceId(StringBuffer deviceIdStr, StringBuffer deviceNameStr, StringBuffer pointIdStr, StringBuffer recognitionTypeIdStr) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": getDeviceId()");

        CUR_AREA_ID = areasLst.indexOf(areaValue.getText().toString()) + 1;

        // 根据 area_id 和 name 获取 device_type_id
        Cursor cr = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(CUR_AREA_ID) + " AND name = '" + deviceTypeValue.getText().toString() + "';", null);
        if(cr.getCount() != 0 && cr.moveToNext()) {
            int deviceTypeId = -1;
            deviceTypeId     = cr.getInt(cr.getColumnIndex("id"));
            cr.close();

            // 根据 device_type_id 和 point_name 获取 point_id、device_name
            if(deviceTypeId != -1) {
                CUR_DEVICE_TYPE_ID = deviceTypeId;

                Cursor cr2 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(CUR_DEVICE_TYPE_ID) + " AND name = '" + positionNameValue.getText().toString() + "';", null);

                if(cr2.getCount() != 0 && cr2.moveToNext()) {
                    int pointId           = -1;
                    int recognitionTypeId = -1;

                    pointId           = cr2.getInt(cr2.getColumnIndex("id"));
                    deviceNameStr     = deviceNameStr.append(cr2.getString(cr2.getColumnIndex("device_name")));

                    Cursor c = db.rawQuery("SELECT id FROM recognition_type WHERE name = '" + recognitionTypeValue.getText().toString() + "';", null);
                    if(c.getCount() != 0 && c.moveToNext()) {
                        recognitionTypeId = c.getInt(c.getColumnIndex("id"));
                    } else { ToastUtils.show("获取识别类型id错误"); }
                    c.close();

                    cr2.close();

                    if(pointId != -1) {
                        CUR_POINT_ID = pointId;

                        deviceIdStr = deviceIdStr.append(String.valueOf(deviceTypeId)).append(String.valueOf(pointId));
                        pointIdStr  = pointIdStr.append(String.valueOf(pointId));
                    }
                    if(recognitionTypeId != -1) {
                        recognitionTypeIdStr = recognitionTypeIdStr.append(String.valueOf(recognitionTypeId));
                    }
                } else { ToastUtils.show("获取point_id错误"); }
            } else { ToastUtils.show("获取device_type_id失败"); }
        } else { ToastUtils.show("1111获取设备类型失败"); }
    }
    // 保存 设备信息
    private void saveDevice() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": saveDevice()");

        // 判断 当前 区域集 是否 已经初始化
        if(myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID) != null) {
            // 判断 当前 区域 设备类型集 是否已经初始化
            if(myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID).get((Integer)CUR_DEVICE_TYPE_ID) != null) {
                // 添加 并 保存点位数据
                myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID).get((Integer)CUR_DEVICE_TYPE_ID).add((Integer)CUR_POINT_ID);
            } else {
                // 初始化 当前区域 第一个设备类型集 并 添加保存 点位数据
                List<Integer> pointIdLst = new ArrayList<>();
                pointIdLst.add((Integer)CUR_POINT_ID);
                myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID).put((Integer)CUR_DEVICE_TYPE_ID, pointIdLst);
            }
        } else {
            List<Integer> pointIdLst = new ArrayList<>();
            pointIdLst.add((Integer)CUR_POINT_ID);

            Map<Integer, List<Integer>> deviceTypeMap = new HashMap<>();
            deviceTypeMap.put((Integer)CUR_DEVICE_TYPE_ID, pointIdLst);

            myApplication.SAVED_IDS.put((Integer)CUR_AREA_ID, deviceTypeMap);
        }

        System.out.println("------已保存数据------");
        Map<Integer, Map<Integer, List<Integer>>> m = myApplication.SAVED_IDS;
        for(Integer key : m.keySet()) {
            Map<Integer, List<Integer>> m2 = m.get(key);

            for(Integer key2 : m2.keySet()) {
                List<Integer> lst = m2.get(key2);

                String str = "";
                for(int n = 0; n < lst.size(); n++) { str += String.valueOf(lst.get(n)) + " "; }
                str = str.trim();

                System.out.println("[" + String.valueOf(key) + ", " + "[" + String.valueOf(key2) + ", [" + str + "]]]");
            }
            System.out.println("***************");
        }
        System.out.println("--------------------");
    }

    // 判断 区域 是否发生改变
    private Map<String, Object> isAreaChanged(int ntd) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": isAreaChanged()");

        Map<String, Object> m = new HashMap<>();

        Cursor cr = db.rawQuery("SELECT area_id FROM device_type WHERE id = " + String.valueOf(ntd) + ";", null);
        if(cr.getCount() != 0 && cr.moveToNext()) {
            int next_aera_id = cr.getInt(cr.getColumnIndex("area_id"));
            m.put("ned", next_aera_id);

            if(CUR_AREA_ID != next_aera_id) {
                m.put("changed", true);
                return m;
            }
            cr.close();
        } else { ToastUtils.show("获取next area_id错误"); }

        m.put("changed", false);

        return m;
    }
    // 判断 设备类型 是否发生改变
    private Map<String, Object> isDeviceTypeChanged() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": isDeviceTypeChanged()");

        Map<String, Object> m = new HashMap<>();

        int ntd   = -1;
        Cursor cr = db.rawQuery("SELECT device_type_id FROM points WHERE id = " + String.valueOf(CUR_POINT_ID + 1) + ";", null);

        if(cr.getCount() != 0 && cr.moveToNext()) {
            ntd = cr.getInt(cr.getColumnIndex("device_type_id"));
            cr.close();

            m.put("ntd", ntd);

            if(CUR_DEVICE_TYPE_ID != ntd) {
                m.put("changed", true);
                return  m;
            }
        } else { ToastUtils.show("获取next device_type_id错误"); }

        m.put("changed", false);

        return m;
    }
    // 区域集不变，更新 设备类型 及 点位
    private void removeSavedData(int ned) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": removeSavedData()");

        // 更新 设备类型
        deviceTypeLst.clear();
        Cursor cr = db.rawQuery("SELECT * FROM device_type WHERE area_id = " + String.valueOf(ned) + ";", null);
        if(cr.getCount() != 0) {
            while(cr.moveToNext()) { deviceTypeLst.add(cr.getString(cr.getColumnIndex("name"))); }
            cr.close();
        } else { ToastUtils.show("获取next区域设备类型总数错误"); }

        // 更新 点位
        positionNameLst.clear();
        Cursor cr2 = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(ned) + " AND name = '" + deviceTypeLst.get(0) + "';", null);
        if(cr2.getCount() != 0 && cr2.moveToNext()) {
            Cursor cr3 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(cr2.getInt(cr2.getColumnIndex("id"))) + ";", null);
            cr2.close();

            if(cr3.getCount() != 0 && cr3.moveToNext()) {
                while(cr3.moveToNext()) { positionNameLst.add(cr3.getString(cr3.getColumnIndex("name"))); }
                cr3.close();
            } else { ToastUtils.show("获取next区域第1个设备类型点位总数错误"); }
        } else { ToastUtils.show("获取next区域第1个设备类型id错误"); }
    }
    // 重置 Select List 数据
    private void resetSelectList() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": resetSelectList()");

        deviceTypeLst.clear();
        Cursor cr = db.rawQuery("SELECT * FROM device_type WHERE area_id = 1", null);
        if(cr.getCount() != 0) {
            while(cr.moveToNext()) { deviceTypeLst.add(cr.getString(cr.getColumnIndex("name"))); }
            cr.close();
        } else { ToastUtils.show("获取区域(id=1)设备类型总数错误"); }

        positionNameLst.clear();
        Cursor cr2 = db.rawQuery("SELECT * FROM points WHERE device_type_id = 1", null);
        if(cr2.getCount() != 0) {
            while(cr2.moveToNext()) { positionNameLst.add(cr2.getString(cr2.getColumnIndex("name"))); }
            cr2.close();
        } else { ToastUtils.show("获取区域(id=1)设备类型(id=1)点位总数错误"); }
    }
    // 过滤 positionNameLst
    private void filterPositionNameLst(int ntd) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": filterPositionNameLst()");

        positionNameLst.clear();
        Cursor cr = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(ntd), null);
        if(cr.getCount() != 0) {
            while(cr.moveToNext()) { positionNameLst.add(cr.getString(cr.getColumnIndex("name"))); }
            cr.close();

            if(myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID).get(ntd) != null) {
                List<Integer> savedPointIds = myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID).get(ntd);

                List<String> savedPointNames = new ArrayList<String>();
                for(int i = 0; i < savedPointIds.size(); i++) {
                    Cursor cr2 = db.rawQuery("SELECT name FROM points WHERE id = " + String.valueOf(savedPointIds.get(i)) + ";", null);
                    if(cr2.getCount() != 0 && cr2.moveToNext()) {
                        savedPointNames.add(cr2.getString(cr2.getColumnIndex("name")));
                    }
                    cr2.close();
                }

                positionNameLst.removeAll(savedPointNames);
                selectListAdapter.notifyDataSetChanged();
            }
        } else { ToastUtils.show("获取next设备类型点位总数错误"); }
    }
    // 移除已填报的 点位
    private void removeSavedDeviceFromSelectList() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": removeSavedDeviceFromSelectList()");

        // 获取 next device_type_id(ntd)，先判断(cpd + 1)是否会越界
        Cursor cr = sqliteUtil.GetLastData(db, "points");
        if(cr.getCount() != 0 && cr.moveToNext()) {
            int last_point_id = cr.getInt(cr.getColumnIndex("id"));
            System.out.println("last_point_id: " + String.valueOf(last_point_id));
            cr.close();

            // 越界，重新筛选
            if(CUR_POINT_ID + 1 > last_point_id) {
                // cpd已是 当前区域 当前设备类型 最后一个点位，devicesTypeLst更新为第一个区域，第一个 设备类型集，positionNameLst 第一个设备类型下的 点位集
                resetSelectList();
            }
            // 未越界
            else {
                Map<String, Object> paras_m = new HashMap<>();
                paras_m = isDeviceTypeChanged();

                // 设备类型 变了
                if((boolean)paras_m.get("changed")) {
                    Map<Integer, List<Integer>> map = myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID);

                    Map<String, Object> paras_m2 = new HashMap<>();
                    paras_m2 = isAreaChanged((Integer)paras_m.get("ntd"));

                    // 判断区域是否发生改变
                    // 区域 变了
                    if((boolean)paras_m2.get("changed")) {
                        // 更新设备类型，点位
                        removeSavedData((Integer)paras_m2.get("ned"));
                    }
                    // 区域 未变
                    else {
                        // 判断当前区域 当前设备类型 当前点位 是否填满
                        List<Integer> savedPointsIds = map.get((Integer)CUR_DEVICE_TYPE_ID);

                        Cursor cr4 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(CUR_DEVICE_TYPE_ID) + ";", null);
                        if(cr4.getCount() != 0 && cr4.moveToNext()) {
                            // 当前设备类型，当前点位 未填满，点位更新
                            if(savedPointsIds.size() >= cr4.getCount()) {
                                cr4.close();
                                // 移除 当前 设备类型
                                if(deviceTypeLst.remove(deviceTypeValue.getText().toString())) { System.out.println("当前已保存设备类型移除成功"); }
                                else { ToastUtils.show("当前已保存设备类型移除失败"); }
                            }
                            filterPositionNameLst((int)paras_m.get("ntd"));
                        } else { ToastUtils.show("获取当前区域，当前设备类型点位总数错误"); }
                    }
                }
                // 设备类型 没变，正常移除 当前 已保存点位
                else {
                    if(positionNameLst.remove(positionNameValue.getText().toString())) { System.out.println("当前已保存点位移除成功"); }
                    else { ToastUtils.show("当前已保存点位移除失败"); }
                }
            }
        } else { ToastUtils.show("获取points表last_point_id错误"); }
    }

    // 修改 更新(remove) 原点位
    private void modifyUpdateOldPointId(int old_ced, int old_ctd, int old_cpd) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": modifyUpdateOldPointId()");

        Map<Integer, List<Integer>> map = myApplication.SAVED_IDS.get((Integer)old_ced);

        if(map.keySet().size() > 1) {
            if(map.get((Integer)old_ctd).size() > 1) {
                if(myApplication.SAVED_IDS.get((Integer)old_ced).get((Integer)old_ctd).remove((Integer)old_cpd)) {
                    System.out.println("移除点位成功");
                } else { ToastUtils.show("移除点位失败"); }
            } else {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if(myApplication.SAVED_IDS.get((Integer)old_ced).remove((Integer)old_ctd, (List<Integer>)myApplication.SAVED_IDS.get((Integer)old_ced).get((Integer)old_ctd))) {
                        System.out.println("移除原设备类型成功(点位总数=1)");
                    } else { ToastUtils.show("移除原设备类型失败(点位总数=1)"); }
                }
            }
        } else {
            if(map.get((Integer)old_ctd).size() > 1) {
                if(myApplication.SAVED_IDS.get((Integer)old_ced).get((Integer)old_ctd).remove((Integer)old_cpd)) {
                    System.out.println("移除点位成功");
                } else { ToastUtils.show("移除点位失败"); }
            } else {
                // 移除整个 区域
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if(myApplication.SAVED_IDS.remove((Integer)old_ced, (Map<Integer, List<Integer>>)myApplication.SAVED_IDS.get((Integer)old_ced))) {
                        System.out.println("移除整个区域成功(数据size=1)");
                    } else { ToastUtils.show("移除整个区域失败(数据size=1)"); }
                }
            }
        }
    }
    // TODO.修改操作 更新 SAVED_IDS
    private void modifyUpdateSavedIDs() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": modifyUpdateSavedIDs()");

        // 点位信息
        int ced = areasLst.indexOf(areaValue.getText().toString()) + 1;

        int ctd = -1;
        Cursor c = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(ced) + " AND name = '" + deviceTypeValue.getText().toString() + "';", null);

        if(c.getCount() != 0 && c.moveToNext()) {
            ctd = c.getInt(c.getColumnIndex("id"));
        } else { ToastUtils.show("获取设备类型id错误" ); }
        c.close();

        int cpd = -1;
        Cursor c2 = db.rawQuery("SELECT id FROM points WHERE device_type_id = " + String.valueOf(ctd) + " AND name = '" + positionNameValue.getText().toString() + "';", null);

        if(c2.getCount() != 0 && c2.moveToNext()) {
            cpd = c2.getInt(c2.getColumnIndex("id"));
        } else { ToastUtils.show("获取点位id错误"); }
        c2.close();

        // 获取 原点位信息
        int old_ced = areasLst.indexOf(MODIFY_AREA_BEFORE) + 1;

        int old_ctd = -1;
        Cursor c3 = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(old_ced) + " AND name = '" + MODIFY_DEVICE_TYPE_BEFORE + "';", null);

        if(c3.getCount() != 0 && c3.moveToNext()) {
            old_ctd = c3.getInt(c3.getColumnIndex("id"));
            c3.close();
        } else { ToastUtils.show("获取原设备类型id错误" ); }
        c3.close();

        int old_cpd = -1;
        Cursor c4 = db.rawQuery("SELECT id FROM points WHERE device_type_id = " + String.valueOf(old_ctd) + " AND name = '" + MODIFY_POSITION_BEFORE + "';", null);

        if(c4.getCount() != 0 && c4.moveToNext()) {
            old_cpd = c4.getInt(c4.getColumnIndex("id"));
        } else { ToastUtils.show("获取原点位id错误"); }
        c4.close();

        System.out.println("------SAVED_IDS更新前------");
        Map<Integer, Map<Integer, List<Integer>>> m = myApplication.SAVED_IDS;
        for(Integer key : m.keySet()) {
            Map<Integer, List<Integer>> m2 = m.get(key);

            for(Integer key2 : m2.keySet()) {
                List<Integer> lst = m2.get(key2);

                String str = "";
                for(int n = 0; n < lst.size(); n++) { str += String.valueOf(lst.get(n)) + " "; }
                str = str.trim();

                System.out.println("[" + String.valueOf(key) + ", " + "[" + String.valueOf(key2) + ", [" + str + "]]]");
            }
            System.out.println("***************");
        }
        System.out.println("--------------------");

        // 区域改变
        if(!areaValue.getText().toString().equals(MODIFY_AREA_BEFORE)) {
            // 条件 移除 原点位
            modifyUpdateOldPointId(old_ced, old_ctd, old_cpd);

            // 更改的 区域 填报过
            if(myApplication.SAVED_IDS.get((Integer)ced) != null) {
                // 更改的 设备类型 填报过
                if(myApplication.SAVED_IDS.get((Integer)ced).get((Integer)ctd) != null) {
                    // 保存 更改的点位
                    if(myApplication.SAVED_IDS.get((Integer)ced).get((Integer)ctd).add((Integer)cpd)) {
                        System.out.println("保存新修改点位id成功");
                    } else { ToastUtils.show("保存新修改点位id失败"); }
                }
                // 更改区域 更改的设备类型 未填报过
                else {
                    List<Integer> pointIdLst = new ArrayList<>();
                    pointIdLst.add((Integer)cpd);
                    myApplication.SAVED_IDS.get((Integer)ced).put((Integer)ctd, pointIdLst);
                }
            }
            // 修改为 未填报过的 新区域
            else {
                // 保存
                List<Integer> pointIdLst = new ArrayList<>();
                pointIdLst.add((Integer)cpd);

                Map<Integer, List<Integer>> deviceTypeMap = new HashMap<>();
                deviceTypeMap.put((Integer)ctd, pointIdLst);

                myApplication.SAVED_IDS.put((Integer)ced, deviceTypeMap);
            }
        }
        // 未改变(ced = old_ced)
        else {
            System.out.println("区域未改变");

            // 设备类型改变
            if(!deviceTypeValue.getText().toString().equals(MODIFY_DEVICE_TYPE_BEFORE)) {
                System.out.println("设备类型改变");

                modifyUpdateOldPointId(old_ced, old_ctd, old_cpd);

                // 保存
                // 更改的 设备类型 填报过
                if(myApplication.SAVED_IDS.get((Integer)ced).get((Integer)ctd) != null) {
                    System.out.println("改变的设备类型 填报过");

                    if(myApplication.SAVED_IDS.get((Integer)old_ced).get((Integer)ctd).add((Integer)cpd)) {
                        System.out.println("保存同区域不同设备类型点位成功");
                    } else { ToastUtils.show("保存同区域不同设备类型点位失败"); }
                }
                // 修改为 未填报过的 新设备类型
                else {
                    System.out.println("改变的设备类型 未填报过");

                    List<Integer> pointIdLst = new ArrayList<>();
                    pointIdLst.add((Integer)cpd);
                    myApplication.SAVED_IDS.get((Integer)old_ced).put((Integer)ctd, pointIdLst);
                }
            }
            // 未改变
            else {
                System.out.println("设备类型未改变");

                // 点位改变
                if(!positionNameValue.getText().toString().equals(MODIFY_POSITION_BEFORE)) {
                    System.out.println("点位改变");

                    // 移除 原保存 点位id
                    if(myApplication.SAVED_IDS.get((Integer)old_ced).get((Integer)old_ctd).remove((Integer)old_cpd)) {
                        System.out.println("移除原点位id成功");
                    } else { ToastUtils.show("移除原点位id失败"); }

                    // 保存 新修改的 点位id
                    if(myApplication.SAVED_IDS.get((Integer)old_ced).get((Integer)old_ctd).add((Integer)cpd)) {
                        System.out.println("保存新修改点位id成功");
                    } else { ToastUtils.show("保存新修改点位id失败"); }
                }
                // 未改变
                else { System.out.println("点位未改变"); }
            }
        }

        System.out.println("------SAVED_IDS更新后------");
        Map<Integer, Map<Integer, List<Integer>>> m_ = myApplication.SAVED_IDS;
        for(Integer key : m_.keySet()) {
            Map<Integer, List<Integer>> m2 = m_.get(key);

            for(Integer key2 : m2.keySet()) {
                List<Integer> lst = m2.get(key2);

                String str = "";
                for(int n = 0; n < lst.size(); n++) { str += String.valueOf(lst.get(n)) + " "; }
                str = str.trim();

                System.out.println("[" + String.valueOf(key) + ", " + "[" + String.valueOf(key2) + ", [" + str + "]]]");
            }
            System.out.println("***************");
        }
        System.out.println("--------------------");
    }

    // 获取 下一个 点位
    private Map<String, Object> checkNextPoint() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": checkNextPoint()");

        Map<String, Object> m = new HashMap<String, Object>();
        m.put("have_next", true);

        // 从第当前 区域开始 过滤(TODO.break写的可能有点问题)
        for(int i = CUR_AREA_ID; i < areasLst.size() + 1; i++) {
            Map<Integer, List<Integer>> map = myApplication.SAVED_IDS.get((Integer)i);

            Cursor c = db.rawQuery("SELECT * FROM device_type WHERE area_id = " + String.valueOf(i) + ";", null);

            if(c.getCount() != 0 && c.moveToNext()) {
                // 当前(i)区域 保存填报 设备类型 未满，继续 筛选
                if(map.size() < c.getCount()) {
                    // 从当前 已填报的设备类型 开始判断，其 点位是否填满
                    int dtd = CUR_DEVICE_TYPE_ID;

                    for(Integer key : map.keySet()) {
                        if((int)key == dtd) {
                            Cursor c2 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(dtd) + ";", null);
                            if(c2.getCount() != 0 && c2.moveToNext()) {
                                List<Integer> savedPointIds = myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID).get((Integer)dtd);

                                // 未填满
                                if(savedPointIds.size() < c2.getCount()) {
                                    // 获取 下一个点位
                                    getNextDataInfo(1, i, dtd, savedPointIds, c2, m);
                                    c2.close();
                                    return m;
                                }
                                // 填满(下一个 设备类型)
                                else {
                                    dtd += 1;
                                    continue;
                                }
                            } else { ToastUtils.show("获取当前已填报设备类型点位总数错误"); }
                        } else { continue;   }
                    }

                    // 当前 已填报设备类型 全部填满，返回 下一个(未填报过的) 设备类型 第1个 点位 信息
                    // 求差集 获取未填报 设备类型集
                    List<Integer> savedDevicesTypeIds  = new ArrayList<>();
                    for(Integer key : map.keySet()) { savedDevicesTypeIds.add(key); }

                    List<Integer> allDevicesTypeIds = new ArrayList<>();
                    while(c.moveToNext()) { allDevicesTypeIds.add(c.getInt(c.getColumnIndex("id"))); }
                    c.close();

                    allDevicesTypeIds.removeAll(savedDevicesTypeIds);
                    Cursor cursor = db.rawQuery("SELECT * FROM device_type WHERE id = " + String.valueOf(allDevicesTypeIds.get(0)) + ";", null);

                    if(cursor.getCount() != 0 && cursor.moveToNext()) {
                        Cursor cursor2 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(allDevicesTypeIds.get(0) + ";"), null);
                        // 只选 第一条 点位 名称即可
                        if(cursor2.getCount() != 0 && cursor2.moveToNext()) {
                            m.put("area", areaValue.getText().toString());
                            m.put("device_type", cursor.getString(cursor.getColumnIndex("name")));
                            m.put("point_name", cursor2.getString(cursor2.getColumnIndex("name")));

                            cursor2.close();
                            cursor.close();

                            return m;
                        } else { ToastUtils.show("获取点位错误"); }
                    } else { ToastUtils.show("获取设备类型错误"); }
                }
                // 当前 区域 设备类型 已填满，判断 每个设备 类型下 的点位是否填满
                else {
                    int dtd = CUR_DEVICE_TYPE_ID;

                    for(Integer key : map.keySet()) {
                        if((int)key == dtd) {
                            Cursor c2 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(dtd) + ";", null);
                            if(c2.getCount() != 0 && c2.moveToNext()) {
                                List<Integer> savedPointIds = myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID).get((Integer)dtd);

                                // 未填满
                                if(savedPointIds.size() < c2.getCount()) {
                                    // 获取 下一个点位
                                    getNextDataInfo(1, i, dtd, savedPointIds, c2, m);
                                    c2.close();
                                    return m;
                                }
                                // 填满(下一个 设备类型)
                                else {
                                    dtd += 1;
                                    continue;
                                }
                            } else { ToastUtils.show("获取当前已填报设备类型点位总数错误"); }
                        } else { continue;   }
                    }

                    continue;
                }
            } else {
                ToastUtils.show("查询当前区域(" + String.valueOf(i) + ")设备类型size错误");
                return m;
            }
        }

        // 当前 电站 设备已全部填满，没有 下一个填报项
        m.put("have_next", false);
        m.put("area", "#1机组零米室内");
        m.put("device_type", "空压机A");
        m.put("point_name", "空压机A");

        return m;
    }
    // 获取 下一项 填报信息
    private void getNextDataInfo(int flag, int area_id, Integer deviceTypeId, List<Integer> lst, Cursor c, Map<String, Object> m) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": getNextDataInfo()");

        switch(flag) {
            case 1:
                // 求差集 获取未填报 点位集
                List<Integer> allPointIds = new ArrayList<>();
                while(c.moveToNext()) { allPointIds.add(c.getInt(c.getColumnIndex("id"))); }
                c.close();

                allPointIds.removeAll(lst);

                Cursor cursor = db.rawQuery("SELECT * FROM points WHERE id = " + String.valueOf(allPointIds.get(0)) + ";", null);
                if(cursor.getCount() != 0 && cursor.moveToNext()) {
                    Cursor cursor2 = db.rawQuery("SELECT name FROM device_type WHERE id = " + String.valueOf(deviceTypeId) + ";", null);

                    String deviceTypeName = null;
                    if(cursor2.getCount() != 0 && cursor2.moveToNext()) {
                        deviceTypeName = cursor2.getString(cursor2.getColumnIndex("name"));
                    } else { ToastUtils.show("获取设备类型名错误"); }
                    cursor2.close();

                    m.put("area", areasLst.get(area_id - 1));
                    m.put("device_type", deviceTypeName);
                    m.put("point_name", cursor.getString(cursor.getColumnIndex("name")));
                } else { ToastUtils.show("获取点位错误"); }
                cursor.close();

                break;

            default: break;
        }
    }

    // 区域选择
    private void onDeviceTypeSelect() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDeviceTypeSelect()");

        SELECT_TYPE = 1;
        loadingAction(50, "正在加载数据，请稍候...");

        if(deviceTypeLst.size() > 0) {
            new Handler().postDelayed(() -> {
                selectIndex = deviceTypeLst.indexOf(deviceTypeValue.getText().toString());

                SelectBlockItemListInitial(deviceTypeLst, "选择设备类型", 1);
                loadingLayout.setVisibility(View.GONE);
            }, 500);
        } else {
            loadingLayout.setVisibility(View.GONE);
            ToastUtils.show("加载设备类型数据异常");
        }
    }
    // 点位选择
    private void onPositionNameSelect() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onPositionNameSelect()");

        SELECT_TYPE = 2;
        loadingAction(50, "正在加载数据，请稍候...");

        if(positionNameLst.size() > 0) {
            new Handler().postDelayed(() -> {
                selectIndex = positionNameLst.indexOf(positionNameValue.getText().toString());

                SelectBlockItemListInitial(positionNameLst, "选择点位名称", 2);
                loadingLayout.setVisibility(View.GONE);
            }, 500);
        } else {
            loadingLayout.setVisibility(View.GONE);
            ToastUtils.show("加载点位数据异常");
        }
    }
    // Table 栏点击监听
    @SuppressLint("NonConstantResourceId")
    public void onColumnClick(View view) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onColumnClick()");

        switch(view.getId()) {
            case R.id.area_select_layout:
                areaSelectItemLayout.setVisibility(View.VISIBLE);
                break;

            case R.id.device_type_select_layout:
                // 重新进入页面 添加 新数据时，不能先选 设备类型
                if(FLAG.equals("1")) {
                    if(!areaValue.getText().toString().equals("")) { onDeviceTypeSelect(); }
                    else { ToastUtils.show("请先选择区域"); }
                } else { onDeviceTypeSelect(); }

                break;

            case R.id.position_name_select_layout:
                // 重新进入页面 添加 新数据时，不能先选 点位
                if(FLAG.equals("1")) {
                    if(!positionNameValue.getText().toString().equals("")) { onPositionNameSelect(); }
                    else { ToastUtils.show("清先选择区域"); }
                } else { onPositionNameSelect(); }

                break;

            case R.id.recognition_type_select_layout:
                SELECT_TYPE = 3;

                selectListAdapter = new SelectListAdapter(this, recognitionTypeLst);
                selectItemList.setAdapter(selectListAdapter);

                selectTitle.setText("选择识别类型");
                selectIndex = recognitionTypeLst.indexOf(recognitionTypeValue.getText().toString());
                selectItemLayout.setVisibility(View.VISIBLE);

                break;

            case R.id.unit_select_layout:
                SELECT_TYPE = 4;

                selectListAdapter = new SelectListAdapter(this, unitLst);
                selectItemList.setAdapter(selectListAdapter);

                selectTitle.setText("选择单位");
                selectIndex = unitLst.indexOf(unitValue.getText().toString());
                selectItemLayout.setVisibility(View.VISIBLE);

                break;

            case R.id.recognition_time_select_layout:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm.isActive()) {
                    // 强制隐藏 软键盘
                    View focv = MainActivity.this.getCurrentFocus();
                    if(focv != null) { imm.hideSoftInputFromWindow(focv.getWindowToken(), 0); }
                }

                @SuppressLint("SetTextI18n") TimePickerView v = new TimePickerView.Builder(this, (date, view1) -> runOnUiThread(() -> {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                    String timeStr       = sdf.format(date);
                    String[] data        = timeStr.split("-");

                    recognitionTimeValue.setText(data[0] + "年" + data[1] + "月" + data[2] + "日" + data[3] + "时" + data[4] + "分" + data[5] + "秒");
                })).setType(new boolean[]{true, true, true, true, true, true})
                   .setDate(Calendar.getInstance())
                   .build();
                v.show();

                break;

            case R.id.alarm_type_select_layout:
                SELECT_TYPE = 5;

                selectListAdapter = new SelectListAdapter(this, alarmTypeLst);
                selectItemList.setAdapter(selectListAdapter);

                selectTitle.setText("选择告警类型");
                selectIndex = alarmTypeLst.indexOf(alarmTypeValue.getText().toString());
                selectItemLayout.setVisibility(View.VISIBLE);

                break;

            default: break;
        }
    }

    // 获取 未填满设备类型 ids
    private List<Integer> getUnFilledDeviceTypeIds(Map<Integer, List<Integer>> m, List<Integer> lst) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": getUnFilledDeviceTypeIds()");

        for(Integer id : lst) {
            @SuppressLint("Recycle") Cursor c = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(id) + ";", null);
            if(c.getCount() != 0 && c.moveToNext()) {
                // 当前 设备类型 点位 未填满
                if(m.get(id).size() < c.getCount()) { continue; }
                // 填满
                else { lst.remove(id); }
            } else { ToastUtils.show("获取当前(" + String.valueOf(id) + ")" + "设备类型点位总数错误"); }
        }

        return lst;
    }
    // Update deviceTypeLst
    private void updateDeviceTypeLst(int ced) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": updateDeviceTypeLst(" + String.valueOf(ced) +")");

        if(ced != CUR_AREA_ID) { CUR_AREA_ID = ced; }

        deviceTypeLst.clear();

        Map<Integer, List<Integer>> map  = myApplication.SAVED_IDS.get((Integer)ced);
        List<Integer> allDeviceTypeIds   = new ArrayList<Integer>();
        List<Integer> savedDeviceTypeIds = new ArrayList<Integer>();

        if(map != null) {
            Cursor c = db.rawQuery("SELECT * FROM device_type WHERE area_id = " + String.valueOf(ced) + ";", null);

            if(c.getCount() != 0) {
                while(c.moveToNext()) { allDeviceTypeIds.add((Integer)c.getInt(c.getColumnIndex("id"))); }
                c.close();

                for(Integer key : map.keySet()) { savedDeviceTypeIds.add(key); }

                List<Integer> finalDeviceTypeIds = new ArrayList<>();

                // 有未填报的 设备类型，获取未填报的 设备类型
                if(allDeviceTypeIds.size() > savedDeviceTypeIds.size()) {
                    allDeviceTypeIds.removeAll(savedDeviceTypeIds);

                    // 未填报的设备类型 + 已填报但未填满的设备类型
                    allDeviceTypeIds.addAll(getUnFilledDeviceTypeIds(map, savedDeviceTypeIds));
                    Collections.sort(allDeviceTypeIds);

                    finalDeviceTypeIds = allDeviceTypeIds;
                }
                // 当前区域 设备类型 已全部填录，逐个判断 每个设备类型 点位 是否填满，获取未填满的 设备类型id
                else { finalDeviceTypeIds = getUnFilledDeviceTypeIds(map, savedDeviceTypeIds); }

                for(Integer v : finalDeviceTypeIds) {
                    Cursor c2 = db.rawQuery("SELECT name FROM device_type WHERE id = " + String.valueOf(v) + ";", null);
                    if(c2.getCount() != 0 && c2.moveToNext()) {
                        deviceTypeLst.add(c2.getString(c2.getColumnIndex("name")));
                    } else { ToastUtils.show("获取当前设备类型(" + String.valueOf(v) + ")name错误"); }

                    c2.close();
                }

                runOnUiThread(() -> deviceTypeValue.setText(deviceTypeLst.get(0)));
            } else { ToastUtils.show("获取当前区域(" + String.valueOf(ced) + ")设备类型总数错误"); }
        }
    }
    // Update positionNameLst
    private void updatePositionNameLst(int ced) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": updatePositionNameLst(" + String.valueOf(ced) + ")");

        // 更新 点位 并过滤
        positionNameLst.clear();

        Map<Integer, List<Integer>> map = myApplication.SAVED_IDS.get((Integer)ced);

        if(map != null) {
            Cursor c = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(ced) + " AND name = '" + deviceTypeValue.getText().toString() + "';", null);
            if(c.getCount() != 0 && c.moveToNext()) {
                int ctd = c.getInt(c.getColumnIndex("id"));
                c.close();

                // 求差集
                List<Integer> savedPointsIds = map.get((Integer)ctd);
                Cursor c2 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(ctd) + ";", null);

                if(c2.getCount() != 0) {
                    List<Integer> allPointIds = new ArrayList<>();
                    while(c2.moveToNext()) { allPointIds.add((Integer)c2.getInt(c2.getColumnIndex("id"))); }
                    c2.close();

                    allPointIds.removeAll(savedPointsIds);
                    for(int i = 0; i < allPointIds.size(); i++) {
                        @SuppressLint("Recycle") Cursor c3 = db.rawQuery("SELECT name FROM points WHERE id = " + String.valueOf(allPointIds.get(i)) + ";", null);
                        if(c3.getCount() != 0 && c3.moveToNext()) {
                            positionNameLst.add(c3.getString(c3.getColumnIndex("name")));
                        } else { ToastUtils.show("获取点位名错误"); }
                    }

                    runOnUiThread(() -> positionNameValue.setText(positionNameLst.get(0)));
                } else { ToastUtils.show("获取ctd点位集错误"); }
            } else { ToastUtils.show("获取设备类型id错误"); }
        }
    }
    // 区域选择Layout初始化
    @SuppressLint("UseCompatLoadingForDrawables")
    private void areaSelectListInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": areaSelectListInitial()");

        // 6大区域
        //for(int i = 0; i < 6; ++i) {
        for(int i = 0; i < areasLst.size(); ++i) {
            LinearLayout linearLayout        = new LinearLayout(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);

            linearLayout.setLayoutParams(params);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setTag("AREA_ITEM_" + String.valueOf(i));

            if(i % 2 == 0) { linearLayout.setBackgroundColor(Color.WHITE); }

            // RadioButton
            RadioButton radioBtn                     = new RadioButton(this);
            LinearLayout.LayoutParams radioBtnParams = new LinearLayout.LayoutParams(32, 32);
            radioBtnParams.setMarginStart(24);
            radioBtnParams.gravity = Gravity.CENTER;
            radioBtn.setLayoutParams(radioBtnParams);

            radioBtn.setTextSize(0);
            radioBtn.setButtonDrawable(null);
            radioBtn.setButtonDrawable(getResources().getDrawable(R.drawable.radio_btn_selector));
            radioBtn.setGravity(Gravity.CENTER_VERTICAL);
            radioBtn.setChecked(false);

            radioBtnLst.add(radioBtn);

            // TextView
            TextView textView                        = new TextView(this);
            LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            textViewParams.setMarginStart(12);
            textViewParams.gravity = Gravity.CENTER;
            textViewParams.setLayoutDirection(LayoutDirection.INHERIT);
            textView.setLayoutParams(textViewParams);

            textView.setText(areasLst.get(i).toString());
            textView.setTextSize(16);
            textView.setTextColor(Color.BLACK);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            textView.setGravity(Gravity.CENTER_VERTICAL);

            linearLayout.addView(radioBtn);
            linearLayout.addView(textView);

            linearLayout.setOnClickListener(view -> {
                String tag = view.getTag().toString();
                int curIdx = Integer.parseInt(tag.substring(10, tag.length()));

                runOnUiThread(() -> {
                    radioBtnLst.get(curIdx).setChecked(true);

                    for(int j = 0; j < radioBtnLst.size(); ++j) {
                        if(curIdx != j) { radioBtnLst.get(j).setChecked(false); }
                    }

                    areaValue.setText(areasLst.get(curIdx).toString());

                    if(myApplication.SAVED_IDS.get((Integer)(curIdx + 1)) != null) {
                        /// 根据当前 区域 更新并过滤 设备类型 与 点位
                        updateDeviceTypeLst(curIdx + 1);   // 设备类型
                        updatePositionNameLst(curIdx + 1); // 点位
                    }
                    // 手动选择 新区域
                    else {
                        deviceTypeLst.clear();
                        Cursor c = db.rawQuery("SELECT * FROM device_type WHERE area_id = " + String.valueOf(curIdx + 1) + ";", null);
                        if(c.getCount() != 0) {
                            while(c.moveToNext()) { deviceTypeLst.add(c.getString(c.getColumnIndex("name"))); }
                            c.close();
                        } else { ToastUtils.show("获取设备类型错误"); }

                        runOnUiThread(() -> deviceTypeValue.setText(deviceTypeLst.get(0)));

                        positionNameLst.clear();
                        Cursor c2 = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(curIdx + 1) + " AND name = '" + deviceTypeValue.getText().toString() + "';", null);
                        if(c2.getCount() != 0 && c2.moveToNext()) {
                            int ctd = c2.getInt(c2.getColumnIndex("id"));
                            c2.close();

                            Cursor c3 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(ctd) + ";", null);
                            if(c3.getCount() != 0) {
                                while(c3.moveToNext()) { positionNameLst.add(c3.getString(c3.getColumnIndex("name"))); }
                                c3.close();
                            } else { ToastUtils.show("获取点位错误"); }
                        } else { ToastUtils.show("获取ctd错误"); }

                        runOnUiThread(() -> positionNameValue.setText(positionNameLst.get(0)));
                    }

                    areaSelectItemLayout.setVisibility(View.GONE);
                });
            });

            areaSelectList.addView(linearLayout);
        }
    }
    // Select Block选项列表动态初始化
    @SuppressLint({"ClickableViewAccessibility", "UseCompatLoadingForDrawables"})
    private void SelectBlockItemListInitial(List<String> lst, String title, int flag) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": selectBlockItemListInitial()");

        // 每项item 70，默认 10[marginTop] + 70[title] + 350[lst显示5个可见项] + 10[footer]
        // Root
        RelativeLayout rl              = new RelativeLayout(this);
        RelativeLayout.LayoutParams rp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        rl.setLayoutParams(rp);
        rl.setElevation(0);
        rl.setBackgroundColor(Color.parseColor("#55000000"));
        rl.setOnTouchListener((v, e) -> {
            v.performClick();
            rl.setVisibility(View.GONE);
            return true;
        });

        // Container
        RelativeLayout cl              = new RelativeLayout(this);
        RelativeLayout.LayoutParams cp = new RelativeLayout.LayoutParams(640, lst.size() >= 5 ? 500 : 150 + 70 * lst.size());
        cp.addRule(RelativeLayout.CENTER_IN_PARENT, rl.getId());

        cl.setLayoutParams(cp);
        cl.setBackground(getResources().getDrawable(R.drawable.select_block_style));
        cl.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // Title
        TextView tv                    = new TextView(this);
        RelativeLayout.LayoutParams tp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);
        tp.addRule(RelativeLayout.ALIGN_PARENT_TOP, cl.getId());
        tp.addRule(RelativeLayout.CENTER_HORIZONTAL, cl.getId());
        tp.topMargin = 10;

        tv.setId(tv.getId());
        tv.setLayoutParams(tp);
        tv.setGravity(Gravity.CENTER);
        tv.setText(title);
        tv.setTextSize(17);
        tv.setTextColor(Color.BLACK);
        tv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // ListView
        ListView lv                     = new ListView(this);
        RelativeLayout.LayoutParams lvp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, lst.size() >= 5 ? 350 : 70 * lst.size());
        lvp.addRule(RelativeLayout.BELOW, tv.getId());
        lvp.topMargin = 80;

        lv.setId(lv.getId());
        lv.setLayoutParams(lvp);
        lv.setDivider(null);
        lv.setBackgroundColor(Color.parseColor("#00ffffff"));

        selectListAdapter = new SelectListAdapter(MainActivity.this, lst);
        lv.setAdapter(selectListAdapter);

        // Item Click
        lv.setOnItemClickListener((adapterView, v, position, id) -> runOnUiThread(() -> {
            selectIndex = position;

            switch(flag) {
                // 选择 设备类型
                case 1:
                    deviceTypeValue.setText(selectListAdapter.valuesLst.get(position));

                    // 更新 点位
                    positionNameLst.clear();
                    int cur_area_id    = areasLst.indexOf(areaValue.getText().toString()) + 1;
                    int device_type_id = -1;

                    Cursor cr = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(cur_area_id) + " AND name = '" + deviceTypeValue.getText().toString() + "';", null);

                    if(cr.getCount() != 0 && cr.moveToNext()) {
                        device_type_id = cr.getInt(cr.getColumnIndex("id"));
                        cr.close();

                        if(device_type_id != -1) {
                            if(device_type_id != CUR_DEVICE_TYPE_ID) { CUR_DEVICE_TYPE_ID = device_type_id; }

                            Cursor cr2 = db.rawQuery("SELECT name FROM points WHERE device_type_id = " + String.valueOf(device_type_id) + ";", null);
                            while(cr2.getCount() != 0 && cr2.moveToNext()) { positionNameLst.add(cr2.getString(cr2.getColumnIndex("name"))); }
                            cr2.close();

                            // 过滤
                            // 当前 设备类型 已初始化(未初始化，则不用过滤)
                            if(myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID).get((Integer)device_type_id) != null) {
                                List<Integer> savedPointIds = myApplication.SAVED_IDS.get((Integer)CUR_AREA_ID).get((Integer)device_type_id);

                                List<String> savedPointNames = new ArrayList<String>();
                                for(int i = 0; i < savedPointIds.size(); i++) {
                                    Cursor cr3 = db.rawQuery("SELECT name FROM points WHERE id = " + String.valueOf(savedPointIds.get(i)) + ";", null);
                                    if(cr3.getCount() != 0 && cr3.moveToNext()) {
                                        savedPointNames.add(cr3.getString(cr3.getColumnIndex("name")));
                                    }
                                    cr3.close();
                                }

                                positionNameLst.removeAll(savedPointNames);
                                selectListAdapter.notifyDataSetChanged();
                            }

                            // 更新 点位显示(默认显示第一个)
                            positionNameValue.setText(positionNameLst.get(0));
                        } else { ToastUtils.show("获取device_type_id(-1)失败"); }
                    } else { ToastUtils.show("获取device_type_id错误"); }

                    break;

                // 选择点位
                case 2: positionNameValue.setText(selectListAdapter.valuesLst.get(position)); break;

                default: break;
            }

            rl.setVisibility(View.GONE);
        }));

        // Footer
        TextView fv                    = new TextView(this);
        RelativeLayout.LayoutParams fp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);
        fp.addRule(RelativeLayout.BELOW, lv.getId());
        fp.topMargin = 430;

        fv.setLayoutParams(fp);
        fv.setBackground(getResources().getDrawable(R.drawable.select_block_footer_style));

        // Add
        cl.addView(tv);
        cl.addView(lv);
        cl.addView(fv);

        rl.addView(cl);

        rootLayout.addView(rl);
    }

    // 删除 照片或视频
    @SuppressLint("NonConstantResourceId")
    public void onDelete(View v) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDelete()");

        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setTitle("删除").create();

        switch(v.getId()) {
            case R.id.photo_delete_icon:
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                    loadingAction(50, "正在删除照片，请稍候...");

                    // 删除图片并更新信息
                    File f = null;
                    if(DATA_INDEX != -1) {
                        if(!myApplication.DATALST.get(DATA_INDEX).getPhoto().equals("")) {
                            f = new File(myApplication.DATALST.get(DATA_INDEX).getPhoto());
                        } else { f = new File(photoName); }
                    } else { f = new File(photoName); }

                    if(f.delete()) {
                        new Handler().postDelayed(() -> {
                            runOnUiThread(() -> {
                                captureImageView.setImageBitmap(null);
                                captureImageView.setBackgroundResource(R.drawable.capture_icon);

                                photoDeleteIcon.setVisibility(View.GONE);

                                if(DATA_INDEX != -1) { myApplication.DATALST.get(DATA_INDEX).setPhoto(""); }
                                else { photoName = ""; }
                            });

                            isCaptured = false;
                            loadingAction(100, "照片已删除");

                            new Handler().postDelayed(() -> {
                                loadingLayout.setVisibility(View.GONE);
                            }, 500);
                        }, 1000);
                    } else { ToastUtils.show("未知错误，应用文件夹照片删除失败"); }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {dialog.dismiss();});
                dialog.setMessage("确定删除当前照片吗？");
                dialog.show();

                break;

            case R.id.video_delete_icon:
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                    loadingAction(50, "正在删除视频，请稍候...");

                    File f = null;
                    if(DATA_INDEX != -1) {
                        if(!myApplication.DATALST.get(DATA_INDEX).getVideo().equals("")) {
                            f = new File(myApplication.DATALST.get(DATA_INDEX).getVideo());
                        } else { f = new File(videoPath); }
                    } else { f = new File(videoPath); }

                    if(f.delete()) {
                        new Handler().postDelayed(() -> {
                            runOnUiThread(() -> {
                                videoImageView.setImageBitmap(null);
                                videoImageView.setBackgroundResource(R.drawable.video_icon);

                                videoDeleteIcon.setVisibility(View.GONE);

                                if(DATA_INDEX != -1) { myApplication.DATALST.get(DATA_INDEX).setVideo(""); }
                                else { videoPath = ""; }
                            });

                            isVideoed = false;
                            loadingAction(100, "视频已删除");

                            new Handler().postDelayed(() -> {
                                loadingLayout.setVisibility(View.GONE);
                            }, 500);
                        }, 1000);
                    } else { ToastUtils.show("未知错误，应用文件夹视频删除失败"); }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {dialog.dismiss();});
                dialog.setMessage("确定删除当前视频吗？");
                dialog.show();

                break;

            default: break;
        }
    }

    // 获取视频缩略图
    private Bitmap getVideoThumbnailBmp(int kind) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": getVideoThumbnailBmp()");

        Bitmap bmp = null;
        MediaMetadataRetriever mr = new MediaMetadataRetriever();

        try {
            if(DATA_INDEX != -1) {
                if(!myApplication.DATALST.get(DATA_INDEX).getVideo().equals("")) {
                    mr.setDataSource(myApplication.DATALST.get(DATA_INDEX).getVideo());
                } else { mr.setDataSource(videoPath); }
            } else { mr.setDataSource(videoPath); }

            bmp = mr.getFrameAtTime(-1);
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            ToastUtils.show("获取视频缩略图异常:" + e.getMessage());
        } finally {
            try { mr.release(); }
            catch(RuntimeException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                ToastUtils.show("获取视频缩略图异常:" + e.getMessage());
            }
        }

        if(bmp == null) { return null; }

        if(kind == MediaStore.Images.Thumbnails.MINI_KIND) {
            int w   = bmp.getWidth();
            int h   = bmp.getHeight();
            int max = Math.max(w, h);

            if(max > 512) {
                float scale = 512f / max;
                int w_      = Math.round(scale * w);
                int h_      = Math.round(scale * h);
                bmp         = Bitmap.createScaledBitmap(bmp, w_, h_, true);
            }
        } else if(kind == MediaStore.Images.Thumbnails.MICRO_KIND) {
            bmp = ThumbnailUtils.extractThumbnail(bmp, 85, 60, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
        }

        return bmp;
    }

    // LoadingLayout显示与隐藏
    private void loadingAction(int step, String note) {
        loadingLayout.setProgressStep(step);
        loadingLayout.setProgressNote(note);
        loadingLayout.setVisibility(View.VISIBLE);
    }

    /***********************************Activity Override Start***********************************/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onActivityResult(" + requestCode + "," + " " + requestCode + "," + " " + data + ")");
        // 打开 相机 拍照或录像
        if(requestCode == CAMERA_CAPTURE) {
            // 保存照片
            if(resultCode == RESULT_OK) {
                runOnUiThread(() -> loadingAction(50, "拍照成功，正在保存图片，请稍候..."));

                new Thread(() -> {
                    try {
                        photoBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(captureImageUri));

                        if(photoBitmap != null) {
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                            Date date            = new Date(System.currentTimeMillis());
                            String timeFlag      = sdf.format(date);

                            photoName      = myApplication.APP_IMAGES_PATH + File.separator + timeFlag + ".jpg";
                            File photoFile = new File(photoName);

                            try {
                                FileOutputStream fos = new FileOutputStream(photoFile);

                                photoBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fos);
                                fos.flush();
                                fos.close();

                                new Handler(Looper.getMainLooper()).postDelayed(() -> loadingAction(100, "照片保存成功"), 500);

                                runOnUiThread(() -> {
                                    captureImageView.setImageBitmap(photoBitmap);
                                    photoDeleteIcon.setVisibility(View.VISIBLE);
                                });
                                isCaptured = true;

                                // 删除 Android/data/packagename/cache/ 下缓存照片
                                System.out.println(captureImageUri.getPath());

                                String cfPath = "/storage/emulated/0/Android/data/" + getPackageName() + "/cache/" + cachePhotoTime + ".jpg";
                                System.out.println(cfPath);
                                File f = new File(cfPath);

                                if(f.exists()) {
                                    if(f.delete()) { System.out.println("缓存照片删除成功"); }
                                    else { ToastUtils.show("缓存照片删除失败"); }
                                } else { ToastUtils.show("缓存照片文件出错"); }

                                new Handler(Looper.getMainLooper()).postDelayed(() -> loadingLayout.setVisibility(View.GONE), 500);
                            } catch(IOException e) {
                                System.out.println(e.getMessage());
                                e.printStackTrace();

                                runOnUiThread(() -> loadingLayout.setVisibility(View.GONE));
                                Looper.prepare();
                                ToastUtils.show("照片保存到应用文件夹失败:" + e.getMessage());
                            }
                        } else {
                            runOnUiThread(() -> loadingLayout.setVisibility(View.GONE));
                            Looper.prepare();
                            ToastUtils.show("bitmap=null，照片保存失败");
                        }
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        e.printStackTrace();

                        runOnUiThread(() -> loadingLayout.setVisibility(View.GONE));
                        Looper.prepare();
                        ToastUtils.show("Exception，照片保存失败");
                    }
                }).start();
            } else { ToastUtils.show("拍照取消或失败"); }
        }
        if(requestCode == CAMERA_VIDEO) {
            // 保存视频
            if(resultCode == RESULT_OK) {
                runOnUiThread(() -> loadingAction(50, "拍摄成功，正在保存视频，请稍候..."));

                new Thread(() -> {
                    // 视频文件拷贝至data目录下
                    try {
                        FileInputStream fis  = new FileInputStream("/storage/emulated/0/Android/data/" + getPackageName() + "/cache/" + cacheVideoTime + ".mp4");

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                        Date date            = new Date(System.currentTimeMillis());
                        String timeFlag      = sdf.format(date);

                        videoPath      = myApplication.APP_VIDEOS_PATH + File.separator + timeFlag + ".mp4";
                        File videoFile = new File(videoPath);

                        FileOutputStream fos = new FileOutputStream(videoFile);

                        byte[] buf = new byte[1024];
                        int len    = -1;

                        while(-1 != (len = fis.read(buf))) { fos.write(buf, 0, len); }

                        fis.close();
                        fos.flush();
                        fos.close();

                        // 获取视频缩略图
                        Bitmap videoThumbnailBmp = getVideoThumbnailBmp(MediaStore.Images.Thumbnails.MINI_KIND);

                        if(videoThumbnailBmp != null) {
                            new Handler(Looper.getMainLooper()).postDelayed(() -> loadingAction(100, "视频保存成功"), 500);

                            runOnUiThread(() -> {
                                videoImageView.setImageBitmap(videoThumbnailBmp);
                                videoDeleteIcon.setVisibility(View.VISIBLE);
                            });
                            isVideoed = true;

                            // 删除 Android/data/packagename/cache/ 下缓存视频
                            System.out.println(captureVideoUri.getPath());

                            String cfPath = "/storage/emulated/0/Android/data/" + getPackageName() + "/cache/" + cacheVideoTime + ".mp4";
                            System.out.println(cfPath);
                            File f = new File(cfPath);

                            if(f.exists()) {
                                if(f.delete()) { System.out.println("缓存视频删除成功"); }
                                else { ToastUtils.show("缓存视频删除失败"); }
                            } else { ToastUtils.show("缓存视频文件出错"); }

                            new Handler(Looper.getMainLooper()).postDelayed(() -> loadingLayout.setVisibility(View.GONE), 500);
                        } else { ToastUtils.show("获取视频缩略图失败"); }
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        e.printStackTrace();
                        ToastUtils.show("保存视频异常:" + e.getMessage());
                    }
                }).start();

                ToastUtils.show("拍摄完成");
            } else { ToastUtils.show("未知错误，拍摄失败"); }
        }
    }
    @Override
    protected void onStart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStart()");
        super.onStart();
    }
    @Override
    protected void onResume() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onResume()");
        super.onResume();
    }
    @Override
    protected void onPause() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onPause()");
        super.onPause();

        MediaPlayerManager.instance().pause();
    }
    @Override
    protected void onStop() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStop()");
        super.onStop();
    }
    @Override
    protected void onRestart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onRestart()");
        super.onRestart();
    }
    @Override
    protected void onDestroy() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDestroy()");
        super.onDestroy();

        MediaPlayerManager.instance().releasePlayerAndView(this);
        // 关闭数据库
        if(db != null && db.isOpen()) { db.close(); }
    }
    /***********************************Activity Override End***********************************/

    // 返回键 监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(isVideoPreviewShow) {
                MediaPlayerManager.instance().releasePlayerAndView(this);

                runOnUiThread(() -> {
                    videoFileName.setText("");
                    videoPreviewLayout.setVisibility(View.GONE);
                });

                isVideoPreviewShow = false;
            }
        }

        return false;
    }
}
