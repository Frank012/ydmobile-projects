package photoview;

public interface OnScaleChangedListener { void onScaleChange(float scaleFactor, float focusX, float focusY); }