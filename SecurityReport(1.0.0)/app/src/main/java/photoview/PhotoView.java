package photoview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.Shape;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatImageView;

@SuppressWarnings("unused")
public class PhotoView extends AppCompatImageView {
    private PhotoViewAttacher attacher;
    private ScaleType pendingScaleType;

    // Constructor
    public PhotoView(Context context) { this(context, null); }
    public PhotoView(Context context, AttributeSet attr) { this(context, attr, 0); }

    public PhotoView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        init();
    }

    private void init() {
        attacher = new PhotoViewAttacher(this);
        //We always pose as a Matrix scale type, though we can change to another scale type via the attacher
        super.setScaleType(ScaleType.MATRIX);

        //apply the previously applied scale type
        if (pendingScaleType != null) {
            setScaleType(pendingScaleType);
            pendingScaleType = null;
        }
    }

    public PhotoViewAttacher getAttacher() { return attacher; }

    // Resources
    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        // setImageBitmap calls through to this method
        if (attacher != null) { attacher.update(); }
    }
    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        if (attacher != null) { attacher.update(); }
    }
    @Override
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        if (attacher != null) { attacher.update(); }
    }
    @Override
    protected boolean setFrame(int l, int t, int r, int b) {
        boolean changed = super.setFrame(l, t, r, b);
        if (changed) { attacher.update(); }

        return changed;
    }

    // Rotation
    public void setRotationTo(float rotationDegree) { attacher.setRotationTo(rotationDegree); }
    public void setRotationBy(float rotationDegree) { attacher.setRotationBy(rotationDegree); }

    // Zoomable
    public boolean isZoomable() { return attacher.isZoomable(); }
    public void setZoomable(boolean zoomable) { attacher.setZoomable(zoomable); }

    // Matrix
    @Override
    public Matrix getImageMatrix() { return attacher.getImageMatrix(); }
    @SuppressWarnings("UnusedReturnValue") public boolean setDisplayMatrix(Matrix finalRectangle) { return attacher.setDisplayMatrix(finalRectangle); }
    public void getSuppMatrix(Matrix matrix) { attacher.getSuppMatrix(matrix); }
    public boolean setSuppMatrix(Matrix matrix) { return attacher.setDisplayMatrix(matrix); }
    public void getDisplayMatrix(Matrix matrix) { attacher.getDisplayMatrix(matrix); }

    // Listener
    @Override
    public void setOnLongClickListener(OnLongClickListener l) { attacher.setOnLongClickListener(l); }
    @Override
    public void setOnClickListener(OnClickListener l) { attacher.setOnClickListener(l); }

    public void setOnViewTapListener(OnViewTapListener listener) { attacher.setOnViewTapListener(listener); }
    public void setOnViewDragListener(OnViewDragListener listener) { attacher.setOnViewDragListener(listener); }
    public void setOnPhotoTapListener(OnPhotoTapListener listener) { attacher.setOnPhotoTapListener(listener); }

    public void setOnMatrixChangeListener(OnMatrixChangedListener listener) {
        attacher.setOnMatrixChangeListener(listener);
    }
    public void setOnOutsidePhotoTapListener(OnOutsidePhotoTapListener listener) {
        attacher.setOnOutsidePhotoTapListener(listener);
    }
    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
        attacher.setOnDoubleTapListener(onDoubleTapListener);
    }
    public void setOnScaleChangeListener(OnScaleChangedListener onScaleChangedListener) {
        attacher.setOnScaleChangeListener(onScaleChangedListener);
    }
    public void setOnSingleFlingListener(OnSingleFlingListener onSingleFlingListener) {
        attacher.setOnSingleFlingListener(onSingleFlingListener);
    }

    // Scale
    @Override
    public ScaleType getScaleType() { return attacher.getScaleType(); }
    @Override
    public void setScaleType(ScaleType scaleType) {
        if (attacher == null) { pendingScaleType = scaleType; }
        else { attacher.setScaleType(scaleType); }
    }
    public float getScale() { return attacher.getScale(); }
    public void setScale(float scale) { attacher.setScale(scale); }
    public void setScale(float scale, boolean animate) { attacher.setScale(scale, animate); }
    public void setScale(float scale, float focalX, float focalY, boolean animate) {
        attacher.setScale(scale, focalX, focalY, animate);
    }
    public void setScaleLevels(float minimumScale, float mediumScale, float maximumScale) {
        attacher.setScaleLevels(minimumScale, mediumScale, maximumScale);
    }

    public float getMinimumScale() { return attacher.getMinimumScale(); }
    public float getMediumScale() { return attacher.getMediumScale(); }
    public float getMaximumScale() { return attacher.getMaximumScale(); }
    public void setMinimumScale(float minimumScale) { attacher.setMinimumScale(minimumScale); }
    public void setMediumScale(float mediumScale) { attacher.setMediumScale(mediumScale); }
    public void setMaximumScale(float maximumScale) { attacher.setMaximumScale(maximumScale); }

    // Other
    public RectF getDisplayRect() { return attacher.getDisplayRect(); }
    public void setAllowParentInterceptOnEdge(boolean allow) { attacher.setAllowParentInterceptOnEdge(allow); }
    public void setZoomTransitionDuration(int milliseconds) { attacher.setZoomTransitionDuration(milliseconds); }
}