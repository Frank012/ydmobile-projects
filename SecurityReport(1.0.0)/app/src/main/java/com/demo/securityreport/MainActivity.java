package com.demo.securityreport;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.LayoutDirection;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;

import com.bigkoo.pickerview.TimePickerView;
import com.hjq.toast.ToastUtils;

import org.salient.artplayer.MediaPlayerManager;
import org.salient.artplayer.VideoView;
import org.salient.artplayer.ui.ControlPanel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import photoview.PhotoView;
import utils.MyApplication;
import utils.SqliteUtil;

public class MainActivity extends Activity {
    private static String FLAG    = null;
    private static int DATA_INDEX = -1;

    // Utils
    private SqliteUtil sqliteUtil = null;

    // 请求码
    private static final int CAMERA_CAPTURE = 5;
    private static final int CAMERA_VIDEO   = 6;

    // Variables
    private MyApplication myApplication = null;
    private static final String Log_Tag = "MainActivity";

    private boolean isDBOpened = false;
    private SQLiteDatabase db  = null;

    private String cachePhotoTime = null;
    private String cacheVideoTime = null;
    private boolean isMenuClick   = false;

    // 测试数据
    public List<String> areasLst               = new ArrayList<String>();
    public List<String> workersLst             = new ArrayList<String>();
    public List<String> unDisciplinedLevelsLst = new ArrayList<String>();

    public String photoName = "";
    public String videoPath = "";

    // 拍照、视频
    private Uri captureImageUri = null;
    private Uri captureVideoUri = null;

    private Bitmap photoBitmap = null;

    private boolean isCaptured = false;
    private boolean isVideoed  = false;

    private TextView areaValue   = null;
    private TextView typeValue   = null;
    private TextView peopleValue = null;
    private TextView timeValue   = null;
    private TextView infoValue   = null;

    private RelativeLayout selectItemLayout = null;

    public int selectIndex          = -1;
    private ListView selectItemList = null;

    private TextView selectTitle = null;

    // 对话框
    private RelativeLayout dialogBlock  = null;
    private TextView dialogTitle        = null;
    private TextView dialogMessage      = null;
    private TextView dialogOKBtn        = null;
    private TextView dialogCancelBtn    = null;

    // Capture & Video
    private RelativeLayout captureLayout = null;
    private RelativeLayout videoLayout   = null;

    private ImageView captureImageView = null;
    private ImageView videoImageView   = null;

    private TextView captureNote = null;
    private TextView videoNote   = null;

    private ImageView photoDeleteIcon = null;
    private ImageView videoDeleteIcon = null;

    // 预览
    private RelativeLayout imagePreviewLayout = null;
    private RelativeLayout videoPreviewLayout = null;

    private ImageView videoPreviewBack = null;
    private TextView videoFileName     = null;

    private boolean isVideoPreviewShow = false;

    private PhotoView photoPreview = null;
    private VideoView videoPreview = null;

    private Button saveBtn = null;

    // 修改
    private Button modifySaveBtn   = null;
    private Button modifyCancelBtn = null;

    public LoadingLayout loadingLayout = null;

    // 区域选择
    private final List<RadioButton> radioBtnLst = new ArrayList<>();

    private RelativeLayout areaSelectItemLayout = null;
    private LinearLayout areaSelectList         = null;

    // 违章行为类别选择
    private final List<RadioButton> alarmLevelRadioBtnLst = new ArrayList<>();

    private RelativeLayout alarmLevelSelectItemLayout = null;
    private LinearLayout alarmLevelSelectList         = null;

    // Select List Adapter
    private SelectListAdapter selectListAdapter = null;

    public static class ViewHolder {
        public RadioButton radioBtn;
        public TextView valueTextView;
    }
    ViewHolder viewHolder = null;

    public class SelectListAdapter extends BaseAdapter {
        private LayoutInflater inflater = null;
        private List<String> valuesLst  = null;

        // Constructor
        public SelectListAdapter(Context context, List<String> list) {
            this.inflater  = LayoutInflater.from(context);
            this.valuesLst = list;
        }

        @Override
        public int getCount() { return valuesLst.size(); }
        @Override
        public Object getItem(int position) { return position; }
        @Override
        public long getItemId(int id) { return id; }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                viewHolder  = new ViewHolder();
                convertView = inflater.inflate(R.layout.select_list_item, null);

                AbsListView.LayoutParams param = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);
                convertView.setLayoutParams(param);

                viewHolder.radioBtn      = convertView.findViewById(R.id.radio_btn);
                viewHolder.radioBtn.setClickable(false);

                viewHolder.valueTextView = convertView.findViewById(R.id.value_text_view);

                convertView.setTag(viewHolder);

                if(position % 2 == 0) { convertView.setBackgroundColor(Color.WHITE); }
            } else { viewHolder = (ViewHolder)convertView.getTag(); }

            viewHolder.radioBtn.setChecked(selectIndex == position);
            viewHolder.valueTextView.setText(valuesLst.get(position));

            return convertView;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApplication = new MyApplication();
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onCreate()");
        setContentView(R.layout.activity_main);

        sqliteUtil = new SqliteUtil();

        // 数据库初始化
        db = SQLiteDatabase.openDatabase(myApplication.DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
        if(db != null) {
            System.out.println("数据库打开成功");
            isDBOpened = true;
        } else { ToastUtils.show("数据库打开失败"); }
        // 获取数据
        DataInit();

        // 页面操作标识
        FLAG = getIntent().getStringExtra("FLAG");
        System.out.println("FLAG: " + FLAG);
        // 数据操作标识
        DATA_INDEX = getIntent().getIntExtra("DATA_INDEX", -1);
        System.out.println("DATA_INDEX: " + DATA_INDEX);

        // 全屏
        MainActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View view = this.getWindow().getDecorView();
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        // UI初始化
        UIInitial();

        if(DATA_INDEX != -1) {
            areaValue.setText(myApplication.DATALST.get(DATA_INDEX).getArea());
            typeValue.setText(myApplication.DATALST.get(DATA_INDEX).getType());
            peopleValue.setText(myApplication.DATALST.get(DATA_INDEX).getPeople());
            timeValue.setText(myApplication.DATALST.get(DATA_INDEX).getTime());
            infoValue.setText(myApplication.DATALST.get(DATA_INDEX).getInfo());

            // 照片、视频
            if(!myApplication.DATALST.get(DATA_INDEX).getPhoto().equals("")) {
                captureImageView.setImageBitmap(BitmapFactory.decodeFile(myApplication.DATALST.get(DATA_INDEX).getPhoto()));
                photoDeleteIcon.setVisibility(View.VISIBLE);
                isCaptured = true;
            }
            if(!myApplication.DATALST.get(DATA_INDEX).getVideo().equals("")) {
                videoImageView.setImageBitmap(getVideoThumbnailBmp());
                videoDeleteIcon.setVisibility(View.VISIBLE);
                isVideoed = true;
            }
        }
    }

    // Data Init
    private void DataInit() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": DataInit()");

        if(isDBOpened) {
            // 获取区域Areas
            Cursor cr = db.rawQuery("SELECT name FROM areas;", null);
            while(cr.getCount() != 0 && cr.moveToNext()) { areasLst.add(cr.getString(cr.getColumnIndex("name"))); }
            cr.close();
            // 获取员工Workers
            Cursor cr2 = db.rawQuery("SELECT worker_name FROM workers;", null);
            while(cr2.getCount() != 0 && cr2.moveToNext()) { workersLst.add(cr2.getString(cr2.getColumnIndex("worker_name"))); }
            cr2.close();
            // 获取违章行为类型levels
            Cursor cr3 = db.rawQuery("SELECT name FROM types;", null);
            while(cr3.getCount() != 0 && cr3.moveToNext()) { unDisciplinedLevelsLst.add(cr3.getString(cr3.getColumnIndex("name"))); }
            cr3.close();
        }
    }

    // UI Initial
    @SuppressLint({"ClickableViewAccessibility", "UseCompatLoadingForDrawables"})
    private void UIInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": UIInitial()");

        // Title Layout
        // UI
        ImageView menu = findViewById(R.id.menu);
        if(FLAG.equals("1")) { menu.setVisibility(View.VISIBLE); }
        else { menu.setVisibility(View.GONE); }

        menu.setOnClickListener(v -> {
            runOnUiThread(() -> {
                dialogTitle.setText("保存");
                dialogMessage.setText("需要保存已添加信息吗？");
                dialogBlock.setVisibility(View.VISIBLE);
            });

            isMenuClick = true;
        });

        // Operation Note
        TextView operationNote = findViewById(R.id.operation_note);
        if(FLAG.equals("2")) { operationNote.setText("修改"); }

        // Select Layout
        areaValue   = findViewById(R.id.area_value);
        typeValue   = findViewById(R.id.type_value);
        peopleValue = findViewById(R.id.people_value);
        timeValue   = findViewById(R.id.time_value);
        infoValue   = findViewById(R.id.info_value);

        // 区域选择
        areaSelectItemLayout    = findViewById(R.id.area_select_item_layout);
        RelativeLayout areaSelectItemContainer = findViewById(R.id.area_select_item_container);
        areaSelectList          = findViewById(R.id.area_select_list);

        areaSelectItemLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            areaSelectItemLayout.setVisibility(View.GONE);
            return true;
        });
        areaSelectItemContainer.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // 区域选择初始化
        areaSelectListInitial();

        // 违章行为类别选择
        alarmLevelSelectItemLayout    = findViewById(R.id.alarm_level_select_item_layout);
        RelativeLayout alarmLevelSelectItemContainer = findViewById(R.id.alarm_level_select_item_container);
        alarmLevelSelectList          = findViewById(R.id.alarm_level_select_list);

        alarmLevelSelectItemLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            alarmLevelSelectItemLayout.setVisibility(View.GONE);
            return true;
        });
        alarmLevelSelectItemContainer.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // 违章行为类别选择初始化
        alarmLevelSelectListInitial();

        // Select Item Layout
        selectTitle = findViewById(R.id.select_title);

        selectItemLayout = findViewById(R.id.select_item_layout);
        selectItemLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            selectItemLayout.setVisibility(View.GONE);
            return true;
        });

        selectItemList   = findViewById(R.id.select_item_list);
        selectItemList.setOnItemClickListener((adapterView, view, position, id) -> {
            selectIndex = position;
            selectItemLayout.setVisibility(View.GONE);

            peopleValue.setText(selectListAdapter.valuesLst.get(position));
        });

        RelativeLayout selectItemContainer = findViewById(R.id.select_item_container);
        selectItemContainer.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // Capture & Video
        captureImageView = findViewById(R.id.capture_icon);
        videoImageView   = findViewById(R.id.video_icon);

        captureNote = findViewById(R.id.capture_note);
        videoNote   = findViewById(R.id.video_note);

        photoDeleteIcon = findViewById(R.id.photo_delete_icon);
        videoDeleteIcon = findViewById(R.id.video_delete_icon);

        // 拍照
        captureLayout = findViewById(R.id.capture_layout);
        captureLayout.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        captureNote.setTextColor(Color.parseColor("#1e90ff"));
                        captureLayout.setBackgroundColor(Color.parseColor("#dcdcdc"));
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        captureNote.setTextColor(Color.BLACK);
                        captureLayout.setBackgroundColor(Color.TRANSPARENT);
                    });

                    // 未拍照，则 前往拍照页面
                    if(!isCaptured) {
                        ToastUtils.show("拍照");

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                        Date date            = new Date(System.currentTimeMillis());

                        cachePhotoTime    = sdf.format(date);
                        File captureImage = new File(getExternalCacheDir(), cachePhotoTime + ".jpg");

                        if(!captureImage.exists()) {
                            try { if(captureImage.createNewFile()) { System.out.println("创建图片Uri成功"); } }
                            catch(IOException ex) {
                                System.out.println(ex.getMessage());
                                ex.printStackTrace();
                                ToastUtils.show("创建图片Uri失败:" + ex.getMessage());
                            }
                        }

                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            captureImageUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.concat(".provider"), captureImage);
                        } else {
                            captureImageUri = Uri.fromFile(captureImage);
                        }

                        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureImageUri);

                        startActivityForResult(captureIntent, CAMERA_CAPTURE);
                    }
                    // 已拍照，则 查看预览图
                    else {
                        if(DATA_INDEX != -1) {
                            photoPreview.setImageDrawable(new BitmapDrawable(BitmapFactory.decodeFile(myApplication.DATALST.get(DATA_INDEX).getPhoto())));
                        } else { photoPreview.setImageDrawable(new BitmapDrawable(photoBitmap)); }

                        imagePreviewLayout.setVisibility(View.VISIBLE);
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });
        // 拍摄视频
        videoLayout = findViewById(R.id.video_layout);
        videoLayout.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        videoNote.setTextColor(Color.parseColor("#1e90ff"));
                        videoLayout.setBackgroundColor(Color.parseColor("#dcdcdc"));
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        videoNote.setTextColor(Color.BLACK);
                        videoLayout.setBackgroundColor(Color.TRANSPARENT);
                    });

                    if(!isVideoed) {
                        ToastUtils.show("拍摄");

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                        Date date            = new Date(System.currentTimeMillis());

                        cacheVideoTime    = sdf.format(date);
                        File captureVideo = new File(getExternalCacheDir(), cacheVideoTime + ".mp4");

                        if(!captureVideo.exists()) {
                            try { if(captureVideo.createNewFile()) { System.out.println("创建视频Uri成功"); } }
                            catch(IOException ex) {
                                System.out.println(ex.getMessage());
                                ex.printStackTrace();
                                ToastUtils.show("创建视频Uri失败Exception:" + ex.getMessage());
                            }
                        }

                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            captureVideoUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.concat(".provider"), captureVideo);
                        } else {
                            captureVideoUri = Uri.fromFile(captureVideo);
                        }

                        Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        videoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10); // 限制录制时间10秒
                        videoIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureVideoUri);

                        startActivityForResult(videoIntent, CAMERA_VIDEO);
                    } else {
                        // ①调用系统播放器播放视频有两个问题：
                        //   1.必须使用getContentResolver()，播放uri路径下的视频，但是会报没有权限的错误；视频不在sdcard上
                        //   2.使用sdcard路径，会有exposed uri的问题(两者好像互相矛盾)
                        // ②使用videoview的问题，屏幕一直黑屏，暂时找不到原因
                        // ③使用MediaPlayer + SurfaceView 再次点击预览，surfaceview无法清空

                        // 使用 ArtPlayer插件
                        videoPreviewLayout.setVisibility(View.VISIBLE);

                        if(DATA_INDEX != -1) {
                            videoPreview.setUp(myApplication.DATALST.get(DATA_INDEX).getVideo());
                            runOnUiThread(() -> videoFileName.setText(new File(myApplication.DATALST.get(DATA_INDEX).getVideo()).getName()));
                        } else {
                            videoPreview.setUp(videoPath);
                            runOnUiThread(() -> videoFileName.setText(new File(videoPath).getName()));
                        }

                        videoPreview.setControlPanel(new ControlPanel(this));
                        videoPreview.start();

                        isVideoPreviewShow = true;
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        /// 预览
        // 图片
        imagePreviewLayout = findViewById(R.id.image_preview_layout);
        imagePreviewLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            imagePreviewLayout.setVisibility(View.GONE);
            return true;
        });
        photoPreview = findViewById(R.id.photo_preview);

        // 视频
        videoPreviewLayout = findViewById(R.id.video_preview_layout);
        videoPreview       = findViewById(R.id.video_preview);

        videoPreviewBack   = findViewById(R.id.video_preview_back);
        videoFileName      = findViewById(R.id.video_file_name);

        videoPreviewBack.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> videoPreviewBack.setBackgroundColor(Color.parseColor("#dcdcdc")));
                    break;
                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> videoPreviewBack.setBackgroundColor(Color.TRANSPARENT));

                    MediaPlayerManager.instance().releasePlayerAndView(this);

                    runOnUiThread(() -> {
                        videoFileName.setText("");
                        videoPreviewLayout.setVisibility(View.GONE);
                    });

                    isVideoPreviewShow = false;

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 保存
        saveBtn = findViewById(R.id.save_btn);
        if(!FLAG.equals("2")) { saveBtn.setVisibility(View.VISIBLE); }
        else { saveBtn.setVisibility(View.GONE); }

        saveBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        saveBtn.setBackgroundColor(Color.parseColor("#498bdd"));
                        loadingAction(50, "正在保存数据，请稍候...");
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> { saveBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style)); });

                    // 检查数据完整性
                    //if(checkInfoComplete()) {
                        // 保存数据 并 跳转到ReportActivity
                        saveDataToDB();
                        saveDataToReportDataClass();

                        loadingAction(100, "数据保存成功，即将跳转到上报页...");

                        new Handler().postDelayed(() -> {
                            Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                            startActivity(intent);
                            overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                            db.close();
                            loadingLayout.setVisibility(View.GONE);

                            MainActivity.this.finish();
                        }, 500);
                    //} else {
                    //    loadingLayout.setVisibility(View.GONE);
                    //    ToastUtils.show("请填报完整数据信息");
                    //}

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // Modify Layout
        RelativeLayout modifyLayout = findViewById(R.id.modify_layout);
        if(FLAG.equals("2")) { modifyLayout.setVisibility(View.VISIBLE); }
        else { modifyLayout.setVisibility(View.GONE); }

        // 保存 修改
        modifySaveBtn = findViewById(R.id.modify_save_btn);
        modifySaveBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        modifySaveBtn.setBackgroundColor(Color.parseColor("#498bdd"));
                        loadingAction(50, "正在保存数据，请稍候...");
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    // 数据库保存数据
                    ContentValues cv = new ContentValues();
                    int updateResult = -1;

                    cv.put("area", areasLst.indexOf(areaValue.getText().toString()) + 1);
                    cv.put("type", unDisciplinedLevelsLst.indexOf(typeValue.getText().toString()) + 1);
                    cv.put("worker", peopleValue.getText().toString());
                    cv.put("time", timeValue.getText().toString());
                    cv.put("info", infoValue.getText().toString());

                    if(!photoName.equals("")) { cv.put("image", photoName); }
                    if(!videoPath.equals("")) { cv.put("video", videoPath); }

                    // 未上报数据 DATA_INDEX 为实际未上报 数据 data_id
                    if(myApplication.UNREPORT_ID_LST.size() > 0) {
                        updateResult = db.update("datas", cv, "id=?", new String[]{ String.valueOf(myApplication.UNREPORT_DATA_ID_LST.get(DATA_INDEX)) });
                    } else {
                        updateResult = db.update("datas", cv, "id=?", new String[]{ String.valueOf(DATA_INDEX + 1) });
                    }

                    if(updateResult != -1) {
                        System.out.println("DATAS数据更新成功");
                        cv.clear();
                        cv = null;
                    } else { ToastUtils.show("datas数据更新失败(" + String.valueOf(updateResult) + ")"); }

                    // 保存并回到 ReportActivity
                    if(DATA_INDEX != -1) {
                        myApplication.DATALST.get(DATA_INDEX).setArea(areaValue.getText().toString());
                        myApplication.DATALST.get(DATA_INDEX).setType(typeValue.getText().toString());
                        myApplication.DATALST.get(DATA_INDEX).setPeople(peopleValue.getText().toString());
                        myApplication.DATALST.get(DATA_INDEX).setTime(timeValue.getText().toString());
                        myApplication.DATALST.get(DATA_INDEX).setInfo(infoValue.getText().toString());

                        if(!photoName.equals("")) {
                            myApplication.DATALST.get(DATA_INDEX).setPhoto(photoName);
                            photoName = "";
                        }
                        if(!videoPath.equals("")) {
                            myApplication.DATALST.get(DATA_INDEX).setVideo(videoPath);
                            videoPath = "";
                        }
                    } else { ToastUtils.show("未知错误(DATA_INDEX = -1)"); }

                    runOnUiThread(() -> {
                        modifySaveBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style));
                        loadingAction(100, "数据保存成功，即将跳转到上报页...");
                    });

                    new Handler().postDelayed(() -> {
                        Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_slide_out_left, R.anim.activity_slide_in_right);

                        db.close();
                        loadingLayout.setVisibility(View.GONE);

                        MainActivity.this.finish();
                    }, 500);

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });
        // 取消 修改
        modifyCancelBtn = findViewById(R.id.modify_cancel_btn);
        modifyCancelBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> modifyCancelBtn.setBackgroundColor(Color.parseColor("#498bdd")));
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        modifyCancelBtn.setBackground(getResources().getDrawable(R.drawable.cancel_btn_normal_style));

                        /* 显示 对话框
                        dialogTitle.setText("修改取消");
                        dialogMessage.setText("确定放弃修改吗？");
                        dialogBlock.setVisibility(View.VISIBLE);
                        */
                        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setTitle("放弃修改").create();
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                            dialog.dismiss();

                            Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.activity_slide_out_left, R.anim.activity_slide_in_right);

                            db.close();
                            MainActivity.this.finish();
                        });
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {dialog.dismiss();});
                        dialog.setMessage("确定放弃当前修改吗？");
                        dialog.show();
                    });

                    isMenuClick = false;
                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 对话框
        dialogBlock = findViewById(R.id.dialog_block);
        dialogBlock.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });
        RelativeLayout dialogLayout = findViewById(R.id.dialog_layout);
        dialogLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });
        ImageView dialogClose = findViewById(R.id.dialog_close);
        dialogClose.setOnClickListener(v -> dialogBlock.setVisibility(View.GONE));

        dialogTitle   = findViewById(R.id.dialog_title);
        dialogMessage = findViewById(R.id.dialog_message);

        dialogOKBtn = findViewById(R.id.dialog_ok_btn);
        dialogOKBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> dialogOKBtn.setTextColor(Color.parseColor("#498bdd")));
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> { dialogOKBtn.setTextColor(Color.parseColor("#1e90ff")); });

                    if(isMenuClick) {
                        //if(checkInfoComplete()) {
                            loadingAction(50, "正在保存数据，请稍候...");

                            saveDataToDB();
                            saveDataToReportDataClass();

                            new Handler().postDelayed(() -> {
                                loadingAction(100, "数据保存成功，即将跳转到上报页...");

                                new Handler().postDelayed(() -> {
                                    Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

                                    db.close();
                                    loadingLayout.setVisibility(View.GONE);

                                    MainActivity.this.finish();
                                }, 500);
                            }, 1000);
                        //} else {
                        //    loadingLayout.setVisibility(View.GONE);
                        //    ToastUtils.show("请填报完整数据信息");
                        //}
                    } else {
                        Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

                        db.close();
                        dialogBlock.setVisibility(View.GONE);

                        MainActivity.this.finish();
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });
        dialogCancelBtn = findViewById(R.id.dialog_cancel_btn);
        dialogCancelBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> dialogCancelBtn.setTextColor(Color.parseColor("#498bdd")));
                    break;

                case MotionEvent.ACTION_UP:
                    if(isMenuClick) {
                        Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

                        db.close();
                        MainActivity.this.finish();
                    } else {
                        runOnUiThread(() -> {
                            dialogCancelBtn.setTextColor(Color.parseColor("#1e90ff"));
                            dialogBlock.setVisibility(View.GONE);
                        });
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // Loading Layout
        loadingLayout = super.findViewById(R.id.loading_layout);
        loadingLayout.setClickCallback(() -> { /* todo sth.*/ });
    }

    // 检查数据完整性
    private boolean checkInfoComplete() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": checkInfoComplete()");

        if(areaValue.getText().toString().equals("") || typeValue.getText().toString().equals("") ||
           peopleValue.getText().toString().equals("") || timeValue.getText().toString().equals("") ||
           infoValue.getText().toString().equals("") || photoName.equals("")) {
            return false;
        }

        return true;
    }

    // 数据 保存到datas表
    private void saveDataToDB() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": saveDataToDB()");

        ContentValues cv  = new ContentValues();
        long insertResult = -1;

        cv.put("area", areasLst.indexOf(areaValue.getText().toString()) + 1);
        cv.put("type", unDisciplinedLevelsLst.indexOf(typeValue.getText().toString()) + 1);
        cv.put("worker", peopleValue.getText().toString());
        cv.put("time", timeValue.getText().toString());
        cv.put("info", infoValue.getText().toString());
        cv.put("image", photoName);
        cv.put("video", videoPath);

        insertResult = db.insert("datas", null, cv);
        if(insertResult != -1) {
            System.out.println("数据库保存data成功");

            // 初始化reports
            if(FLAG.equals("0")) {
                // 为当前用户创建 report_id(reports表)
                cv.clear();
                insertResult = -1;

                cv.put("time", "");
                cv.put("is_reported", 0);

                insertResult = db.insert("reports", null, cv);
                if(insertResult != -1) {
                    System.out.println("REPORTS表插入数据成功");

                    // 创建user_report 关联数据
                    // 获取 report_id
                    Cursor cr = sqliteUtil.GetLastData(db, "reports");
                    if(cr.getCount() != 0 && cr.moveToNext()) {
                        myApplication.REPORT_ID = cr.getInt(cr.getColumnIndex("id"));
                        System.out.println("report_id: " + String.valueOf(myApplication.REPORT_ID));
                        cr.close();

                        cv.clear();
                        insertResult = -1;

                        cv.put("user_id", myApplication.USER_ID);
                        cv.put("report_id", myApplication.REPORT_ID);

                        insertResult = db.insert("user_report", null, cv);
                        if(insertResult != -1) {
                            System.out.println("USER_REPORT表插入数据成功");

                            // 创建data_report 关联数据
                            // 获取data_id
                            Cursor cr2 = sqliteUtil.GetLastData(db, "datas");
                            if(cr2.getCount() != 0 && cr2.moveToNext()) {
                                int data_id = cr2.getInt(cr2.getColumnIndex("id"));
                                System.out.println("data_id: " + String.valueOf(data_id));
                                cr2.close();

                                cv.clear();
                                insertResult = -1;

                                cv.put("data_id", data_id);
                                cv.put("report_id", myApplication.REPORT_ID);

                                insertResult = db.insert("data_report", null, cv);
                                if(insertResult != -1) {
                                    System.out.println("DATA_REPORT表插入数据成功");
                                    cv.clear();
                                    cv = null;
                                } else { ToastUtils.show("data_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                            } else { ToastUtils.show("获取data_id失败"); }
                        } else { ToastUtils.show("user_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                    } else { ToastUtils.show("获取report_id失败"); }
                } else { ToastUtils.show("reports插入数据失败(" + String.valueOf(insertResult) + ")"); }
            }
            // 新添加 数据
            else if(FLAG.equals("1")) {
                // 新添data_report 关联
                Cursor cr = sqliteUtil.GetLastData(db, "datas");
                if(cr.getCount() != 0 && cr.moveToNext()) {
                    int data_id = cr.getInt(cr.getColumnIndex("id"));
                    System.out.println("data_id: " + String.valueOf(data_id));
                    cr.close();

                    cv.clear();
                    insertResult = -1;

                    cv.put("data_id", data_id);

                    // TODO.用户有未上报 数据，且 新 添加 数据(初始化并获得 myApplication.REPORT_ID)
                    if(myApplication.UNREPORT_DATA_ID_LST.size() > 0) {
                        if(!myApplication.UNREPORT_NEW_DATA_INITIALED) {
                            myApplication.UNREPORT_NEW_DATA_INITIALED = true;
                            System.out.println("用户有未上报数据，且新'添加'数据(初始化myApplication.REPORT_ID)");

                            // 初始化reports表
                            ContentValues cv2 = new ContentValues();
                            cv2.put("time", "");
                            cv2.put("is_reported", 0);

                            long insr = -1;
                            insr = db.insert("reports", null, cv2);
                            if(insr != -1) {
                                Cursor cr2 = sqliteUtil.GetLastData(db, "reports");

                                if(cr2.getCount() != 0 && cr2.moveToNext()) {
                                    myApplication.REPORT_ID = cr2.getInt(cr.getColumnIndex("id"));
                                    System.out.println("REPORT_ID: " + String.valueOf(myApplication.REPORT_ID));
                                    cr2.close();

                                    cv2.clear();
                                    insr = -1;

                                    cv2.put("user_id", myApplication.USER_ID);
                                    cv2.put("report_id", myApplication.REPORT_ID);

                                    // 关联 user_report表
                                    insr = db.insert("user_report", null, cv2);
                                    if(insr != -1) {
                                        cv2.clear();
                                        insr = -1;

                                        // TODO.user_report关联成功之后，将REPORT_ID插入到data_report表，及 UNREPORT_ID_LST
                                        cv.put("report_id", myApplication.REPORT_ID);

                                        myApplication.UNREPORT_ID_LST.add(myApplication.REPORT_ID);
                                        myApplication.UNREPORT_DATA_ID_LST.add(data_id);
                                    } else { ToastUtils.show("user_report插入数据失败(" + String.valueOf(insr) + ")"); }
                                } else { ToastUtils.show("获取report_id错误"); }
                            } else { ToastUtils.show("reports插入数据失败(" + String.valueOf(insr) + ")"); }
                        }
                    }
                    // myApplication.REPORT_ID已初始化，正常添加即可
                    else { cv.put("report_id", myApplication.REPORT_ID); }

                    insertResult = db.insert("data_report", null, cv);
                    if(insertResult != -1) {
                        System.out.println("DATA_REPORT表插入数据成功");
                        cv.clear();
                        cv = null;
                    } else { ToastUtils.show("data_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                } else { ToastUtils.show("获取data_id错误"); }
            }
        } else { ToastUtils.show("数据库保存数据失败(" + String.valueOf(insertResult) + ")"); }
    }
    // 将数据保存 到reportData
    private void saveDataToReportDataClass() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": saveDataToReportDataClass()");

        ReportData data = new ReportData();

        data.setArea(areaValue.getText().toString());
        data.setType(typeValue.getText().toString());
        data.setPeople(peopleValue.getText().toString());
        data.setTime(timeValue.getText().toString());
        data.setInfo(infoValue.getText().toString());
        data.setPhoto(photoName);
        data.setVideo(videoPath);

        myApplication.DATALST.add(data);

        photoName = "";
        videoPath = "";
    }

    // Table 栏点击监听
    @SuppressLint("NonConstantResourceId")
    public void onColumnClick(View view) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onColumnClick()");

        switch(view.getId()) {
            case R.id.area_select_layout: areaSelectItemLayout.setVisibility(View.VISIBLE);       break;
            case R.id.type_select_layout: alarmLevelSelectItemLayout.setVisibility(View.VISIBLE); break;

            case R.id.people_select_layout:
                selectListAdapter = new SelectListAdapter(this, workersLst);
                selectItemList.setAdapter(selectListAdapter);

                selectTitle.setText("选择违章人员");
                selectItemLayout.setVisibility(View.VISIBLE);

                break;

            case R.id.time_select_layout:
                @SuppressLint("SetTextI18n") TimePickerView v = new TimePickerView.Builder(this, (date, view1) -> runOnUiThread(() -> {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                    String timeStr       = sdf.format(date);
                    String[] data        = timeStr.split("-");

                    timeValue.setText(data[0] + "年" + data[1] + "月" + data[2] + "日" + data[3] + "时" + data[4] + "分" + data[5] + "秒");
                })).setType(new boolean[]{true, true, true, true, true, true})
                   .setDate(Calendar.getInstance())
                   .build();
                v.show();

                break;

            default: break;
        }
    }
    // 区域选择Layout初始化
    @SuppressLint("UseCompatLoadingForDrawables")
    private void areaSelectListInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": areaSelectListInitial()");

        // 6大区域
        //for(int i = 0; i < 6; ++i) {
        for(int i = 0; i < areasLst.size(); ++i) {
            LinearLayout linearLayout        = new LinearLayout(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);

            linearLayout.setLayoutParams(params);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setTag("AREA_ITEM_" + String.valueOf(i));

            if(i % 2 == 0) { linearLayout.setBackgroundColor(Color.WHITE); }

            // RadioButton
            RadioButton radioBtn                     = new RadioButton(this);
            LinearLayout.LayoutParams radioBtnParams = new LinearLayout.LayoutParams(32, 32);
            radioBtnParams.setMarginStart(24);
            radioBtnParams.gravity = Gravity.CENTER;
            radioBtn.setLayoutParams(radioBtnParams);

            radioBtn.setTextSize(0);
            radioBtn.setButtonDrawable(null);
            radioBtn.setButtonDrawable(getResources().getDrawable(R.drawable.radio_btn_selector));
            radioBtn.setGravity(Gravity.CENTER_VERTICAL);
            radioBtn.setChecked(false);

            radioBtnLst.add(radioBtn);

            // TextView
            TextView textView                        = new TextView(this);
            LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            textViewParams.setMarginStart(12);
            textViewParams.gravity = Gravity.CENTER;
            textViewParams.setLayoutDirection(LayoutDirection.INHERIT);
            textView.setLayoutParams(textViewParams);

            textView.setText(areasLst.get(i).toString());
            textView.setTextSize(16);
            textView.setTextColor(Color.BLACK);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            textView.setGravity(Gravity.CENTER_VERTICAL);

            linearLayout.addView(radioBtn);
            linearLayout.addView(textView);

            linearLayout.setOnClickListener(view -> {
                String tag = view.getTag().toString();
                int curIdx = Integer.parseInt(tag.substring(10, tag.length()));

                runOnUiThread(() -> {
                    radioBtnLst.get(curIdx).setChecked(true);

                    for(int j = 0; j < radioBtnLst.size(); ++j) {
                        if(curIdx != j) { radioBtnLst.get(j).setChecked(false); }
                    }

                    areaValue.setText(areasLst.get(curIdx).toString());
                    areaSelectItemLayout.setVisibility(View.GONE);
                });
            });

            areaSelectList.addView(linearLayout);
        }
    }
    // 违章行为类别选择Layout初始化
    @SuppressLint("UseCompatLoadingForDrawables")
    private void alarmLevelSelectListInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": alarmLevelSelectListInitial()");

        // 3个等级
        for(int i = 0; i < 3; ++i) {
            LinearLayout linearLayout        = new LinearLayout(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);

            linearLayout.setLayoutParams(params);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setTag("ALARM_LEVEL_ITEM_" + String.valueOf(i));

            if(i % 2 == 0) { linearLayout.setBackgroundColor(Color.WHITE); }

            // RadioButton
            RadioButton radioBtn                     = new RadioButton(this);
            LinearLayout.LayoutParams radioBtnParams = new LinearLayout.LayoutParams(32, 32);
            radioBtnParams.setMarginStart(24);
            radioBtnParams.gravity = Gravity.CENTER;
            radioBtn.setLayoutParams(radioBtnParams);

            radioBtn.setTextSize(0);
            radioBtn.setButtonDrawable(null);
            radioBtn.setButtonDrawable(getResources().getDrawable(R.drawable.radio_btn_selector));
            radioBtn.setGravity(Gravity.CENTER_VERTICAL);
            radioBtn.setChecked(false);

            alarmLevelRadioBtnLst.add(radioBtn);

            // TextView
            TextView textView                        = new TextView(this);
            LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            textViewParams.setMarginStart(12);
            textViewParams.gravity = Gravity.CENTER;
            textViewParams.setLayoutDirection(LayoutDirection.INHERIT);
            textView.setLayoutParams(textViewParams);

            textView.setText(unDisciplinedLevelsLst.get(i).toString());
            textView.setTextSize(16);
            textView.setTextColor(Color.BLACK);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            textView.setGravity(Gravity.CENTER_VERTICAL);

            linearLayout.addView(radioBtn);
            linearLayout.addView(textView);

            linearLayout.setOnClickListener(view -> {
                String tag = view.getTag().toString();
                int curIdx = Integer.parseInt(tag.substring(17, tag.length()));

                runOnUiThread(() -> {
                    alarmLevelRadioBtnLst.get(curIdx).setChecked(true);

                    for(int j = 0; j < alarmLevelRadioBtnLst.size(); ++j) {
                        if(curIdx != j) { alarmLevelRadioBtnLst.get(j).setChecked(false); }
                    }

                    typeValue.setText(unDisciplinedLevelsLst.get(curIdx).toString());
                    alarmLevelSelectItemLayout.setVisibility(View.GONE);
                });
            });

            alarmLevelSelectList.addView(linearLayout);
        }
    }

    // 删除 照片或视频
    @SuppressLint("NonConstantResourceId")
    public void onDelete(View v) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDelete()");

        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setTitle("删除").create();

        switch(v.getId()) {
            case R.id.photo_delete_icon:
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                    loadingAction(50, "正在删除照片，请稍候...");

                    // 删除图片并更新信息
                    File f = null;
                    if(DATA_INDEX != -1) {
                        if(!myApplication.DATALST.get(DATA_INDEX).getPhoto().equals("")) {
                            f = new File(myApplication.DATALST.get(DATA_INDEX).getPhoto());
                        } else { f = new File(photoName); }
                    } else { f = new File(photoName); }

                    if(f.delete()) {
                        new Handler().postDelayed(() -> {
                            runOnUiThread(() -> {
                                captureImageView.setImageBitmap(null);
                                captureImageView.setBackgroundResource(R.drawable.capture_icon);

                                photoDeleteIcon.setVisibility(View.GONE);

                                if(DATA_INDEX != -1) { myApplication.DATALST.get(DATA_INDEX).setPhoto(""); }
                                else { photoName = ""; }
                            });

                            isCaptured = false;
                            loadingAction(100, "照片已删除");

                            new Handler().postDelayed(() -> {
                                loadingLayout.setVisibility(View.GONE);
                            }, 500);
                        }, 1000);
                    } else { ToastUtils.show("未知错误，应用文件夹照片删除失败"); }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {dialog.dismiss();});
                dialog.setMessage("确定删除当前照片吗？");
                dialog.show();

                break;

            case R.id.video_delete_icon:
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                    loadingAction(50, "正在删除视频，请稍候...");

                    File f = null;
                    if(DATA_INDEX != -1) {
                        if(!myApplication.DATALST.get(DATA_INDEX).getVideo().equals("")) {
                            f = new File(myApplication.DATALST.get(DATA_INDEX).getVideo());
                        } else { f = new File(videoPath); }
                    } else { f = new File(videoPath); }

                    if(f.delete()) {
                        new Handler().postDelayed(() -> {
                            runOnUiThread(() -> {
                                videoImageView.setImageBitmap(null);
                                videoImageView.setBackgroundResource(R.drawable.video_icon);

                                videoDeleteIcon.setVisibility(View.GONE);

                                if(DATA_INDEX != -1) { myApplication.DATALST.get(DATA_INDEX).setVideo(""); }
                                else { videoPath = ""; }
                            });

                            isVideoed = false;
                            loadingAction(100, "视频已删除");

                            new Handler().postDelayed(() -> {
                                loadingLayout.setVisibility(View.GONE);
                            }, 500);
                        }, 1000);
                    } else { ToastUtils.show("未知错误，应用文件夹视频删除失败"); }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {dialog.dismiss();});
                dialog.setMessage("确定删除当前视频吗？");
                dialog.show();

                break;

            default: break;
        }
    }

    // 获取视频缩略图
    private Bitmap getVideoThumbnailBmp() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": getVideoThumbnailBmp()");

        Bitmap bmp = null;
        MediaMetadataRetriever mr = new MediaMetadataRetriever();

        try {
            if(DATA_INDEX != -1) {
                if(!myApplication.DATALST.get(DATA_INDEX).getVideo().equals("")) {
                    mr.setDataSource(myApplication.DATALST.get(DATA_INDEX).getVideo());
                } else { mr.setDataSource(videoPath); }
            } else { mr.setDataSource(videoPath); }

            bmp = mr.getFrameAtTime(-1);
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            ToastUtils.show("获取视频缩率图Exception:" + e.getMessage());
        } finally {
            try { mr.release(); }
            catch(RuntimeException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                ToastUtils.show("获取视频缩率图Exception:" + e.getMessage());
            }
        }

        if(bmp == null) { return null; }

        if(MediaStore.Images.Thumbnails.MINI_KIND == MediaStore.Images.Thumbnails.MINI_KIND) {
            int w   = bmp.getWidth();
            int h   = bmp.getHeight();
            int max = Math.max(w, h);

            if(max > 512) {
                float scale = 512f / max;
                int w_      = Math.round(scale * w);
                int h_      = Math.round(scale * h);
                bmp         = Bitmap.createScaledBitmap(bmp, w_, h_, true);
            }
        } else if(MediaStore.Images.Thumbnails.MINI_KIND == MediaStore.Images.Thumbnails.MICRO_KIND) {
            bmp = ThumbnailUtils.extractThumbnail(bmp, 85, 60, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
        }

        return bmp;
    }

    // LoadingLayout显示与隐藏
    private void loadingAction(int step, String note) {
        loadingLayout.setProgressStep(step);
        loadingLayout.setProgressNote(note);
        loadingLayout.setVisibility(View.VISIBLE);
    }

    /***********************************Activity Override Start***********************************/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onActivityResult(" + requestCode + "," + " " + requestCode + "," + " " + data + ")");
        // 打开 相机 拍照或录像
        if(requestCode == CAMERA_CAPTURE) {
            // 保存照片
            if(resultCode == RESULT_OK) {
                runOnUiThread(() -> loadingAction(50, "拍照成功，正在保存图片，请稍候..."));

                new Thread(() -> {
                    try {
                        photoBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(captureImageUri));

                        if(photoBitmap != null) {
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                            Date date            = new Date(System.currentTimeMillis());
                            String timeFlag      = sdf.format(date);

                            photoName      = myApplication.APP_IMAGES_PATH + File.separator + timeFlag + ".jpg";
                            File photoFile = new File(photoName);

                            try {
                                FileOutputStream fos = new FileOutputStream(photoFile);

                                photoBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fos);
                                fos.flush();
                                fos.close();

                                new Handler(Looper.getMainLooper()).postDelayed(() -> loadingAction(100, "照片保存成功"), 500);

                                runOnUiThread(() -> {
                                    captureImageView.setImageBitmap(photoBitmap);
                                    photoDeleteIcon.setVisibility(View.VISIBLE);
                                });
                                isCaptured = true;

                                // 删除 Android/data/packagename/cache/ 下缓存照片
                                System.out.println(captureImageUri.getPath());

                                String cfPath = "/storage/emulated/0/Android/data/" + getPackageName() + "/cache/" + cachePhotoTime + ".jpg";
                                System.out.println(cfPath);
                                File f = new File(cfPath);

                                if(f.exists()) {
                                    if(f.delete()) { System.out.println("缓存照片删除成功"); }
                                    else { ToastUtils.show("缓存照片删除失败"); }
                                } else { ToastUtils.show("缓存照片文件出错"); }

                                new Handler(Looper.getMainLooper()).postDelayed(() -> loadingLayout.setVisibility(View.GONE), 500);
                            } catch(IOException e) {
                                System.out.println(e.getMessage());
                                e.printStackTrace();

                                runOnUiThread(() -> loadingLayout.setVisibility(View.GONE));
                                Looper.prepare();
                                ToastUtils.show("照片保存到应用文件夹失败");
                            }
                        } else {
                            runOnUiThread(() -> loadingLayout.setVisibility(View.GONE));
                            Looper.prepare();
                            ToastUtils.show("bitmap=null，照片保存失败");
                        }
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        e.printStackTrace();

                        runOnUiThread(() -> loadingLayout.setVisibility(View.GONE));
                        Looper.prepare();
                        ToastUtils.show("照片保存失败Exception:" + e.getMessage());
                    }
                }).start();
            } else { ToastUtils.show("拍照取消或失败"); }
        }
        if(requestCode == CAMERA_VIDEO) {
            // 保存视频
            if(resultCode == RESULT_OK) {
                runOnUiThread(() -> loadingAction(50, "拍摄成功，正在保存视频，请稍候..."));

                new Thread(() -> {
                    // 视频文件拷贝至data目录下
                    try {
                        FileInputStream fis  = new FileInputStream("/storage/emulated/0/Android/data/" + getPackageName() + "/cache/" + cacheVideoTime + ".mp4");

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                        Date date            = new Date(System.currentTimeMillis());
                        String timeFlag      = sdf.format(date);

                        videoPath      = myApplication.APP_VIDEOS_PATH + File.separator + timeFlag + ".mp4";
                        File videoFile = new File(videoPath);

                        FileOutputStream fos = new FileOutputStream(videoFile);

                        byte[] buf = new byte[1024];
                        int len    = -1;

                        while(-1 != (len = fis.read(buf))) { fos.write(buf, 0, len); }

                        fis.close();
                        fos.flush();
                        fos.close();

                        // 获取视频缩略图
                        Bitmap videoThumbnailBmp = getVideoThumbnailBmp();

                        if(videoThumbnailBmp != null) {
                            new Handler(Looper.getMainLooper()).postDelayed(() -> loadingAction(100, "视频保存成功"), 500);

                            runOnUiThread(() -> {
                                videoImageView.setImageBitmap(videoThumbnailBmp);
                                videoDeleteIcon.setVisibility(View.VISIBLE);
                            });
                            isVideoed = true;

                            // 删除 Android/data/packagename/cache/ 下缓存视频
                            System.out.println(captureVideoUri.getPath());

                            String cfPath = "/storage/emulated/0/Android/data/" + getPackageName() + "/cache/" + cacheVideoTime + ".mp4";
                            System.out.println(cfPath);
                            File f = new File(cfPath);

                            if(f.exists()) {
                                if(f.delete()) { System.out.println("缓存视频删除成功"); }
                                else { ToastUtils.show("缓存视频删除失败"); }
                            } else { ToastUtils.show("缓存视频文件出错"); }

                            new Handler(Looper.getMainLooper()).postDelayed(() -> loadingLayout.setVisibility(View.GONE), 500);
                        } else { ToastUtils.show("获取视频缩略图失败"); }
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        e.printStackTrace();
                        ToastUtils.show("获取视频缩率图Exception:" + e.getMessage());
                    }
                }).start();

                ToastUtils.show("拍摄完成");
            } else { ToastUtils.show("未知错误，拍摄失败"); }
        }
    }
    @Override
    protected void onStart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStart()");
        super.onStart();
    }
    @Override
    protected void onResume() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onResume()");
        super.onResume();
    }
    @Override
    protected void onPause() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onPause()");
        super.onPause();

        MediaPlayerManager.instance().pause();
    }
    @Override
    protected void onStop() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStop()");
        super.onStop();
    }
    @Override
    protected void onRestart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onRestart()");
        super.onRestart();
    }
    @Override
    protected void onDestroy() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDestroy()");
        super.onDestroy();

        MediaPlayerManager.instance().releasePlayerAndView(this);
        // 关闭数据库
        if(db != null && db.isOpen()) { db.close(); }
    }
    /***********************************Activity Override End***********************************/

    // 返回键 监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(isVideoPreviewShow) {
                MediaPlayerManager.instance().releasePlayerAndView(this);

                runOnUiThread(() -> {
                    videoFileName.setText("");
                    videoPreviewLayout.setVisibility(View.GONE);
                });

                isVideoPreviewShow = false;
            }
        }

        return false;
    }
}
