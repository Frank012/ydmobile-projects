package com.demo.securityreport;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.UUID;

public class BluetoothConnectThread extends Thread {
    private static final UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private Object lock = new Object();

    private BluetoothDevice device = null;
    public  BluetoothSocket socket = null;

    public boolean connected = false;

    // 回调接口
    private BluetoothConnectCallback connectCallback;

    // Constructor
    public BluetoothConnectThread(BluetoothDevice dev, BluetoothConnectCallback callback) {
        try {
            device = dev;
            socket = device.createInsecureRfcommSocketToServiceRecord(uuid);

            connectCallback = callback;
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        if(socket != null) {
            if(connected) {
                disconnect();
                connected = false;
            }
        }

        new Thread() {
            @Override
            public void run() {
                connect();

                if(connected) {
                    if(connectCallback != null) {
                        connectCallback.connectSuccess(socket);
                    }
                }
            }
        }.start();
    }

    // 连接
    public void connect() {
        try {
            synchronized(lock) {
                socket.connect();
                connected = true;
            }
        } catch(Exception e) {
            e.printStackTrace();
            cancel();

            try {
                Method m;
                m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
                socket = (BluetoothSocket) m.invoke(device, Integer.valueOf(1));
                socket.connect();

                connected = true;
            } catch(Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();

                if(connectCallback != null){ connectCallback.connectFailed(ex.getMessage()); }
            }
        }
    }

    public void cancel() {
        try {
            synchronized (lock) {
                if(connected) {
                    socket.close();
                    socket = null;
                    connected = false;
                }
            }
        } catch(IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    // 断开连接
    public void disconnect() {
        try {
            synchronized (lock) {
                socket.close();
                socket = null;
                connected = false;
            }
        } catch(IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
