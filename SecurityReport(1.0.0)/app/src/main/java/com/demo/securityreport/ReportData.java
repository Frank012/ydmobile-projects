package com.demo.securityreport;

import android.os.Parcel;
import android.os.Parcelable;

public class ReportData implements Parcelable {
    private String area;
    private String type;
    private String people;
    private String time;
    private String info;
    private String photo;
    private String video;

    public static final Parcelable.Creator<ReportData> CREATOR = new Creator<ReportData>() {
        @Override
        public ReportData createFromParcel(Parcel source) {
            ReportData data = new ReportData();

            data.area   = source.readString();
            data.type   = source.readString();
            data.people = source.readString();
            data.time   = source.readString();
            data.info   = source.readString();
            data.photo  = source.readString();
            data.video  = source.readString();

            return data;
        }
        @Override
        public ReportData[] newArray(int size) { return new ReportData[size]; }
    };

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(area);
        parcel.writeString(type);
        parcel.writeString(people);
        parcel.writeString(time);
        parcel.writeString(info);
        parcel.writeString(photo);
        parcel.writeString(video);
    }

    @Override
    public int describeContents() { return 0; }

    // SETS & GETS
    public void setArea(String v) { this.area = v; }
    public String getArea() { return area; }

    public void setType(String v) { this.type = v; }
    public String getType() { return type; }

    public void setPeople(String v) { this.people = v; }
    public String getPeople() { return people; }

    public void setTime(String v) { this.time = v; }
    public String getTime() { return time; }

    public void setInfo(String v) { this.info = v; }
    public String getInfo() { return info; }

    public void setPhoto(String v) { this.photo = v; }
    public String getPhoto() { return photo; }

    public void setVideo(String v) { this.video = v; }
    public String getVideo() { return video; }
}
