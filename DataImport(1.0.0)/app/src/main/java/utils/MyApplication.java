package utils;

import android.app.Application;
import android.os.Environment;
import android.view.Gravity;

import com.hjq.toast.ToastUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyApplication extends Application {
    // Variables
    private static final String Log_Tag     = "MyApplication";
    private AppCrashHandler appCrashHandler = null;

    // dbApp 数据库状态配置文件
    public static String DB_APP_STATUS_CONFIG_FILE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "王曲变电站数据/config.xml";
    public static String DB_APP_IS_OPEN_TAG = "1";

    // App数据库路径
    // TODO.基于【华为荣耀平板5·AGS2-W09HN】SD卡存储路径测试，不同型号平板，路径可能不同
    public static String DEVICES_REPORT_DB_PATH  = "/Android/data/com.demo.devicesreport/devices_report.db";
    public static String SECURITY_REPORT_DB_PATH = "/Android/data/com.demo.securityreport/security_report.db";

    // 程序文件、日志 数据及数据库 路径
    public static String APP_DIR_PATH  = null;
    public static String APP_LOGS_PATH = null;
    public static String APP_DATA_PATH = null;
    public static String APP_DB_PATH   = null;

    // Constructor
    public MyApplication() {
        super();
        System.out.println(Log_Tag + ": Constructor");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println(Log_Tag + ": onCreate()");

        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            System.out.println("SD卡path: " + Environment.getExternalStorageDirectory());

            // 创建 应用文件夹
            APP_DIR_PATH = Environment.getExternalStorageDirectory() + File.separator + "DataImport";
            File appDir  = new File(APP_DIR_PATH);

            if(!appDir.exists()) {
                if(appDir.mkdir()) {
                    System.out.println("应用文件夹创建成功：" + appDir.getAbsolutePath());
                } else { System.out.println("未知错误，应用文件夹创建失败"); }
            } else { System.out.println("应用文件夹已存在：" + appDir.getAbsolutePath()); }

            // 创建 日志文件夹
            APP_LOGS_PATH = APP_DIR_PATH + File.separator + "Logs";
            File logDir   = new File(APP_LOGS_PATH);

            if(!logDir.exists()) {
                if(logDir.mkdir()) { System.out.println("应用日志文件夹创建成功：" + logDir.getAbsolutePath()); }
                else { System.out.println("未知错误，应用日志文件夹创建失败"); }
            } else { System.out.println("应用日志文件夹已存在：" + logDir.getAbsolutePath()); }

            // 创建 数据库文件夹
            APP_DB_PATH = APP_DIR_PATH + File.separator + "db";
            File dbDir  = new File(APP_DB_PATH);

            if(!dbDir.exists()) {
                if(dbDir.mkdir()) { System.out.println("应用数据库文件夹创建成功：" + dbDir.getAbsolutePath()); }
                else { System.out.println("未知错误，应用数据库文件夹创建失败"); }
            } else { System.out.println("应用数据库文件夹已存在：" + dbDir.getAbsolutePath()); }

            // 创建 Data文件夹
            APP_DATA_PATH = Environment.getExternalStorageDirectory() + File.separator + "王曲变电站数据";
            File dataDir  = new File(APP_DATA_PATH);

            if(!dataDir.exists()) {
                if(dataDir.mkdir()) { System.out.println("应用data文件夹创建成功：" + dataDir.getAbsolutePath()); }
                else { System.out.println("未知错误，应用data文件夹创建失败"); }
            } else { System.out.println("应用data文件夹已存在：" + dataDir.getAbsolutePath()); }
        }

        // Toast Tool Init
        ToastUtils.init(this);
        ToastUtils.setGravity(Gravity.BOTTOM, 0, 100);

        // 程序异常崩溃handler
        appCrashHandler = AppCrashHandler.GetInstance();
        appCrashHandler.Init(getApplicationContext());
    }
}