package utils;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SqliteUtil {
    // Constructor
    public SqliteUtil() {
        System.out.println("-----SqliteUtil Constructor-----");
    }

    // 以"表名"的形式查询表是否存在
    public boolean IsTableExists(SQLiteDatabase db, String tableName) {
        System.out.println("-----SqliteUtil IsTableExists()-----");
        System.out.println("-----表：" + tableName + "-----");

        String sqlStr = "SELECT COUNT(*) as c from sqlite_master WHERE TYPE = 'table' and NAME = '" + tableName + "';";
        Cursor cr     = db.rawQuery(sqlStr, null);

        if(cr.moveToNext()) {
            int count = cr.getInt(0);
            if(count > 0) {
                cr.close();
                return true;
            }
        }

        return false;
    }

    // 插入数据
    public boolean InsertData(SQLiteDatabase db, String tableName, String sql) {
        System.out.println("-----SqliteUtil InsertData()-----");

        Cursor cr = db.rawQuery("SELECT * FROM " + tableName + ";", null);
        int lastCount = cr.getCount();

        db.execSQL(sql);

        cr = db.rawQuery("SELECT * FROM " + tableName + ";", null);
        int curCount = cr.getCount();

        if(curCount - lastCount == 1) { return true; }

        return false;
    }

    // 更新数据
    public boolean UpdateData(SQLiteDatabase db, String sql) {
        System.out.println("-----SqliteUtil UpdateData()-----");

        return false;
    }

    // 返回表的最后一条数据
    public Cursor GetLastData(SQLiteDatabase db, String tableName) {
        System.out.println("-----SqliteUtil GetLastData()-----");

        String sqlStr = "SELECT * FROM " + tableName + " LIMIT 1 OFFSET (SELECT COUNT(*) - 1 FROM " + tableName + ");";
        Cursor cr = db.rawQuery(sqlStr, null);

        return cr;
    }

    // 清空表数据
    public boolean ClearTableData(SQLiteDatabase db, String tableName) {
        System.out.println("-----SqliteUtil ClearTableData()-----");

        String clearDataSql = "DELETE FROM " + tableName + ";";
        db.execSQL(clearDataSql);

        // 查询数据是被否被清空
        Cursor cr = db.rawQuery("SELECT * FROM " + tableName + ";", null);
        if(cr.getCount() == 0) {
            // 将id重置为0
            String resetSequenceSql = "UPDATE sqlite_sequence SET seq = 0 WHERE name = '" + tableName + "';";
            db.execSQL(resetSequenceSql);

            // TODO.test 压缩空间(小米pad-Sqlite版本更高[跟用户目录路径不同的问题一样，是不是都是因为系统安卓版本更高的缘故引起的]？
            // TODO.test VACUUM command命令在高版本中已经不再指向某个表，而是整个database)
            //String recycleSpaceSql = "VACUUM '" + tableName + "';";
            //db.execSQL(recycleSpaceSql);

            db.execSQL("VACUUM");

            return true;
        }

        return false;
    }
}