package utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.SimpleFormatter;

public class TimeUtil {
    // Variables
    private static final String Log_Tag = "TimeUtil";
    private MyApplication myApplication = null;

    // Constructor
    public TimeUtil() {
        System.out.println(Log_Tag + ": Constructor");
        myApplication = new MyApplication();
    }

    // 返回 格式化的 当前时间字符串，默认返回 年月日，通过boolean变量要求是否需要 时分秒；以要求的 连接符 连接各部分(年月日、时分秒 以 空格符 隔开)
    public String GetFormatedTimeString(String connectStr, boolean is_H_M_S_Need, boolean is_filled_with_0) {
       //System.out.println(Log_Tag + ": GetFormatedTimeString(connectStr: " + connectStr + ", is_H_M_S_Need: " + is_H_M_S_Need + ", is_filled_with_0: " + is_filled_with_0 + ")");

        String type = is_H_M_S_Need ? "yyyy" + connectStr + "MM" + connectStr + "dd" + " " + "HH" + connectStr + "mm" + connectStr + "ss"
                                    : "yyyy" + connectStr + "MM" + connectStr + "dd";
        SimpleDateFormat format = new SimpleDateFormat(type);
        Date date               = new Date(System.currentTimeMillis());

        String timeFlag = format.format(date);
        if(!is_filled_with_0) {
            int y = Calendar.getInstance().get(Calendar.YEAR);
            int m = Calendar.getInstance().get(Calendar.MONTH) + 1;
            int d = Calendar.getInstance().get(Calendar.DATE);

            int H = Calendar.getInstance().get(Calendar.HOUR);
            int M = Calendar.getInstance().get(Calendar.MINUTE);
            int S = Calendar.getInstance().get(Calendar.SECOND);

            timeFlag = is_H_M_S_Need ? String.valueOf(y) + connectStr + String.valueOf(m) + connectStr + String.valueOf(d) + " " + String.valueOf(H) + connectStr + String.valueOf(M) + connectStr + String.valueOf(S)
                                     : String.valueOf(y) + connectStr + String.valueOf(m) + connectStr + String.valueOf(d);
        }
        //System.out.println("格式化时间字符串：" + timeFlag);

        return timeFlag;
    }

    // 计算时间差，以map格式返回天、时、分、秒的值
    public Map<String, Long> CaculateTimeDifference(Date curDate, Date lastDate) {
        System.out.println(Log_Tag + ": CaculateTimeDifference()");

        Map<String, Long> timeMap = new HashMap<String, Long>();
        long timeDiff = curDate.getTime() - lastDate.getTime();
        long timeDiffOfDays    = timeDiff / (1000 * 60 * 60 * 24);
        long timeDiffOfHours   = (timeDiff - timeDiffOfDays * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
        long timeDiffOfMinutes = (timeDiff - timeDiffOfDays * (1000 * 60 * 60 * 24) - timeDiffOfHours * (1000 * 60 * 60)) / (100 * 60);
        long timeDiffOfSeconds = (timeDiff - timeDiffOfDays * (1000 * 60 * 60 * 24) - timeDiffOfHours * (1000 * 60 * 60) - timeDiffOfMinutes * (1000 * 60)) / 1000;

        timeMap.put("days", timeDiffOfDays);
        timeMap.put("hours", timeDiffOfHours);
        timeMap.put("minutes", timeDiffOfMinutes);
        timeMap.put("seconds", timeDiffOfSeconds);

        return timeMap;
    }
}