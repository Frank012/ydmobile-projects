package com.demo.dataimport;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.hjq.toast.ToastUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.SortedMap;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import utils.LoadingLayout;
import utils.MyAlertDialog;
import utils.MyApplication;
import utils.SqliteUtil;

public class MainActivity extends Activity {
    // Variables
    private MyApplication myApplication = null;
    private static final String Log_Tag = "MainActivity";
    private static final long mExitTime = 0;

    private SQLiteDatabase db = null;
    private File dateDirF     = null;

    private static int jsonFileIndex  = -1;
    private static int loadFileIndex  = -1;
    private static JSONArray jsonData = null;

    // Utils
    private SqliteUtil sqliteUtil       = null;
    private MyAlertDialog myAlertDialog = null;

    private TextView jsonFilePath   = null;
    private ImageView fileStateIcon = null;

    //private RadioButton clearConditionRbtn  = null;
    //private RadioButton newAddConditionRbtn = null;

    private TextView importLog       = null;
    private ScrollView logScrollView = null;

    private Button importBtn = null;
    private Button exitBtn   = null;

    private LoadingLayout loadingLayout = null;

    // 导入Progress Block
    private RelativeLayout importProgressBlock  = null;
    private TextView importProgressNote         = null;
    private ProgressBar importProgressBar       = null;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApplication = new MyApplication();
        System.out.println(Log_Tag + ": onCreate()");

        setContentView(R.layout.activity_main);

        // 全屏
        MainActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View view = this.getWindow().getDecorView();
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        // Utils初始化
        sqliteUtil = new SqliteUtil();

        // UI初始化
        UIInitial();

        /* 引导用户进入 权限设置界面，打开 usagePackageStats 权限
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            try {
                startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
            } catch(Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                ToastUtils.show(e.getMessage());
            }
        }
        */
    }

    // UI Initial
    @SuppressLint({"UseCompatLoadingForDrawables", "ClickableViewAccessibility"})
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void UIInitial() {
        System.out.println(Log_Tag + ": UIInitial()");

        // 导入数据类型 Spinner
        // UI
        Spinner dataTypeSpinner = findViewById(R.id.data_type_spinner);
        dataTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                runOnUiThread(() -> {
                    String path     = myApplication.APP_DATA_PATH + File.separator;
                    String filePath = "";

                    jsonFileIndex = position;

                    if(jsonFileIndex == 0) { filePath = path + "workers.txt"; }
                    else { filePath = path + "devices.txt"; }

                    if(jsonFileIndex != loadFileIndex) {
                        importBtn.setEnabled(true);
                        runOnUiThread(() -> { importBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style)); });
                    }

                    jsonFilePath.setText(filePath);
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        jsonFilePath  = findViewById(R.id.json_file_path);
        fileStateIcon = findViewById(R.id.file_state_icon);

        /* 导入 条件
        clearConditionRbtn = findViewById(R.id.radio_btn1);
        clearConditionRbtn.setOnCheckedChangeListener((btn, isChecked) -> newAddConditionRbtn.setChecked(!isChecked));

        newAddConditionRbtn = findViewById(R.id.radio_btn2);
        newAddConditionRbtn.setOnCheckedChangeListener((btn, isChecked) -> clearConditionRbtn.setChecked(!isChecked));
        */

        // 导入数据 Btn
        importBtn = findViewById(R.id.start_import_btn);
        importBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    loadFileIndex = jsonFileIndex;
                    runOnUiThread(() -> { importBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_disable_style)); });
                    break;

                case MotionEvent.ACTION_UP:
                    // 检查 安全填报、设备填报 App是否正在运行
                    if(isDBAppRunning(loadFileIndex)) {
                        runOnUiThread(() -> {
                            importBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style));

                            String type = loadFileIndex == 0 ? "安全" : "设备";
                            ToastUtils.show("「" + type + "填报」App正在运行，请先保存数据退出「" + type + "填报」App后，再导入数据");
                        });
                    }
                    // 开始导入
                    else { importData(); }

                    break;

                case MotionEvent.ACTION_MOVE: break;

                default: break;
            }

            return true;
        });
        // 退出 Btn
        exitBtn = findViewById(R.id.exit_btn);
        exitBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        exitBtn.setBackgroundColor(Color.parseColor("#5495e5"));
                        exitBtn.setTextColor(Color.WHITE);
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        exitBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.cancel_btn_style));
                        exitBtn.setTextColor(Color.parseColor("#5495e5"));
                    });

                    // 弹出 对话框 确定退出应用
                    myAlertDialog.setTitle("退出");
                    myAlertDialog.setContent("确定退出应用吗？");
                    myAlertDialog.setClickCallback(new MyAlertDialog.ClickCallback() {
                        @Override
                        public void onOKClick() {
                            if(db != null && db.isOpen()) { db.close(); }

                            MainActivity.this.finish();
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(0);
                        }
                        @Override
                        public void onCancelClick() { }
                    });
                    myAlertDialog.setVisibility(View.VISIBLE);

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 导入 Progress Block
        importProgressBlock = findViewById(R.id.import_progress_block);
        importProgressBlock.setElevation(1);
        importProgressBlock.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });
        RelativeLayout importProgressLayout = findViewById(R.id.import_progress_layout);
        importProgressLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        importProgressNote  = findViewById(R.id.import_progress_note);
        importProgressBar   = findViewById(R.id.import_progress_bar);
        importProgressBar.setMin(0);
        importProgressBar.setMax(100);
        importProgressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);

        // 导入日志
        logScrollView = findViewById(R.id.import_log_block);
        importLog     = findViewById(R.id.import_log);

        // 对话框
        myAlertDialog = findViewById(R.id.my_alert_dialog);

        // 加载等待
        loadingLayout = findViewById(R.id.loading_layout);
        loadingLayout.setClickCallback(() -> { /* todo sth. */ });
    }

    // 检查 安全填报、设备填报 App是否运行
    private boolean isDBAppRunning(int type) {
        System.out.println(Log_Tag + ": isDBAppRunning(" + String.valueOf(type) + ")");

        File f = new File(MyApplication.DB_APP_STATUS_CONFIG_FILE_PATH);
        if(!f.exists()) {
            ToastUtils.show("数据导入App.DB状态配置文件不存在");
            return true;
        }

        String packageName = type == 0 ? "com.demo.securityreport" : "com.demo.devicesreport";
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder        = factory.newDocumentBuilder();

            Document doc = builder.parse(f);
            Node root    = doc.getDocumentElement();

            if(root.hasChildNodes()) {
                NodeList nodeLst = root.getChildNodes();

                for(int i = 0; i < nodeLst.getLength(); i++) {
                    if(nodeLst.item(i).getNodeType() == Node.ELEMENT_NODE) {
                        Element e = (Element)nodeLst.item(i);

                        if(e.getTagName().equals(packageName)) {
                            System.out.println("isbackground: " + e.getAttribute("isbackground"));

                            if(e.getAttribute("isbackground").equals(MyApplication.DB_APP_IS_OPEN_TAG)) {
                                return true;
                            }
                        }
                    }
                }
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return false;
    }

    // 导入数据
    @SuppressLint("UseCompatLoadingForDrawables")
    private void importData() {
        System.out.println(Log_Tag + ": importData()");

        importBtn.setEnabled(false);

        runOnUiThread(() -> {
            loadingAction(1, "正在加载文件...", View.VISIBLE);
            importLog.append(">>>正在加载文件(" + jsonFilePath.getText().toString() +")...\n");
        });

        // 检查文件
        final boolean state = checkJsonFile(jsonFilePath.getText().toString());

        runOnUiThread(() -> {
            if(state) {
                fileStateIcon.setImageDrawable(getResources().getDrawable(R.drawable.yes));
                fileStateIcon.setVisibility(View.VISIBLE);

                loadingAction(10, "加载文件成功，开始解析json...", View.VISIBLE);
                importLog.append(">>>加载文件成功，开始解析json...\n");

                // 解析json
                parseJsonData(jsonData);
            } else {
                fileStateIcon.setVisibility(View.VISIBLE);
                ErrorShow(10, "加载文件失败，文件不存在或格式错误");
            }
        });
    }
    // 检查json文件
    private boolean checkJsonFile(String fp) {
        System.out.println(Log_Tag + ": checkJsonFile()");

        File file = new File(fp);
        // 文件是否存在
        if(file.exists()) {
            // 从文件读取json
            String jsonStr = readJsonFromFile(file);
            if(jsonStr != null && jsonStr.length() > 0) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONArray root     = jsonObj.getJSONArray("datas");

                    if(root.length() > 0) {
                        jsonData = root;
                        return true;
                    }
                } catch(Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        return false;
    }
    // 读取json
    private String readJsonFromFile(File f) {
        System.out.println(Log_Tag + ": readJsonFromFile()");

        String jsonStr = null;
        try {
            InputStream ins          = new FileInputStream(f);
            InputStreamReader reader = new InputStreamReader(ins);
            BufferedReader bufReader = new BufferedReader(reader);

            String str   = null;
            StringBuffer buffer = new StringBuffer("");

            while((str = bufReader.readLine()) != null) { buffer.append(str); }
            jsonStr = buffer.toString();
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return jsonStr;
    }
    // 解析数据
    private void parseJsonData(JSONArray array) {
        System.out.println(Log_Tag + ": parseJsonData()");

        try {
            if(jsonFileIndex == 0) {
                runOnUiThread(() -> runOnUiThread(() -> importLog.append(">>>*********************************************\n")));
                for(int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);

                    String workerName = obj.getString("name");
                    runOnUiThread(() -> { importLog.append(">>>员工姓名：" + workerName + "\n"); });
                }
                runOnUiThread(() -> runOnUiThread(() -> importLog.append(">>>*********************************************\n")));
            } else {
                for(int i = 0; i < array.length(); i++) {
                    JSONObject areaObj = array.getJSONObject(i);

                    String areaRealId = areaObj.getString("real_id");
                    String areaName   = areaObj.getString("area_name");

                    runOnUiThread(() -> {
                        importLog.append(">>>*********************************************\n");
                        importLog.append(">>>区域real_id：" + areaRealId + "\n");
                        importLog.append(">>>区域：" + areaName + "\n");
                        importLog.append(">>>----------------------------------------------------------------------\n");
                    });

                    JSONArray deviceTypeArray = areaObj.getJSONArray("device_types");

                    for(int j = 0; j < deviceTypeArray.length(); j++) {
                        JSONObject deviceTypeArrayObj = deviceTypeArray.getJSONObject(j);

                        String deviceTypeRealId = deviceTypeArrayObj.getString("real_id");
                        String deviceTypeName   = deviceTypeArrayObj.getString("device_type_name");

                        runOnUiThread(() -> {
                            importLog.append(">>>设备类型real_id：" + deviceTypeRealId + "\n");
                            importLog.append(">>>设备类型：" + deviceTypeName + "\n");
                            importLog.append(">>>......................................................................\n");
                        });

                        JSONArray pointsArray = deviceTypeArrayObj.getJSONArray("points");

                        for(int k = 0; k < pointsArray.length(); k++) {
                            JSONObject pointsArrayObj = pointsArray.getJSONObject(k);

                            String pointRealId = pointsArrayObj.getString("real_id");
                            String deviceName  = pointsArrayObj.getString("device_name");
                            String pointName   = pointsArrayObj.getString("point_name");

                            runOnUiThread(() -> {
                                importLog.append(">>>点位real_id：" + pointRealId + "\n");
                                importLog.append(">>>点位：" + deviceName + "-" + pointName + "\n");
                                importLog.append(">>>......................................................................\n");
                            });
                        }
                        runOnUiThread(() -> importLog.append(">>>----------------------------------------------------------------------\n"));
                    }
                    runOnUiThread(() -> runOnUiThread(() -> importLog.append(">>>*********************************************\n")));
                }
            }

            runOnUiThread(() -> {
                loadingAction(70, "解析json成功，开始准备导入数据...", View.VISIBLE);
                importLog.append(">>>解析json成功，开始准备导入数据...\n");
            });
            // logScrollView滚动到底部
            new Handler().post(() -> logScrollView.fullScroll(ScrollView.FOCUS_DOWN));

            // 开始 插入数据
            insertData(array);
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

            ErrorShow(70, "解析json数据失败");
        }
    }
    // 插入数据
    private void insertData(JSONArray array) {
        System.out.println(Log_Tag + ": insertData()");

        if(isDBPrepared()) {
            // 员工数据
            if(jsonFileIndex == 0) {
                db = SQLiteDatabase.openDatabase(dateDirF.getAbsolutePath() + File.separator + "security_report.db", null, SQLiteDatabase.OPEN_READWRITE);
                if(db != null) {
                    runOnUiThread(() -> { importLog.append(">>>本地数据库security_report.db打开成功！\n"); });

                    // 清空并导入
                    //if(clearConditionRbtn.isChecked()) {
                        if(isDBDataCleared()) {
                            runOnUiThread(() -> {
                                loadingAction(100, "数据库准备完毕，开始插入数据...", View.VISIBLE);
                                importLog.append(">>>数据库准备完毕，开始插入数据...\n");
                            });
                            new Handler().postDelayed(() -> loadingAction(100, "数据库准备完毕，开始插入数据...", View.GONE), 2000);

                            runOnUiThread(() -> {
                                importProgressBlock.setVisibility(View.VISIBLE);
                                importProgressUpdate(10);
                            });

                            startInsertData(array);
                        }
                    //}
                    /* 新增导入(暂时关闭该功能)
                    else {
                        runOnUiThread(() -> {
                            loadingAction(100, "开始插入数据...", View.VISIBLE);
                            importLog.append(">>>开始插入数据...\n");
                        });
                        new Handler().postDelayed(() -> loadingAction(100, "开始插入数据...", View.GONE), 1000);

                        runOnUiThread(() -> {
                            importProgressBlock.setVisibility(View.VISIBLE);
                            importProgressUpdate(10);
                        });
                    }

                    // 插入数据
                    startInsertData(array);
                    */
                }
            }
            // 设备数据
            else {
                db = SQLiteDatabase.openDatabase(dateDirF.getAbsolutePath() + File.separator + "devices_report.db", null, SQLiteDatabase.OPEN_READWRITE);
                if(db != null) {
                    runOnUiThread(() -> { importLog.append(">>>本地数据库devices_report.db打开成功！\n"); });

                    //if(clearConditionRbtn.isChecked()) {
                        if(isDBDataCleared()) {
                            runOnUiThread(() -> {
                                loadingAction(100, "数据库准备完毕，开始插入数据...", View.VISIBLE);
                                importLog.append(">>>数据库准备完毕，开始插入数据...\n");
                            });
                            new Handler().postDelayed(() -> loadingAction(100, "数据库准备完毕，开始插入数据...", View.GONE), 1000);

                            runOnUiThread(() -> {
                                importProgressBlock.setVisibility(View.VISIBLE);
                                importProgressUpdate(10);
                            });

                            startInsertData(array);
                        }
                    //}
                    /*
                    else {
                        runOnUiThread(() -> {
                            loadingAction(100, "开始插入数据...", View.VISIBLE);
                            importLog.append(">>>开始插入数据...\n");
                        });
                        new Handler().postDelayed(() -> loadingAction(100, "开始插入数据...", View.GONE), 1000);

                        runOnUiThread(() -> {
                            importProgressBlock.setVisibility(View.VISIBLE);
                            importProgressUpdate(10);
                        });

                        startAddNewDevicesData(array);
                    }
                    */
                }
            }
        }
    }
    // 数据库 准备
    private boolean isDBPrepared() {
        System.out.println(Log_Tag + ": isDBPrepared()");

        try {
            // 创建 日期操作文件夹
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            Date date            = new Date(System.currentTimeMillis());
            String time_str      = sdf.format(date);

            String dateDir = myApplication.APP_DB_PATH + File.separator + time_str;
            dateDirF       = new File(dateDir);

            if(dateDirF.mkdir()) {
                runOnUiThread(() -> importLog.append(">>>操作日期文件夹创建成功" + dateDirF.getAbsolutePath() + "！\n"));

                File f = null;
                FileChannel input     = null;
                FileChannel input_bp  = null;
                FileChannel output    = null;
                FileChannel output_bp = null;

                // 安全填报App数据库拷贝(本地如果已有，则覆盖)
                if(jsonFileIndex == 0) {
                    f        = new File(Environment.getExternalStorageDirectory() + myApplication.SECURITY_REPORT_DB_PATH);
                    input    = new FileInputStream(f).getChannel();
                    input_bp = new FileInputStream(f).getChannel();

                    output    = new FileOutputStream(dateDirF.getAbsolutePath() + File.separator + "security_report.db").getChannel();
                    output_bp = new FileOutputStream(dateDirF.getAbsolutePath() + File.separator + "security_report_backup.db").getChannel();

                    output.transferFrom(input, 0, input.size());
                    output_bp.transferFrom(input_bp, 0, input_bp.size());
                }
                // 设备填报App数据库拷贝
                else {
                    f        = new File(Environment.getExternalStorageDirectory() + myApplication.DEVICES_REPORT_DB_PATH);
                    input    = new FileInputStream(f).getChannel();
                    input_bp = new FileInputStream(f).getChannel();

                    output    = new FileOutputStream(dateDirF.getAbsolutePath() + File.separator + "devices_report.db").getChannel();
                    output_bp = new FileOutputStream(dateDirF.getAbsolutePath() + File.separator + "devices_report_backup.db").getChannel();

                    output.transferFrom(input, 0, input.size());
                    output_bp.transferFrom(input_bp, 0, input_bp.size());
                }

                input.close();
                input_bp.close();

                output.close();
                output_bp.close();

                return true;
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

            ErrorShow(70, "App数据库拷入本地失败");
        }

        return false;
    }
    // 数据库 清除原有数据
    private boolean isDBDataCleared() {
        System.out.println(Log_Tag + ": isDBDataCleared()");

        if(jsonFileIndex == 0) {
            // 清空 workers表
            db.execSQL("DELETE FROM workers;");
            db.execSQL("DELETE FROM sqlite_sequence WHERE name = 'workers';");

            @SuppressLint("Recycle") Cursor c = db.rawQuery("SELECT * FROM workers;", null);
            if(c.getCount() == 0 && !c.moveToNext()) {
                System.out.println("workers表清空成功");
                return true;
            }
        } else {
            // 清空 areas表
            db.execSQL("DELETE FROM areas;");
            db.execSQL("DELETE FROM sqlite_sequence WHERE name = 'areas';");
            // 清空 device_type表
            db.execSQL("DELETE FROM device_type;");
            db.execSQL("DELETE FROM sqlite_sequence WHERE name = 'device_type';");
            // 清空 points表
            db.execSQL("DELETE FROM points;");
            db.execSQL("DELETE FROM sqlite_sequence WHERE name = 'points';");

            boolean b1 = false, b2 = false, b3 = false;

            @SuppressLint("Recycle") Cursor c1 = db.rawQuery("SELECT * FROM areas;", null);
            if(c1.getCount() == 0 && !c1.moveToNext()) {
                System.out.println("areas表清空成功");
                b1 = true;
            }

            @SuppressLint("Recycle") Cursor c2 = db.rawQuery("SELECT * FROM device_type;", null);
            if(c2.getCount() == 0 && !c2.moveToNext()) {
                System.out.println("device_type表清空成功");
                b2 = true;
            }

            @SuppressLint("Recycle") Cursor c3 = db.rawQuery("SELECT * FROM points;", null);
            if(c3.getCount() == 0 && !c3.moveToNext()) {
                System.out.println("points表清空成功");
                b3 = true;
            }

            if(b1 && b2 && b3) { return true; }
            else { ErrorShow(90, "数据清空错误"); }
        }

        return false;
    }
    // 开始插入 数据
    private void startInsertData(JSONArray array) {
        System.out.println(Log_Tag + ": insertDeviceData()");

        try {
            ContentValues cv  = new ContentValues();
            long insertResult = -1;

            int i;
            if(jsonFileIndex == 0) {
                for(i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);

                    cv.put("worker_name", obj.getString("name"));

                    insertResult = db.insert("workers", null, cv);
                    if(insertResult != -1) {
                        insertResult = -1;
                        cv.clear();
                    } else {
                        ErrorShow2(30);
                        break;
                    }
                }
            } else {
                for(i = 0; i < array.length(); i++) {
                    JSONObject areaObj = array.getJSONObject(i);

                    cv.put("real_id", areaObj.getString("real_id"));
                    cv.put("name", areaObj.getString("area_name"));

                    insertResult = db.insert("areas", null, cv);
                    if(insertResult != -1 ) {
                        insertResult = -1;
                        cv.clear();

                        JSONArray deviceTypeArray = areaObj.getJSONArray("device_types");

                        for(int j = 0; j < deviceTypeArray.length(); j++) {
                            JSONObject deviceTypeArrayObj = deviceTypeArray.getJSONObject(j);

                            cv.put("real_id", deviceTypeArrayObj.getString("real_id"));
                            cv.put("area_id", (i + 1));
                            cv.put("name", deviceTypeArrayObj.getString("device_type_name"));

                            insertResult = db.insert("device_type", null, cv);
                            int ctd      = -1;

                            if(insertResult != -1) {
                                Cursor cursor = sqliteUtil.GetLastData(db, "device_type");
                                if(cursor.getCount() != 0 && cursor.moveToNext()) {
                                    ctd = cursor.getInt(cursor.getColumnIndex("id"));
                                }
                                cursor.close();

                                if(ctd != -1) {
                                    insertResult = -1;
                                    cv.clear();

                                    JSONArray pointsArray = deviceTypeArrayObj.getJSONArray("points");

                                    for(int k = 0; k < pointsArray.length(); k++) {
                                        JSONObject pointsArrayObj = pointsArray.getJSONObject(k);

                                        cv.put("real_id", pointsArrayObj.getString("real_id"));
                                        cv.put("device_type_id", ctd);
                                        cv.put("device_name", pointsArrayObj.getString("device_name"));
                                        cv.put("name", pointsArrayObj.getString("point_name"));
                                        cv.put("recognition_type_id", 2);

                                        insertResult = db.insert("points", null, cv);
                                        if(insertResult != -1) {
                                            insertResult = -1;
                                            cv.clear();
                                        } else {
                                            ErrorShow2(30);
                                            break;
                                        }
                                    }
                                } else {
                                    ErrorShow2(30);
                                    break;
                                }
                            } else {
                                ErrorShow2(30);
                                break;
                            }
                        }
                    } else {
                        ErrorShow2(30);
                        break;
                    }
                }
            }

            if(i == array.length()) {
                runOnUiThread(() -> {
                    importLog.append(">>>插入数据成功，正在保存数据...\n");
                    importProgressUpdate(100);
                });

                new Handler().postDelayed(() -> {
                    importProgressBlock.setVisibility(View.GONE);
                    loadingAction(10, "正在保存数据，请稍候...", View.VISIBLE);
                }, 1000);

                // 保存数据
                saveData();
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

            ErrorShow(10, "插入数据失败");
        }
    }

    /* 是否为 新增区域
    private boolean isNewArea(String name) {
        Cursor c = db.rawQuery("SELECT name FROM areas;", null);
        if(c.getCount() != 0) {
            while(c.moveToNext()) {
                if(name.equals(c.getString(c.getColumnIndex("name")))) { return false; }
                else { continue; }
            }
            return true;
        }

        return false;
    }
    // 是否为 新增设备类型
    private boolean isNewDeviceType(String name) {
        Cursor c = db.rawQuery("SELECT name FROM device_type;", null);
        if(c.getCount() != 0) {
            while(c.moveToNext()) {
                if(name.equals(c.getString(c.getColumnIndex("name")))) { return false; }
                else { continue; }
            }
            return true;
        }

        return false;
    }
    // 新增 设备数据
    private void startAddNewDevicesData(JSONArray array) {
        System.out.println(Log_Tag + ": startAddNewDevicesData()");

        try {
            ContentValues cv  = new ContentValues();
            long insertResult = -1;

            int i;
            for(i = 0; i < array.length(); i++) {
                JSONObject areaObj = array.getJSONObject(i);

                String areaName = areaObj.getString("area_name");
                // 新区域
                if(isNewArea(areaName)) {
                    System.out.println("新区域");

                    cv.put("name", areaName);

                    insertResult = db.insert("areas", null, cv);
                    if(insertResult != -1 ) {
                        insertResult = -1;
                        cv.clear();

                        int area_id = -1;
                        Cursor c    = sqliteUtil.GetLastData(db, "areas");

                        if(c.getCount() != 0 && c.moveToNext()) {
                            area_id = c.getInt(c.getColumnIndex("id"));
                            c.close();

                            if(area_id != -1) {
                                JSONArray deviceTypeArray = areaObj.getJSONArray("device_types");

                                for(int j = 0; j < deviceTypeArray.length(); j++) {
                                    JSONObject deviceTypeArrayObj = deviceTypeArray.getJSONObject(j);

                                    cv.put("area_id", area_id);
                                    cv.put("name", deviceTypeArrayObj.getString("device_type_name"));

                                    insertResult = db.insert("device_type", null, cv);
                                    int ctd      = -1;

                                    if(insertResult != -1) {
                                        Cursor cursor = sqliteUtil.GetLastData(db, "device_type");
                                        if(cursor.getCount() != 0 && cursor.moveToNext()) {
                                            ctd = cursor.getInt(cursor.getColumnIndex("id"));
                                        }
                                        cursor.close();

                                        if(ctd != -1) {
                                            insertResult = -1;
                                            cv.clear();

                                            JSONArray pointsArray = deviceTypeArrayObj.getJSONArray("points");

                                            for(int k = 0; k < pointsArray.length(); k++) {
                                                JSONObject pointsArrayObj = pointsArray.getJSONObject(k);

                                                cv.put("device_type_id", ctd);
                                                cv.put("device_name", pointsArrayObj.getString("device_name"));
                                                cv.put("name", pointsArrayObj.getString("point_name"));
                                                cv.put("recognition_type_id", 2);

                                                insertResult = db.insert("points", null, cv);
                                                if(insertResult != -1) {
                                                    insertResult = -1;
                                                    cv.clear();
                                                } else {
                                                    ErrorShow2(30);
                                                    break;
                                                }
                                            }
                                        } else {
                                            ErrorShow2(30);
                                            break;
                                        }
                                    } else {
                                        ErrorShow2(30);
                                        break;
                                    }
                                }
                            } else {
                                ErrorShow2(30);
                                break;
                            }
                        }
                    } else {
                        ErrorShow2(30);
                        break;
                    }
                }
                // 非新区域
                else {
                    System.out.println("非新区域");

                    // 获取 区域id
                    int areaId = -1;
                    Cursor c   = db.rawQuery("SELECT id FROM areas WHERE name = '" + areaName + "';", null);
                    if(c.getCount() != 0 && c.moveToNext()) {

                        areaId = c.getInt(c.getColumnIndex("id"));
                        c.close();

                        if(areaId != -1) {
                            JSONArray deviceTypeArray = areaObj.getJSONArray("device_types");

                            for(int j = 0; j < deviceTypeArray.length(); j++) {
                                JSONObject deviceTypeArrayObj = deviceTypeArray.getJSONObject(j);

                                String deviceTypeName = deviceTypeArrayObj.getString("device_type_name");
                                // 新增 新的设备类型
                                if(isNewDeviceType(deviceTypeName)) {
                                    System.out.println("新设备类型");

                                    cv.put("area_id", areaId);
                                    cv.put("name", deviceTypeName);

                                    insertResult = db.insert("device_type", null, cv);
                                    int ctd      = -1;

                                    if(insertResult != -1) {
                                        Cursor cursor = sqliteUtil.GetLastData(db, "device_type");
                                        if(cursor.getCount() != 0 && cursor.moveToNext()) {
                                            ctd = cursor.getInt(cursor.getColumnIndex("id"));
                                        }
                                        cursor.close();

                                        if(ctd != -1) {
                                            insertResult = -1;
                                            cv.clear();

                                            JSONArray pointsArray = deviceTypeArrayObj.getJSONArray("points");

                                            for(int k = 0; k < pointsArray.length(); k++) {
                                                JSONObject pointsArrayObj = pointsArray.getJSONObject(k);

                                                cv.put("device_type_id", ctd);
                                                cv.put("device_name", pointsArrayObj.getString("device_name"));
                                                cv.put("name", pointsArrayObj.getString("point_name"));
                                                cv.put("recognition_type_id", 2);

                                                insertResult = db.insert("points", null, cv);
                                                if(insertResult != -1) {
                                                    insertResult = -1;
                                                    cv.clear();
                                                } else {
                                                    ErrorShow2(30);
                                                    break;
                                                }
                                            }
                                        } else {
                                            ErrorShow2(30);
                                            break;
                                        }
                                    } else {
                                        ErrorShow2(30);
                                        break;
                                    }
                                }
                                // 非 新增的 设备类型，为 新的 点位
                                else {
                                    System.out.println("非新设备类型");

                                    // 获取 新增点位的 设备类型id
                                    Cursor c2 = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(areaId) + " AND name = '" + deviceTypeName + "';", null);
                                    if(c2.getCount() != 0 && c2.moveToNext()) {
                                        int ctd = -1;
                                        ctd     = c2.getInt(c2.getColumnIndex("id"));
                                        c2.close();

                                        if(ctd != -1) {
                                            insertResult = -1;
                                            cv.clear();

                                            JSONArray pointsArray = deviceTypeArrayObj.getJSONArray("points");

                                            for(int k = 0; k < pointsArray.length(); k++) {
                                                JSONObject pointsArrayObj = pointsArray.getJSONObject(k);

                                                cv.put("device_type_id", ctd);
                                                cv.put("device_name", pointsArrayObj.getString("device_name"));
                                                cv.put("name", pointsArrayObj.getString("point_name"));
                                                cv.put("recognition_type_id", 2);

                                                insertResult = db.insert("points", null, cv);
                                                if(insertResult != -1) {
                                                    insertResult = -1;
                                                    cv.clear();
                                                } else {
                                                    ErrorShow2(30);
                                                    break;
                                                }
                                            }
                                        } else {
                                            ErrorShow2(30);
                                            break;
                                        }
                                    }
                                }
                            }
                        } else {
                            ErrorShow2(30);
                            break;
                        }
                    }
                }
            }

            if(i == array.length()) {
                runOnUiThread(() -> {
                    importLog.append(">>>插入数据成功，正在保存数据...\n");
                    importProgressUpdate(100);
                });

                new Handler().postDelayed(() -> {
                    importProgressBlock.setVisibility(View.GONE);
                    loadingAction(10, "正在保存数据，请稍候...", View.VISIBLE);
                }, 1000);

                // 保存数据
                saveData();
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

            ErrorShow(10, "插入数据失败");
        }
    }
    */

    // 保存数据
    @SuppressLint("UseCompatLoadingForDrawables")
    private void saveData() {
        System.out.println(Log_Tag + ": saveData()");

        db.close();

        try {
            File f             = null;
            FileChannel input  = null;
            FileChannel output = null;

            if(jsonFileIndex == 0) {
                f      = new File(dateDirF.getAbsolutePath() + File.separator + "security_report.db");
                input  = new FileInputStream(f).getChannel();
                output = new FileOutputStream(Environment.getExternalStorageDirectory() + myApplication.SECURITY_REPORT_DB_PATH).getChannel();
            } else {
                f      = new File(dateDirF.getAbsolutePath() + File.separator + "devices_report.db");
                input  = new FileInputStream(f).getChannel();
                output = new FileOutputStream(Environment.getExternalStorageDirectory() + myApplication.DEVICES_REPORT_DB_PATH).getChannel();
            }

            output.transferFrom(input, 0, input.size());

            input.close();
            output.close();

            runOnUiThread(() -> {
                loadingAction(100, "数据导入成功", View.VISIBLE);
                importLog.append(">>>数据导入成功！\n");

                ToastUtils.show("数据导入成功！");
                importBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style));
            });

            new Handler().postDelayed(() -> loadingAction(100, "数据导入成功", View.GONE), 1000);
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

            ErrorShow(10, "保存数据失败(" + e.getMessage() + ")");
        }
    }

    // 导入数据进度 更新
    @SuppressLint("SetTextI18n")
    private void importProgressUpdate(int step) {
        runOnUiThread(() -> {
            importProgressNote.setText("已完成" + String.valueOf(step) + "%");
            importProgressBar.setProgress(step);
        });
    }

    // Error Show
    @SuppressLint("UseCompatLoadingForDrawables")
    private void ErrorShow(int progress, String info) {
        runOnUiThread(() -> {
            fileStateIcon.setImageDrawable(getResources().getDrawable(R.drawable.no));
            importBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_disable_style));

            loadingAction(progress, info, View.VISIBLE);
            importLog.append(">>>" + info + "\n");

            ToastUtils.show(info);
            new Handler().postDelayed(() -> loadingAction(progress, info, View.GONE), 1000);
        });
    }
    @SuppressLint("UseCompatLoadingForDrawables")
    private void ErrorShow2(int progress) {
        runOnUiThread(() -> {
            fileStateIcon.setImageDrawable(getResources().getDrawable(R.drawable.no));
            importBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_disable_style));
            importLog.append(">>>插入数据错误！\n");

            ToastUtils.show("插入数据错误！");
            importProgressUpdate(progress);

            new Handler().postDelayed(() -> importProgressBlock.setVisibility(View.GONE), 1000);
        });
    }

    // LoadingLayout 显示更新
    private void loadingAction(int step, String note, int visibility) {
        runOnUiThread(() -> {
            loadingLayout.setProgressStep(step);
            loadingLayout.setProgressNote(note);
            loadingLayout.setVisibility(visibility);
        });
    }

    /***********************************Activity Override Start***********************************/
    @Override
    protected void onStart() {
        System.out.println(Log_Tag + ": onStart()");
        super.onStart();
    }
    @Override
    protected void onResume() {
        System.out.println(Log_Tag + ": onResume()");
        super.onResume();
    }
    @Override
    protected void onPause() {
        System.out.println(Log_Tag + ": onPause()");
        super.onPause();
    }
    @Override
    protected void onStop() {
        System.out.println(Log_Tag + ": onStop()");
        super.onStop();
    }
    @Override
    protected void onRestart() {
        System.out.println(Log_Tag + ": onRestart()");
        super.onRestart();
    }
    @Override
    protected void onDestroy() {
        System.out.println(Log_Tag + ": onDestroy()");
        super.onDestroy();

        // 关闭 数据库
        if(db != null && db.isOpen()) { db.close(); }
    }
    /***********************************Activity Override End***********************************/

    // 返回键 监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            /*
            if(System.currentTimeMillis() - mExitTime > 2000) {
                Toast.makeText(MainActivity.this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();
            } else {
                MainActivity.this.finish();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
            */
        }

        return false;
    }
}