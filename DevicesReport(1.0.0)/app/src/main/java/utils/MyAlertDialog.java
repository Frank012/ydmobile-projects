package utils;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.demo.devicesreport.R;

public class MyAlertDialog extends RelativeLayout implements View.OnTouchListener {
    private RelativeLayout dialogBlock  = null;
    private RelativeLayout dialogLayout = null;

    public TextView title   = null;
    public TextView content = null;
    public TextView ok      = null;
    public TextView cancel  = null;

    public MyAlertDialog.ClickCallback clickCallback = null;

    // Constructor
    public MyAlertDialog(Context context) { this(context, null); }
    public MyAlertDialog(Context context, AttributeSet attrs) {
        super(context, attrs);

        View view = LayoutInflater.from(context).inflate(R.layout.my_alert_dialog, this, true);

        dialogBlock = view.findViewById(R.id.dialog_block);
        dialogBlock.setOnTouchListener((v, e) -> {
            v.performClick();
            this.setVisibility(GONE);
            return true;
        });

        dialogLayout = view.findViewById(R.id.dialog_layout);
        dialogLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        title        = view.findViewById(R.id.title);
        content      = view.findViewById(R.id.content);
        ok           = view.findViewById(R.id.ok);
        cancel       = view.findViewById(R.id.cancel);

        ok.setOnTouchListener(this);
        cancel.setOnTouchListener(this);
    }

    public void setTitle(String title) { this.title.setText(title); }
    public void setContent(String content) { this.content.setText(content); }

    public void setClickCallback(MyAlertDialog.ClickCallback callback) {
        this.clickCallback = callback;
    }

    public static interface ClickCallback {
        void onOKClick();
        void onCancelClick();
    }

    @Override
    public boolean onTouch(View v, MotionEvent e) {
        switch(v.getId()) {
            case R.id.ok:
                switch(e.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ok.setTextColor(getResources().getColor(R.color.my_alert_dialog_btn_compressed_color));
                        break;
                    case MotionEvent.ACTION_UP:
                        ok.setTextColor(getResources().getColor(R.color.my_alert_dialog_theme_color));

                        clickCallback.onOKClick();
                        this.setVisibility(GONE);

                        break;

                    case MotionEvent.ACTION_MOVE: break;
                    default: break;
                }
                break;

            case R.id.cancel:
                switch(e.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        cancel.setTextColor(getResources().getColor(R.color.my_alert_dialog_btn_compressed_color));
                        break;
                    case MotionEvent.ACTION_UP:
                        cancel.setTextColor(getResources().getColor(R.color.my_alert_dialog_theme_color));

                        clickCallback.onCancelClick();
                        this.setVisibility(GONE);

                        break;

                    case MotionEvent.ACTION_MOVE: break;
                    default: break;
                }
                break;

            default: break;
        }

        return true;
    }
}