package photoview;

import android.graphics.RectF;

public interface OnMatrixChangedListener { void onMatrixChanged(RectF rect); }