package com.demo.devicesreport;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoadingLayout extends RelativeLayout implements View.OnClickListener {
    public TextView progressStep = null;
    public TextView progressNote = null;

    public ClickCallback clickCallback = null;

    // Constructor
    public LoadingLayout(Context context) {
        this(context, null);
    }
    public LoadingLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        View view    = LayoutInflater.from(context).inflate(R.layout.loading_layout, this, true);
        progressStep = (TextView)view.findViewById(R.id.progress_step);
        progressNote = (TextView)view.findViewById(R.id.progress_note);

        view.setOnClickListener(this);
    }

    public void setProgressStep(int step) {
        this.progressStep.setText(String.valueOf(step) + "%");
    }
    public void setProgressNote(String note) {
        this.progressNote.setText(note);
    }

    public void setClickCallback(ClickCallback callback) {
        this.clickCallback = callback;
    }

    public static interface ClickCallback {
        void onViewClick();
    }

    @Override
    public void onClick(View view) {
        clickCallback.onViewClick();
    }
}
