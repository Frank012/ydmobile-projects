package com.demo.devicesreport;

import android.os.Parcel;
import android.os.Parcelable;

public class ReportData implements Parcelable {
    private String area;
    private String deviceType;
    private String positionName;
    private String recognitionType;
    private String result;
    private String unit;
    private String recognitionTime;
    private int    havaAlarm;
    private String alarmType;
    private String alarmDesc;
    private String photo;
    private String video;

    public static final Parcelable.Creator<ReportData> CREATOR = new Creator<ReportData>() {
        @Override
        public ReportData createFromParcel(Parcel source) {
            ReportData data = new ReportData();

            data.area            = source.readString();
            data.deviceType      = source.readString();
            data.positionName    = source.readString();
            data.recognitionType = source.readString();
            data.result          = source.readString();
            data.unit            = source.readString();
            data.recognitionTime = source.readString();
            data.havaAlarm       = source.readInt();
            data.alarmType       = source.readString();
            data.alarmDesc       = source.readString();
            data.photo           = source.readString();
            data.video           = source.readString();

            return data;
        }
        @Override
        public ReportData[] newArray(int size) { return new ReportData[size]; }
    };

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(area);
        parcel.writeString(deviceType);
        parcel.writeString(positionName);
        parcel.writeString(recognitionType);
        parcel.writeString(result);
        parcel.writeString(unit);
        parcel.writeString(recognitionTime);
        parcel.writeInt(havaAlarm);
        parcel.writeString(alarmType);
        parcel.writeString(alarmDesc);
        parcel.writeString(photo);
        parcel.writeString(video);
    }

    @Override
    public int describeContents() { return 0; }

    // SETS & GETS
    public void setArea(String v) { this.area = v; }
    public String getArea() { return area; }

    public void setDeviceType(String v) { this.deviceType = v; }
    public String getDeviceType() { return deviceType; }

    public void setPositionName(String v) { this.positionName = v; }
    public String getPositionName() { return positionName; }

    public void setRecognitionType(String v) { this.recognitionType = v; }
    public String getRecognitionType() { return recognitionType; }

    public void setResult(String v) { this.result = v; }
    public String getResult() { return result; }

    public void setUnit(String v) { this.unit = v; }
    public String getUnit() { return unit; }

    public void setRecognitionTime(String v) { this.recognitionTime = v; }
    public String getRecognitionTime() { return recognitionTime; }

    public void setHaveAlarm(int v) { this.havaAlarm = v; }
    public int getHaveAlarm() { return havaAlarm; }

    public void setAlarmType(String v) { this.alarmType = v; }
    public String getAlarmType() { return alarmType; }

    public void setAlarmDesc(String v) { this.alarmDesc = v; }
    public String getAlarmDesc() { return alarmDesc; }

    public void setPhoto(String v) { this.photo = v; }
    public String getPhoto() { return photo; }

    public void setVideo(String v) { this.video = v; }
    public String getVideo() { return video; }
}
