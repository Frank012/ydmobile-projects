package com.demo.devicesreport;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;

import utils.MyApplication;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class LaunchActivity extends Activity implements OnPermissionCallback {
    // Variables
    private MyApplication myApplication = null;
    private static final String Log_Tag = "LaunchActivity";

    // PermissionHelper
    private PermissionHelper permissionHelper   = null;
    private final static java.lang.String[] MULTIPLE_PERMISSIONS = new java.lang.String[] {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.BLUETOOTH_PRIVILEGED
    };
    private AlertDialog alertDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApplication = new MyApplication();
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onCreate()");
        setContentView(R.layout.activity_launch);

        // permission helper
        permissionHelper = PermissionHelper.getInstance(this);
        permissionHelper.setForceAccepting(false).request(MULTIPLE_PERMISSIONS);

        // 全屏
        LaunchActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View view = this.getWindow().getDecorView();
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    /***********************************Activity Override Start***********************************/
    @Override
    protected void onStart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStart()");
        super.onStart();
    }
    @Override
    protected void onResume() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onResume()");
        super.onResume();
    }
    @Override
    protected void onPause() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onPause()");
        super.onPause();
    }
    @Override
    protected void onStop() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStop()");
        super.onStop();
    }
    @Override
    protected void onRestart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onRestart()");
        super.onRestart();
    }
    @Override
    protected void onDestroy() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDestroy()");
        super.onDestroy();
    }
    /***********************************Activity Override End***********************************/

    /******************************PermissionHelper Override Start******************************/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": PermissionHelper onActivityResult()");
        System.out.println("requestCode/resultCode: " + String.valueOf(requestCode) + "/" + String.valueOf(resultCode));
        permissionHelper.onActivityForResult(requestCode);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": PermissionHelper onRequestPermissionsResult()");
        System.out.println("requestCode: " + String.valueOf(requestCode));

        for(int i = 0; i < permissions.length; i++) { System.out.println(permissions[i] + ": " + String.valueOf(grantResults[i])); }
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    @Override
    public void onPermissionGranted(@NonNull String[] permissionName) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": PermissionHelper onPermissionGranted()");
        for (String name : permissionName) { System.out.println(name); }

        // 3s后跳转到主页面
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(LaunchActivity.this, LoginActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

            LaunchActivity.this.finish();
        }, 1500);
    }
    @Override
    public void onPermissionDeclined(@NonNull String[] permissionName) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": PermissionHelper onPermissionDeclined()");
        for (String name : permissionName) { System.out.println(name); }
    }
    @Override
    public void onPermissionPreGranted(@NonNull String permissionsName) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": PermissionHelper onPermissionPreGranted()");
        System.out.println(permissionsName);
    }
    @Override
    public void onPermissionNeedExplanation(@NonNull String permissionName) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": PermissionHelper onPermissionNeedExplanation()");
        System.out.println(permissionName);

        String[] neededPermission = PermissionHelper.declinedPermissions(this, MULTIPLE_PERMISSIONS);
        StringBuilder builder = new StringBuilder(neededPermission.length);

        if (neededPermission.length > 0) {
            for (String permission : neededPermission) { builder.append(permission).append("\n"); }
        }

        AlertDialog alertDialog = getAlertDialog(neededPermission, builder.toString());
        if (!alertDialog.isShowing()) { alertDialog.show(); }
    }
    @Override
    public void onPermissionReallyDeclined(@NonNull String permissionName) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": PermissionHelper onPermissionReallyDeclined()");
        System.out.println(permissionName);
    }
    @Override
    public void onNoPermissionNeeded() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": PermissionHelper onNoPermissionNeeded()");
    }
    public AlertDialog getAlertDialog(final String[] permissions, final String permissionName) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": PermissionHelper getAlertDialog()");
        for (String name : permissions) { System.out.println(name); }
        System.out.println(permissionName);

        if (alertDialog == null) { alertDialog = new AlertDialog.Builder(this).setTitle("Permission Needs Explanation").create(); }

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Request", (dialog, which) -> permissionHelper.requestAfterExplanation(permissions));
        alertDialog.setMessage("Permissions need explanation (" + permissionName + ")");

        return alertDialog;
    }
    /******************************PermissionHelper Override End******************************/

    // 返回键 监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            // todo.
        }

        return false;
    }
}