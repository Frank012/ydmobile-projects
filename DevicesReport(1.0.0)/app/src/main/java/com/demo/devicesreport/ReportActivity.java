package com.demo.devicesreport;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.LayoutDirection;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.hjq.toast.ToastUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.salient.artplayer.MediaPlayerManager;
import org.salient.artplayer.VideoView;
import org.salient.artplayer.ui.ControlPanel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import photoview.PhotoView;
import utils.MyApplication;
import utils.SqliteUtil;

public class ReportActivity extends Activity implements View.OnClickListener {
    private static int RESEND_TIME = 0; // 重新上报次数

    // 蓝牙相关 请求码
    private static final int REQUEST_ENABLE_BT             = 2;
    private static final int REQUEST_ENABLE_ACTIVE_FOREVER = 3;

    // Utils
    SqliteUtil sqliteUtil = null;

    // Variables
    private MyApplication myApplication = null;
    private static final String Log_Tag = "ReportActivity";

    private boolean isDBOpened = false;
    private SQLiteDatabase db  = null;

    private final int COUNT = 0;
    private boolean isVideoPreviewShow = false;

    // BlueTooth
    private int BLUETOOTH_PAIRED_FAILURE_COUNT  = 0;
    private int BLUETOOTH_FOUNDED_FAILURE_COUNT = 0;

    public File dateDirF = null;

    public boolean isServerBluetoothFound = false;
    public boolean isBroadcastRegistered  = false;

    private BluetoothAdapter bluetoothAdapter = null;
    private BluetoothDevice serverBTDevice    = null;

    // UI
    public LoadingLayout loadingLayout = null;

    private Button newDataBtn = null;

    private GridLayout gridLayout = null;

    private Button reportBtn      = null;
    private Button reportLaterBtn = null;

    // 蓝牙设备列表
    private Animation refreshRotateAni = null;

    private boolean isBleLstRefresh        = false;
    private boolean isBleDiscoveryFinished = false;

    private int selectBluetoothIdx             = -1;
    private List<RadioButton> radioBtnLst      = new ArrayList<>();
    private List<BluetoothDevice> bluetoothLst = new ArrayList<>();

    private RelativeLayout bluetoothListBlock = null;
    private LinearLayout bluetoothLayout      = null;
    private ImageView refreshBleLstIcon       = null;

    // 上报Block
    private RelativeLayout reportProgressBlock = null;
    private TextView reportProgressNote        = null;
    private ProgressBar reportProgressBar      = null;

    // 照片、视频 预览
    private RelativeLayout imagePreviewLayout = null;
    private RelativeLayout videoPreviewLayout = null;

    private PhotoView photoPreview = null;
    private VideoView videoPreview = null;

    private ImageView videoPreviewBack = null;
    private TextView videoFileName     = null;

    // 蓝牙设备搜索 广播
    // 蓝牙连接回调
    private BluetoothConnectThread bluetoothConnectThread = null;
    private final BluetoothConnectCallback bluetoothConnectCallback = new BluetoothConnectCallback() {
        @Override
        public void connectSuccess(BluetoothSocket socket) {
            System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": BluetoothConnectCallback connectSuccess()");

            // 蓝牙连接异常，是否需要重新上报
            boolean isNeedReSend = false;
            if(!isNeedReSend) {
                runOnUiThread(() -> loadingAction(100, "连接成功，开始上报数据"));
                // 上报(发送) 数据
                // 该回调接口不在 主线程或UI线程内，Handler的创建必须在主线程或UI线程中，故这里不能使用空参创建Handler；需要得到主线程的Looper
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    loadingLayout.setVisibility(View.GONE);
                    reportData();
                }, 1000);
            } else {
                runOnUiThread(() -> loadingAction(100, "连接成功，开始重新发送数据"));
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    loadingLayout.setVisibility(View.GONE);
                    reSendZip(dateDirF.getAbsolutePath());
                }, 1000);
            }
        }
        @Override
        public void connectFailed(String errorMsg) {
            System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": BluetoothConnectCallback connectFailed()");
            // TODO. 重新连接
        }
        @Override
        public void connectCancel() {
            System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": BluetoothConnectCallback connectCancel()");
            // to do sth.
        }
    };

    private final BroadcastReceiver btfReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": BroadcastReceiver onReceive()");

            String action = intent.getAction();
            System.out.println("ACTION: " + action);

            BluetoothDevice dev = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            // 已搜索到的 蓝牙设备
            if(BluetoothDevice.ACTION_FOUND.equals(action)) {
                if(dev != null) {
                    if(dev.getName() != null && dev.getAddress() != null) {
                        System.out.println("Name: " + dev.getName() + ", " + "Address: " + dev.getAddress());

                        // TODO.蓝牙设备列表
                        bluetoothLst.add(dev);

                        /*
                        if(dev.getName().equals(myApplication.SERVER_BLUETOOTH_NAME)) {
                            BLUETOOTH_FOUNDED_FAILURE_COUNT = 0;

                            isServerBluetoothFound = true;
                            bluetoothAdapter.cancelDiscovery();

                            // 未匹配
                            if(dev.getBondState() == BluetoothDevice.BOND_NONE) {
                                loadingAction(90, "搜索到服务端蓝牙设备，开始匹配...");

                                // 蓝牙 匹配
                                BluetoothPair(dev);

                                // 注册 匹配状态 监听
                                IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
                                registerReceiver(btfReceiver, filter);
                                isBroadcastRegistered = true;
                            }
                            // 已匹配 则直接 连接
                            else {
                                loadingAction(90, "搜索到服务端蓝牙设备，开始连接...");

                                // 阻塞线程 开始连接 服务端蓝牙
                                bluetoothConnectThread = new BluetoothConnectThread(
                                        bluetoothAdapter.getRemoteDevice(dev.getAddress()),
                                        bluetoothConnectCallback);
                                bluetoothConnectThread.start();
                            }
                        }
                        */
                    }
                }
            }
            else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) { ToastUtils.show("开始搜索"); }
            else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if(!isBleDiscoveryFinished) {
                    isBleDiscoveryFinished = true;

                    // 去重
                    HashSet hashSet = new HashSet(bluetoothLst);
                    bluetoothLst.clear();
                    bluetoothLst.addAll(hashSet);

                    System.out.println(">>>>>>>>>>>>>>>>>>>>>");
                    for(BluetoothDevice device : bluetoothLst) { System.out.println(device.getName() + "[" + device.getAddress() + "]"); }
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<");

                    ToastUtils.show("搜索结束");
                    loadingAction(100, "搜索结束");

                    new Handler().postDelayed(() -> {
                        loadingLayout.setVisibility(View.GONE);
                        // BluetoothList 初始化
                        bluetoothListInitial();
                    }, 500);
                }

                /*
                ToastUtils.show("搜索结束");

                // 未搜索到 服务端蓝牙设备 请求重新搜索
                if(!isServerBluetoothFound) {
                    BLUETOOTH_FOUNDED_FAILURE_COUNT ++;
                    loadingAction(80, "未搜索到服务端蓝牙设备，请确认服务端蓝牙是否已开启");

                    new Handler().postDelayed(() -> {
                        loadingLayout.setVisibility(View.GONE);

                        // 对话框 提醒 重新搜索
                        AlertDialog dialog = new AlertDialog.Builder(ReportActivity.this).setTitle("搜索蓝牙设备").create();;

                        dialog.setMessage("未搜索到服务端蓝牙设备，是否重新搜索？");
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, w) -> {
                            bluetoothAdapter.startDiscovery();
                            loadingAction(50, "重新搜索服务端蓝牙设备(第" + String.valueOf(BLUETOOTH_FOUNDED_FAILURE_COUNT) + "次)，请稍候...");
                        });
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, w) -> dialog.dismiss());
                        dialog.show();
                    }, 500);
                }
                */
            }
            // 蓝牙匹配
            else if(action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                switch(dev.getBondState()) {
                    case BluetoothDevice.BOND_NONE:
                        BLUETOOTH_PAIRED_FAILURE_COUNT ++;
                        new Handler().postDelayed(() -> { BluetoothPair(dev); }, 500);
                        break;

                    case BluetoothDevice.BOND_BONDED:
                        BLUETOOTH_PAIRED_FAILURE_COUNT = 0;
                        loadingAction(99, "匹配成功，正在连接...");

                        bluetoothConnectThread = new BluetoothConnectThread(serverBTDevice, bluetoothConnectCallback);
                        bluetoothConnectThread.start();
                        break;

                    case BluetoothDevice.BOND_BONDING:
                        if(BLUETOOTH_PAIRED_FAILURE_COUNT == 0) { loadingAction(95, "正在匹配..."); }
                        else {
                            loadingAction(95, "匹配失败，正在重新匹配(第" + String.valueOf(BLUETOOTH_PAIRED_FAILURE_COUNT) + "次)...");
                        }

                        break;

                    default: break;
                }
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApplication = new MyApplication();
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onCreate()");
        setContentView(R.layout.activity_report);

        // 全屏
        ReportActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View view = this.getWindow().getDecorView();
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        // 蓝牙支持检测
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter != null) { System.out.println("设备支持蓝牙"); }
        else { ToastUtils.show("当前设备不支持蓝牙！"); }

        sqliteUtil = new SqliteUtil();

        // 数据库初始化
        db = SQLiteDatabase.openDatabase(MyApplication.DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
        if(db != null) { isDBOpened = true; }
        else { ToastUtils.show("数据库打开失败"); }

        // 判断是否 从LoginActivity跳转
        String FLAG = getIntent().getStringExtra("FLAG");
        System.out.println("FLAG: " + FLAG);

        // 获取当前用户 未上报数据
        if(FLAG != null && FLAG.equals("3")) { DataInit(); }

        UIInitial();             // UI Initial
        ReportItemListInitial(); // Report Item List Initial
    }

    // 设置蓝牙永久可见
    private void SetBlueToothShowForever() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": SetBlueToothShowForever()");

        @SuppressWarnings("rawtypes") Class btShowForeverClass   = null;
        Method btShowForeverMethod = null;

        try {
            btShowForeverClass  = Class.forName("android.bluetooth.BluetoothAdapter");

            //noinspection JavaReflectionMemberAccess
            btShowForeverMethod = btShowForeverClass.getMethod("setDiscoverableTimeout", String.class);
            btShowForeverMethod.setAccessible(true);

            btShowForeverMethod.invoke(btShowForeverClass.newInstance(), 300);

            // 双重保证 蓝牙永久可见、可连
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,0);
            startActivityForResult(intent, REQUEST_ENABLE_ACTIVE_FOREVER);
        } catch(Exception e) {
            System.out.println("Setting BTShowForever Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // 蓝牙初始化
    private void BTInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": BTInitial()");

        // 查询已配对的设备
        Set<BluetoothDevice> pairedDevs = bluetoothAdapter.getBondedDevices();
        if(pairedDevs.size() > 0) {
            System.out.println("-------------------已配对蓝牙设备------------------");
            for(BluetoothDevice dev : pairedDevs) {
                System.out.println("Name: " + dev.getName() + ", " + "Address: " + dev.getAddress());
                System.out.println("**********************************");
            }
            System.out.println("-------------------------------------------------");

            for(BluetoothDevice dev : pairedDevs) {
                if(dev.getName().equals(myApplication.SERVER_BLUETOOTH_NAME)) {
                    System.out.println("Server BlueTooth Found: " + dev.getName() + "(MAC=" + dev.getAddress() + ")");
                    break;
                }
            }

            // 开始搜索 服务端蓝牙
            new Handler().postDelayed(this::StartSearchServerBluetooth, 1000);
        } else { StartSearchServerBluetooth(); }
    }

    // 获取 未上报数据
    private void DataInit() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": DataInit()");

        if(isDBOpened) {
            Cursor cr = db.rawQuery("SELECT * FROM user_report WHERE user_id = " + String.valueOf(myApplication.USER_ID) + ";", null);

            if(cr.getCount() != 0) {
                List<Integer> reportIDs = new ArrayList<Integer>();

                while(cr.moveToNext()) { reportIDs.add(cr.getInt(cr.getColumnIndex("report_id"))); }
                cr.close();

                for(int i = 0; i < reportIDs.size(); ++i) {
                    Cursor cr2 = db.rawQuery("SELECT is_reported FROM reports WHERE id = " + String.valueOf(reportIDs.get(i)) + ";", null);

                    if(cr2.getCount() != 0 && cr2.moveToNext()) {
                        // 上报失败(未上报)
                        if(cr2.getInt(cr2.getColumnIndex("is_reported")) == 0) {
                            System.out.println("未上报id: " + String.valueOf(reportIDs.get(i)));
                            myApplication.UNREPORT_ID_LST.add(reportIDs.get(i));
                        }
                    } else { ToastUtils.show("获取数据上报状态错误"); }

                    cr2.close();
                }

                // TODO.test
                if(myApplication.UNREPORT_ID_LST.size() > 1) {
                    System.out.println("用户有多次未上报数据，将以往未上报数据全部重新打包上报");
                } else {
                    System.out.println("用户有1次未上报数据，将重新打包数据上报");
                }

                for(int j = 0; j < myApplication.UNREPORT_ID_LST.size(); ++j) {
                    Cursor cr3 = db.rawQuery("SELECT data_id FROM data_report WHERE report_id = " + String.valueOf(myApplication.UNREPORT_ID_LST.get(j)) + ";", null);

                    if(cr3.getCount() != 0) {
                        while(cr3.moveToNext()) {
                            int data_id = cr3.getInt(cr3.getColumnIndex("data_id"));
                            myApplication.UNREPORT_DATA_ID_LST.add(data_id);

                            Cursor cr4 = db.rawQuery("SELECT * FROM datas WHERE id = " + String.valueOf(data_id) + ";", null);
                            if(cr4.getCount() != 0) {
                                while(cr4.moveToNext()) {
                                    // 初始化数据
                                    ReportData data = new ReportData();

                                    // 区域
                                    int ced = cr4.getInt(cr4.getColumnIndex("area"));
                                    Cursor cr5 = db.rawQuery("SELECT name FROM areas WHERE id = " + String.valueOf(ced) + ";", null);
                                    if(cr5.getCount() != 0 && cr5.moveToNext()) {
                                        data.setArea(cr5.getString(cr5.getColumnIndex("name")));
                                    } else { ToastUtils.show("获取区域错误"); }
                                    cr5.close();

                                    // 设备类型
                                    int cpd = cr4.getInt(cr4.getColumnIndex("point_id"));
                                    Cursor cursor = db.rawQuery("SELECT device_type_id FROM points WHERE id = " + String.valueOf(cpd) + ";", null);
                                    if(cursor.getCount() != 0 && cursor.moveToNext()) {
                                        int ctd = cursor.getInt(cursor.getColumnIndex("device_type_id"));
                                        cursor.close();

                                        Cursor cursor1 = db.rawQuery("SELECT name FROM device_type WHERE id = " + String.valueOf(ctd) + ";", null);
                                        if(cursor1.getCount() != 0 && cursor1.moveToNext()) {
                                            data.setDeviceType(cursor1.getString(cursor1.getColumnIndex("name")));
                                            cursor1.close();
                                        } else { ToastUtils.show("获取设备类型错误");   }
                                    } else { ToastUtils.show("获取point_id错误"); }

                                    // 点位名称
                                    data.setPositionName(cr4.getString(cr4.getColumnIndex("point_name")));

                                    // 识别类型
                                    int recognitionTypeId = cr4.getInt(cr4.getColumnIndex("recognition_type_id"));
                                    Cursor cr6 = db.rawQuery("SELECT name FROM recognition_type WHERE id = " + String.valueOf(recognitionTypeId) +";", null);
                                    if(cr6.getCount() != 0 && cr6.moveToNext()) {
                                        data.setRecognitionType(cr6.getString(cr6.getColumnIndex("name")));
                                    } else { ToastUtils.show("获取识别类型错误"); }
                                    cr6.close();

                                    // 结果
                                    data.setResult(cr4.getString(cr4.getColumnIndex("result")));

                                    // 单位
                                    int unitId = cr4.getInt(cr4.getColumnIndex("unit_id"));
                                    Cursor cr7 = db.rawQuery("SELECT name FROM unit WHERE id = " + String.valueOf(unitId) + ";", null);
                                    if(cr7.getCount() != 0 && cr7.moveToNext()) {
                                        data.setUnit(cr7.getString(cr7.getColumnIndex("name")));
                                    } else { ToastUtils.show("获取单位错误");   }
                                    cr7.close();

                                    // 识别时间
                                    data.setRecognitionTime(cr4.getString(cr4.getColumnIndex("recognition_time")));
                                    // 异常
                                    data.setHaveAlarm(cr4.getInt(cr4.getColumnIndex("alarm_status")));

                                    if(data.getHaveAlarm() != 0) {
                                        // 告警类型
                                        int alarmTypeId = cr4.getInt(cr4.getColumnIndex("alarm_type_id"));
                                        Cursor cr8 = db.rawQuery("SELECT name FROM alarm_type WHERE id = " + String.valueOf(alarmTypeId) + ";", null);
                                        if(cr8.getCount() != 0 && cr8.moveToNext()) {
                                            data.setAlarmType(cr8.getString(cr8.getColumnIndex("name")));
                                        } else { ToastUtils.show("获取告警类型错误"); }
                                        cr8.close();

                                        // 异常描述
                                        data.setAlarmDesc(cr4.getString(cr4.getColumnIndex("alarm_description")));
                                        // 图片
                                        data.setPhoto(cr4.getString(cr4.getColumnIndex("image")) == null ?
                                                "" : cr4.getString(cr4.getColumnIndex("image")));
                                        // 视频
                                        data.setVideo(cr4.getString(cr4.getColumnIndex("video")) == null ?
                                                "" : cr4.getString(cr4.getColumnIndex("video")));
                                    } else {
                                        data.setAlarmType("");
                                        data.setAlarmDesc("");
                                        data.setPhoto("");
                                        data.setVideo("");
                                    }

                                    myApplication.DATALST.add(data);
                                }
                            } else { ToastUtils.show("获取未上报数据信息错误"); }
                            cr4.close();
                        }
                    } else { ToastUtils.show("获取未上报数据data_id错误"); }
                    cr3.close();
                }
            } else { ToastUtils.show("未知错误，获取未上报数据失败"); }
        }
    }

    // UI 初始化
    @SuppressLint({"UseCompatLoadingForDrawables", "ClickableViewAccessibility"})
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void UIInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": UIInitial()");

        // 添加
        newDataBtn = findViewById(R.id.new_data_btn);
        newDataBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> newDataBtn.setBackgroundColor(Color.parseColor("#498bdd")));
                    break;
                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> newDataBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style)));

                    // TODO.有未上报 数据时 设计原则上不允许再添加数据，只可 修改(这里对用户做出提示)
                    if(myApplication.UNREPORT_DATA_ID_LST.size() > 0) { ToastUtils.show("您有未上报数据，请及时上传"); }

                    Intent intent = new Intent(ReportActivity.this, MainActivity.class);
                    intent.putExtra("FLAG", "1");

                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

                    db.close();
                    ReportActivity.this.finish();

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 上报列表
        gridLayout = findViewById(R.id.report_table);

        // 上报
        reportBtn = findViewById(R.id.report_btn);
        reportBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> reportBtn.setBackgroundColor(Color.parseColor("#498bdd")));
                    break;
                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> reportBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style)));

                    if(checkAllDataInfoComplete()) {
                        // 蓝牙是否开启
                        if(!bluetoothAdapter.isEnabled()) {
                            loadingAction(10, "蓝牙未开启，正在开启蓝牙...");

                            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(intent, REQUEST_ENABLE_BT);
                        } else {
                            loadingAction(10, "蓝牙已开启，正在初始化...");
                            SetBlueToothShowForever();
                            BTInitial();
                        }
                    } else { ToastUtils.show("请完整填报上报数据"); }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });
        // TODO.稍后上报
        reportLaterBtn = findViewById(R.id.report_later_btn);
        reportLaterBtn.setOnTouchListener((v, e) -> {
            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        reportLaterBtn.setBackgroundColor(Color.parseColor("#498bdd"));
                        reportLaterBtn.setTextColor(Color.WHITE);
                    });
                    break;
                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        reportLaterBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.cancel_btn_normal_style));
                        reportLaterBtn.setTextColor(Color.BLACK);
                    });

                    if(checkAllDataInfoComplete()) {
                        AlertDialog dialog = new AlertDialog.Builder(this).setTitle("稍后上报数据").create();
                        dialog.setMessage("为保护数据安全，请尽快上报数据。确定稍后上报数据吗？");
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                            loadingAction(50, "正在保存数据，请稍候...");

                            myApplication.DATALST.clear();
                            myApplication.UNREPORT_ID_LST.clear();
                            myApplication.UNREPORT_DATA_ID_LST.clear();

                            myApplication.REPORT_ID                   = -1;
                            myApplication.REPORT_ID_INITIALED         = false;
                            myApplication.UNREPORT_NEW_DATA_INITIALED = false;

                            new Handler().postDelayed(() -> loadingAction(100, "数据保存成功，即将跳转到登录页..."), 1500);
                            new Handler().postDelayed(() -> {
                                Intent intent = new Intent(ReportActivity.this, LoginActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

                                loadingLayout.setVisibility(View.GONE);
                                db.close();

                                ReportActivity.this.finish();
                            }, 2000);
                        });
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> { dialog.dismiss(); });
                        dialog.show();
                    } else { ToastUtils.show("请完整填报上报数据"); }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 上报进度对话框
        reportProgressBlock = findViewById(R.id.report_progress_block);
        reportProgressBlock.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });
        RelativeLayout reportProgressLayout = findViewById(R.id.report_progress_layout);
        reportProgressLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        reportProgressNote = findViewById(R.id.report_progress_note);
        reportProgressBar  = findViewById(R.id.report_progress_bar);
        reportProgressBar.setMin(0);
        reportProgressBar.setMax(100);
        reportProgressBar.setIndeterminateTintMode(PorterDuff.Mode.SRC_ATOP);

        /// 蓝牙设备列表
        bluetoothListBlock = findViewById(R.id.bluetooth_list_block);
        bluetoothListBlock.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });
        RelativeLayout bluetoothListLayout = findViewById(R.id.bluetooth_list_layout);
        bluetoothListLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });
        // 列表
        bluetoothLayout = findViewById(R.id.select_item_list);
        // 选择蓝牙 确定
        TextView dialogOk = findViewById(R.id.dialog_ok_btn);
        dialogOk.setOnClickListener(view -> {
            if(selectBluetoothIdx != -1) {
                // 对话框 再次提示
                AlertDialog dialog = new AlertDialog.Builder(ReportActivity.this).setTitle("连接蓝牙").create();;
                dialog.setMessage("确定连接到蓝牙『" + bluetoothLst.get(selectBluetoothIdx).getName() + "』吗？");
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, w) -> {
                    dialog.dismiss();
                    bluetoothListBlock.setVisibility(View.GONE);

                    // 判断 是否已匹配
                    // 未匹配
                    if(bluetoothLst.get(selectBluetoothIdx).getBondState() == BluetoothDevice.BOND_NONE) {
                        loadingAction(10, "正在匹配蓝牙，请稍候...");

                        // 蓝牙 匹配
                        BluetoothPair(bluetoothLst.get(selectBluetoothIdx));

                        // 注册 匹配状态 监听
                        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
                        registerReceiver(btfReceiver, filter);
                        isBroadcastRegistered = true;
                    } else {
                        loadingAction(10, "正在连接蓝牙，请稍候...");

                        // 阻塞线程 开始连接 服务端蓝牙
                        bluetoothConnectThread = new BluetoothConnectThread(
                                bluetoothAdapter.getRemoteDevice(bluetoothLst.get(selectBluetoothIdx).getAddress()),
                                bluetoothConnectCallback);
                        bluetoothConnectThread.start();
                    }

                    // 清空
                    radioBtnLst.clear();
                    bluetoothLst.clear();

                    selectBluetoothIdx = -1;
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, w) -> { dialog.dismiss(); });
                dialog.show();
            } else { ToastUtils.show("请选择需要连接的蓝牙设备"); }
        });
        // 蓝牙设备 取消
        TextView dialogCancel = findViewById(R.id.dialog_cancel_btn);
        dialogCancel.setOnClickListener(view -> {
            bluetoothListBlock.setVisibility(View.GONE);
            bluetoothLayout.removeAllViews();
            bluetoothLst.clear();

            selectBluetoothIdx = -1;
            radioBtnLst.clear();
        });
        // 蓝牙设备 刷新
        refreshBleLstIcon = findViewById(R.id.refresh_icon);
        refreshBleLstIcon.setOnClickListener(v -> {
            AlertDialog dialog = new AlertDialog.Builder(ReportActivity.this).setTitle("刷新蓝牙设备列表").create();;
            dialog.setMessage("未找到上报蓝牙设备，确定重新搜索吗？");
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, w) -> {
                if(!isBleLstRefresh) {
                    isBleLstRefresh = true;

                    refreshRotateAni = AnimationUtils.loadAnimation(ReportActivity.this, R.anim.ble_list_refresh);
                    refreshBleLstIcon.startAnimation(refreshRotateAni);

                    bluetoothLayout.removeAllViews();
                    radioBtnLst.clear();

                    bluetoothLst.clear();
                    selectBluetoothIdx = -1;

                    bluetoothAdapter.startDiscovery();
                    isBleDiscoveryFinished = false;

                    loadingAction(50, "正在重新搜索蓝牙设备，请稍候...");

                    // 定时取消蓝牙搜索
                    cancelBleDiscoveryInTime();
                }
            });
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, w) -> { dialog.dismiss(); });
            dialog.show();
        });

        /// 预览
        // 照片
        imagePreviewLayout = findViewById(R.id.preview_layout);
        imagePreviewLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            imagePreviewLayout.setVisibility(View.GONE);
            return true;
        });
        photoPreview = findViewById(R.id.photo_preview);

        // 视频
        videoPreviewLayout = findViewById(R.id.video_preview_layout);
        videoPreview       = findViewById(R.id.video_preview);

        videoPreviewBack   = findViewById(R.id.video_preview_back);
        videoFileName      = findViewById(R.id.video_file_name);

        videoPreviewBack.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> videoPreviewBack.setBackgroundColor(Color.parseColor("#dcdcdc")));
                    break;
                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> videoPreviewBack.setBackgroundColor(Color.TRANSPARENT));

                    MediaPlayerManager.instance().releasePlayerAndView(this);

                    runOnUiThread(() -> {
                        videoFileName.setText("");
                        videoPreviewLayout.setVisibility(View.GONE);
                    });

                    isVideoPreviewShow = false;

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });


        // Loading Layout
        loadingLayout = super.findViewById(R.id.loading_layout);
        loadingLayout.setClickCallback(() -> { /* todo sth. */ });
    }

    // BluetoothList Initial 蓝牙设备列表初始化
    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    private void bluetoothListInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": bluetoothListInitial()");

        if(isBleLstRefresh) {
            isBleLstRefresh = false;
            refreshBleLstIcon.clearAnimation();
        }

        for(int i = 0; i < bluetoothLst.size(); i++) {
            LinearLayout linearLayout        = new LinearLayout(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);

            linearLayout.setLayoutParams(params);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setTag("DEV_ITEM_" + String.valueOf(i));

            if(i % 2 == 0) { linearLayout.setBackgroundColor(Color.WHITE); }

            // RadioButton
            RadioButton radioBtn                     = new RadioButton(this);
            LinearLayout.LayoutParams radioBtnParams = new LinearLayout.LayoutParams(32, 32);
            radioBtnParams.setMarginStart(24);
            radioBtnParams.gravity = Gravity.CENTER;
            radioBtn.setLayoutParams(radioBtnParams);

            radioBtn.setTextSize(0);
            radioBtn.setButtonDrawable(null);
            radioBtn.setButtonDrawable(getResources().getDrawable(R.drawable.radio_btn_selector));
            radioBtn.setGravity(Gravity.CENTER_VERTICAL);
            radioBtn.setChecked(false);

            radioBtnLst.add(radioBtn);

            // TextView
            TextView textView                        = new TextView(this);
            LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            textViewParams.setMarginStart(12);
            textViewParams.gravity = Gravity.CENTER;
            textViewParams.setLayoutDirection(LayoutDirection.INHERIT);
            textView.setLayoutParams(textViewParams);

            textView.setText(bluetoothLst.get(i).getName());
            textView.setTextSize(16);
            textView.setTextColor(Color.BLACK);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            textView.setGravity(Gravity.CENTER_VERTICAL);

            linearLayout.addView(radioBtn);
            linearLayout.addView(textView);

            linearLayout.setOnClickListener(v -> {
                String tagStr = v.getTag().toString();
                int idx       = Integer.parseInt(tagStr.substring(9, tagStr.length()));

                runOnUiThread(() -> {
                    radioBtnLst.get(idx).setChecked(true);

                    for(int j = 0; j < radioBtnLst.size(); j++) {
                        if(idx != j) { radioBtnLst.get(j).setChecked(false); }
                    }
                });

                selectBluetoothIdx = idx;
            });

            bluetoothLayout.addView(linearLayout);
        }

        bluetoothListBlock.setVisibility(View.VISIBLE);
    }
    // 8s 定时取消 蓝牙搜索
    private void cancelBleDiscoveryInTime() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": cancelBleDiscoveryInTime()");
        new Handler().postDelayed(() -> bluetoothAdapter.cancelDiscovery(), 8000);
    }

    // 检查所有数据的 信息完整性
    private boolean checkAllDataInfoComplete() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": checkAllDataInfoComplete()");

        // 检查数据完整性
        boolean isComplete = true;

        for(int i = 0; i < myApplication.DATALST.size(); ++i) {
            ReportData data = myApplication.DATALST.get(i);

            // 无异常(则为 数值 填报 巡检结果)
            if(data.getHaveAlarm() == 0) {
                if(data.getArea().equals("") || data.getDeviceType().equals("") || data.getPositionName().equals("") ||
                        data.getRecognitionType().equals("") || data.getResult().equals("") || data.getUnit().equals("") ||
                        data.getRecognitionTime().equals("")) {
                    isComplete = false;
                    System.out.println("数据(" + String.valueOf(i + 1) + ")填报信息不完整");
                    break;
                }
            } else {
                if(data.getArea().equals("") || data.getDeviceType().equals("") || data.getPositionName().equals("") ||
                        data.getRecognitionType().equals("") || data.getResult().equals("") || data.getUnit().equals("") ||
                        data.getRecognitionTime().equals("") || data.getAlarmType().equals("") || data.getAlarmDesc().equals("") ||
                        data.getPhoto().equals("")) {
                    isComplete = false;
                    System.out.println("数据(" + String.valueOf(i + 1) + ")填报信息不完整");
                    break;
                }
            }
        }

        return isComplete;
    }

    // Report Item List Initial
    @SuppressLint("UseCompatLoadingForDrawables")
    private void ReportItemListInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": ReportItemListInitial()");

        int size = myApplication.DATALST.size();
        int SIZE = size + 1;

        gridLayout.setRowCount(Math.max(SIZE, 10));

        for(int i = 0; i < (Math.max(SIZE, 10)); ++i) {
            // Table Header
            if(i == 0) {
                LinearLayout tableHeader               = new LinearLayout(this);
                LinearLayout.LayoutParams tableHeaderP = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60);
                tableHeader.setLayoutParams(tableHeaderP);

                // 序号(Index Column)
                TextView indexTv              = new TextView(this);
                ViewGroup.LayoutParams indexP = new ViewGroup.LayoutParams(75, ViewGroup.LayoutParams.MATCH_PARENT);
                indexTv.setLayoutParams(indexP);

                indexTv.setGravity(Gravity.CENTER); indexTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                indexTv.setText("序号");
                indexTv.setTextSize(14); indexTv.setTextColor(Color.BLACK); indexTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); indexTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 区域(Area Column)
                TextView areaTv              = new TextView(this);
                ViewGroup.LayoutParams areaP = new ViewGroup.LayoutParams(250, ViewGroup.LayoutParams.MATCH_PARENT);
                areaTv.setLayoutParams(areaP);

                areaTv.setGravity(Gravity.CENTER); areaTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                areaTv.setText("区域");
                areaTv.setTextSize(14); areaTv.setTextColor(Color.BLACK); areaTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); areaTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 设备类型(Device Type Column)
                TextView deviceTypeTv              = new TextView(this);
                ViewGroup.LayoutParams deviceTypeP = new ViewGroup.LayoutParams(360, ViewGroup.LayoutParams.MATCH_PARENT);
                deviceTypeTv.setLayoutParams(deviceTypeP);

                deviceTypeTv.setGravity(Gravity.CENTER); deviceTypeTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                deviceTypeTv.setText("设备类型");
                deviceTypeTv.setTextSize(14); deviceTypeTv.setTextColor(Color.BLACK); deviceTypeTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); deviceTypeTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 点位名称(Position Name Column)
                TextView positionNameTv              = new TextView(this);
                ViewGroup.LayoutParams positionNameP = new ViewGroup.LayoutParams(600, ViewGroup.LayoutParams.MATCH_PARENT);
                positionNameTv.setLayoutParams(positionNameP);

                positionNameTv.setGravity(Gravity.CENTER); positionNameTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                positionNameTv.setText("点位名称");
                positionNameTv.setTextSize(14); positionNameTv.setTextColor(Color.BLACK); positionNameTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); positionNameTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 识别类型(Recognition Type Column)
                TextView recognitionTypeTv              = new TextView(this);
                ViewGroup.LayoutParams recognitionTypeP = new ViewGroup.LayoutParams(240, ViewGroup.LayoutParams.MATCH_PARENT);
                recognitionTypeTv.setLayoutParams(recognitionTypeP);

                recognitionTypeTv.setGravity(Gravity.CENTER); recognitionTypeTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                recognitionTypeTv.setText("识别类型");
                recognitionTypeTv.setTextSize(14); recognitionTypeTv.setTextColor(Color.BLACK); recognitionTypeTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); recognitionTypeTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 巡检结果(Result Column)
                TextView resultTv              = new TextView(this);
                ViewGroup.LayoutParams resultP = new ViewGroup.LayoutParams(240, ViewGroup.LayoutParams.MATCH_PARENT);
                resultTv.setLayoutParams(resultP);

                resultTv.setGravity(Gravity.CENTER); resultTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                resultTv.setText("巡检结果");
                resultTv.setTextSize(14); resultTv.setTextColor(Color.BLACK); resultTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); resultTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 识别时间(Recognition Time Column)
                TextView recognitionTimeTv              = new TextView(this);
                ViewGroup.LayoutParams recognitionTimeP = new ViewGroup.LayoutParams(410, ViewGroup.LayoutParams.MATCH_PARENT);
                recognitionTimeTv.setLayoutParams(recognitionTimeP);

                recognitionTimeTv.setGravity(Gravity.CENTER); recognitionTimeTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                recognitionTimeTv.setText("识别时间");
                recognitionTimeTv.setTextSize(14); recognitionTimeTv.setTextColor(Color.BLACK); recognitionTimeTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); recognitionTimeTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 异常(Have Alarm Column)
                TextView haveAlarmTv              = new TextView(this);
                ViewGroup.LayoutParams haveAlarmP = new ViewGroup.LayoutParams(140, ViewGroup.LayoutParams.MATCH_PARENT);
                haveAlarmTv.setLayoutParams(haveAlarmP);

                haveAlarmTv.setGravity(Gravity.CENTER); haveAlarmTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                haveAlarmTv.setText("异常有无");
                haveAlarmTv.setTextSize(14); haveAlarmTv.setTextColor(Color.BLACK); haveAlarmTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); haveAlarmTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 告警类型(Alarm Type Column)
                TextView alarmTypeTv              = new TextView(this);
                ViewGroup.LayoutParams alarmTypeP = new ViewGroup.LayoutParams(240, ViewGroup.LayoutParams.MATCH_PARENT);
                alarmTypeTv.setLayoutParams(alarmTypeP);

                alarmTypeTv.setGravity(Gravity.CENTER); alarmTypeTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                alarmTypeTv.setText("告警类型");
                alarmTypeTv.setTextSize(14); alarmTypeTv.setTextColor(Color.BLACK); alarmTypeTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); alarmTypeTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 异常描述(Alarm Desc Column)
                TextView alarmDescTv              = new TextView(this);
                ViewGroup.LayoutParams alarmDescP = new ViewGroup.LayoutParams(480, ViewGroup.LayoutParams.MATCH_PARENT);
                alarmDescTv.setLayoutParams(alarmDescP);

                alarmDescTv.setGravity(Gravity.CENTER); alarmDescTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                alarmDescTv.setText("异常描述");
                alarmDescTv.setTextSize(14); alarmDescTv.setTextColor(Color.BLACK); alarmDescTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); alarmDescTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 采集信息(Data Column)
                TextView dataTv              = new TextView(this);
                ViewGroup.LayoutParams dataP = new ViewGroup.LayoutParams(160, ViewGroup.LayoutParams.MATCH_PARENT);
                dataTv.setLayoutParams(dataP);

                dataTv.setGravity(Gravity.CENTER); dataTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                dataTv.setText("采集信息");
                dataTv.setTextSize(14); dataTv.setTextColor(Color.BLACK); dataTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); dataTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                // 操作(Operation Column)
                TextView operationTv              = new TextView(this);
                ViewGroup.LayoutParams operationP = new ViewGroup.LayoutParams(80, ViewGroup.LayoutParams.MATCH_PARENT);
                operationTv.setLayoutParams(operationP);

                operationTv.setGravity(Gravity.CENTER); operationTv.setBackgroundDrawable(getResources().getDrawable(R.drawable.grid_cell_border));
                operationTv.setText("操作");
                operationTv.setTextSize(14); operationTv.setTextColor(Color.BLACK); operationTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER); operationTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

                tableHeader.addView(indexTv);
                tableHeader.addView(areaTv);
                tableHeader.addView(deviceTypeTv);
                tableHeader.addView(positionNameTv);
                tableHeader.addView(recognitionTypeTv);
                tableHeader.addView(resultTv);
                tableHeader.addView(recognitionTimeTv);
                tableHeader.addView(haveAlarmTv);
                tableHeader.addView(alarmTypeTv);
                tableHeader.addView(alarmDescTv);
                tableHeader.addView(dataTv);
                tableHeader.addView(operationTv);

                gridLayout.addView(tableHeader);
            } else {
                LinearLayout tableItem                    = new LinearLayout(this);
                LinearLayout.LayoutParams tableItemParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 80);
                tableItem.setLayoutParams(tableItemParams);

                if(i % 2 != 0) { tableItem.setBackgroundColor(Color.parseColor("#f6faff")); }

                // 序号
                TextView indexTv              = new TextView(this);
                ViewGroup.LayoutParams indexP = new ViewGroup.LayoutParams(75, ViewGroup.LayoutParams.MATCH_PARENT);

                indexTv.setLayoutParams(indexP); indexTv.setGravity(Gravity.CENTER);
                indexTv.setText((i + 1) > SIZE ? "" : String.valueOf(i));
                indexTv.setTextSize(14); indexTv.setTextColor(Color.parseColor("#77808c")); indexTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                // 区域
                TextView areaTv              = new TextView(this);
                ViewGroup.LayoutParams areaP = new ViewGroup.LayoutParams(250, ViewGroup.LayoutParams.MATCH_PARENT);

                areaTv.setLayoutParams(areaP); areaTv.setGravity(Gravity.CENTER);
                areaTv.setText((i + 1) > SIZE ? "" : myApplication.DATALST.get(i - 1).getArea());
                areaTv.setTextSize(14); areaTv.setTextColor(Color.parseColor("#77808c")); areaTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                // 设备类型
                TextView deviceTypeTv              = new TextView(this);
                ViewGroup.LayoutParams deviceTypeP = new ViewGroup.LayoutParams(360, ViewGroup.LayoutParams.MATCH_PARENT);

                deviceTypeTv.setLayoutParams(deviceTypeP); deviceTypeTv.setGravity(Gravity.CENTER);
                deviceTypeTv.setText((i + 1) > SIZE ? "" : myApplication.DATALST.get(i - 1).getDeviceType());
                deviceTypeTv.setTextSize(14); deviceTypeTv.setTextColor(Color.parseColor("#77808c")); deviceTypeTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                // 点位名称
                TextView positionNameTv              = new TextView(this);
                ViewGroup.LayoutParams positionNameP = new ViewGroup.LayoutParams(600, ViewGroup.LayoutParams.MATCH_PARENT);

                positionNameTv.setLayoutParams(positionNameP); positionNameTv.setGravity(Gravity.CENTER);
                positionNameTv.setText((i + 1) > SIZE ? "" : myApplication.DATALST.get(i - 1).getPositionName());
                positionNameTv.setTextSize(14); positionNameTv.setTextColor(Color.parseColor("#77808c")); positionNameTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                // 识别类型
                TextView recognitionTypeTv              = new TextView(this);
                ViewGroup.LayoutParams recognitionTypeP = new ViewGroup.LayoutParams(240, ViewGroup.LayoutParams.MATCH_PARENT);

                recognitionTypeTv.setLayoutParams(recognitionTypeP); recognitionTypeTv.setGravity(Gravity.CENTER);
                recognitionTypeTv.setText((i + 1) > SIZE ? "" : myApplication.DATALST.get(i - 1).getRecognitionType());
                recognitionTypeTv.setTextSize(14); recognitionTypeTv.setTextColor(Color.parseColor("#77808c")); recognitionTypeTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                // 巡检结果
                TextView resultTv              = new TextView(this);
                ViewGroup.LayoutParams resultP = new ViewGroup.LayoutParams(240, ViewGroup.LayoutParams.MATCH_PARENT);

                resultTv.setLayoutParams(resultP); resultTv.setGravity(Gravity.CENTER);
                resultTv.setText((i + 1) > SIZE ? "" : (myApplication.DATALST.get(i - 1).getResult().equals("") ?
                                                        "" : myApplication.DATALST.get(i - 1).getResult() + myApplication.DATALST.get(i - 1).getUnit()));
                resultTv.setTextSize(14); resultTv.setTextColor(Color.parseColor("#77808c")); resultTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                // 识别时间
                TextView recognitionTimeTv              = new TextView(this);
                ViewGroup.LayoutParams recognitionTimeP = new ViewGroup.LayoutParams(410, ViewGroup.LayoutParams.MATCH_PARENT);

                recognitionTimeTv.setLayoutParams(recognitionTimeP); recognitionTimeTv.setGravity(Gravity.CENTER);
                recognitionTimeTv.setText((i + 1) > SIZE ? "" : myApplication.DATALST.get(i - 1).getRecognitionTime());
                recognitionTimeTv.setTextSize(14); recognitionTimeTv.setTextColor(Color.parseColor("#77808c")); recognitionTimeTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                // 异常
                TextView haveAlarmTv              = new TextView(this);
                ViewGroup.LayoutParams haveAlarmP = new ViewGroup.LayoutParams(140, ViewGroup.LayoutParams.MATCH_PARENT);

                haveAlarmTv.setLayoutParams(haveAlarmP); haveAlarmTv.setGravity(Gravity.CENTER);
                haveAlarmTv.setText((i + 1) > SIZE ? "" : (myApplication.DATALST.get(i - 1).getHaveAlarm() == 0 ? "无" : "有"));
                haveAlarmTv.setTextSize(14); haveAlarmTv.setTextColor(Color.parseColor("#77808c")); haveAlarmTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                // 告警类型
                TextView alarmTypeTv              = new TextView(this);
                ViewGroup.LayoutParams alarmTypeP = new ViewGroup.LayoutParams(240, ViewGroup.LayoutParams.MATCH_PARENT);

                alarmTypeTv.setLayoutParams(alarmTypeP); alarmTypeTv.setGravity(Gravity.CENTER);
                alarmTypeTv.setText((i + 1 ) > SIZE ? "" : myApplication.DATALST.get(i - 1).getAlarmType());
                alarmTypeTv.setTextSize(14); alarmTypeTv.setTextColor(Color.parseColor("#77808c")); alarmTypeTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                // 异常描述
                TextView alarmDescTv              = new TextView(this);
                ViewGroup.LayoutParams alarmDescP = new ViewGroup.LayoutParams(480, ViewGroup.LayoutParams.MATCH_PARENT);

                alarmDescTv.setLayoutParams(alarmDescP); alarmDescTv.setGravity(Gravity.CENTER);
                alarmDescTv.setText((i + 1) > SIZE ? "" : myApplication.DATALST.get(i - 1).getAlarmDesc());
                alarmDescTv.setTextSize(14); alarmDescTv.setTextColor(Color.parseColor("#77808c")); alarmDescTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                alarmDescTv.setSingleLine();
                alarmDescTv.setEllipsize(TextUtils.TruncateAt.END);

                /// 采集信息
                RelativeLayout dataL              = new RelativeLayout(this);
                RelativeLayout.LayoutParams dataP = new RelativeLayout.LayoutParams(160, RelativeLayout.LayoutParams.MATCH_PARENT);
                dataL.setLayoutParams(dataP);
                // 图片
                TextView photoTv                   = new TextView(this);
                RelativeLayout.LayoutParams photoP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                photoP.addRule(RelativeLayout.CENTER_VERTICAL); photoP.addRule(RelativeLayout.ALIGN_PARENT_START);
                photoP.setMarginStart(10);
                photoTv.setLayoutParams(photoP);

                photoTv.setText((i + 1) > SIZE ? "" : (myApplication.DATALST.get(i - 1).getPhoto().equals("") ? "" : "图片"));
                photoTv.setTextSize(14); photoTv.setTextColor(Color.parseColor("#5495e5")); photoTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                if(i < SIZE) {
                    photoTv.setTag("PHOTO_VIEW_" + myApplication.DATALST.get(i - 1).getPhoto());
                    photoTv.setOnClickListener(this);
                }
                // 视频
                TextView videoTv                   = new TextView(this);
                RelativeLayout.LayoutParams videoP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                videoP.addRule(RelativeLayout.CENTER_VERTICAL); videoP.addRule(RelativeLayout.ALIGN_PARENT_END);
                videoP.setMarginEnd(10);
                videoTv.setLayoutParams(videoP);

                videoTv.setText((i + 1) > SIZE ? "" : (myApplication.DATALST.get(i - 1).getVideo().equals("") ? "" : "视频"));
                videoTv.setTextSize(14); videoTv.setTextColor(Color.parseColor("#5495e5")); videoTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                if(i  < SIZE) {
                    videoTv.setTag("VIDEO_VIEW_" + myApplication.DATALST.get(i - 1).getVideo());
                    videoTv.setOnClickListener(this);
                }

                dataL.addView(photoTv); dataL.addView(videoTv);

                // 操作
                TextView operationTv              = new TextView(this);
                ViewGroup.LayoutParams operationP = new ViewGroup.LayoutParams(80, ViewGroup.LayoutParams.MATCH_PARENT);

                operationTv.setLayoutParams(operationP); operationTv.setGravity(Gravity.CENTER);
                operationTv.setText((i + 1) > SIZE ? "" : "修改");
                operationTv.setTextSize(14); operationTv.setTextColor(Color.parseColor("#5495e5")); operationTv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                if(i < SIZE) {
                    operationTv.setTag("OPERATION_TEXT_VIEW_" + String.valueOf(i -1));
                    operationTv.setOnClickListener(this);
                }

                tableItem.addView(indexTv);
                tableItem.addView(areaTv);
                tableItem.addView(deviceTypeTv);
                tableItem.addView(positionNameTv);
                tableItem.addView(recognitionTypeTv);
                tableItem.addView(resultTv);
                tableItem.addView(recognitionTimeTv);
                tableItem.addView(haveAlarmTv);
                tableItem.addView(alarmTypeTv);
                tableItem.addView(alarmDescTv);
                tableItem.addView(dataL);
                tableItem.addView(operationTv);

                gridLayout.addView(tableItem);
            }
        }
    }

    // 开始搜索服务端蓝牙
    private void StartSearchServerBluetooth() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": StartSearchServerBluetooth()");

        // 开始搜索设备(广播)
        loadingAction(50, "开始搜索服务端蓝牙设备，请稍候...");

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(btfReceiver, filter);

        isBroadcastRegistered = true;

        bluetoothAdapter.startDiscovery();
        isBleDiscoveryFinished = false;

        cancelBleDiscoveryInTime();
    }
    // 蓝牙匹配
    private void BluetoothPair(BluetoothDevice d) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": BluetoothPair()");

        // 反射机制 开始匹配
        serverBTDevice = bluetoothAdapter.getRemoteDevice(d.getAddress());
        try {
            Method method = BluetoothDevice.class.getMethod("createBond", null);
            method.invoke(serverBTDevice,null);
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

            loadingAction(90, "匹配失败Exception：" + e.getMessage());
            new Handler().postDelayed(() -> loadingLayout.setVisibility(View.GONE), 500);
        }
    }

    @Override
    public void onClick(View v) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onClick()");
        System.out.println(v.getTag().toString());

        /// 数据预览
        // 图片
        if(v.getTag().toString().startsWith("PHOTO_VIEW_")) {
            String tag  = v.getTag().toString();
            String path = tag.substring(11, tag.length());

            if(!path.equals("")) {
                runOnUiThread(() -> {
                    photoPreview.setImageDrawable(new BitmapDrawable(BitmapFactory.decodeFile(path)));
                    imagePreviewLayout.setVisibility(View.VISIBLE);
                });
            }
        }
        // 视频
        if(v.getTag().toString().startsWith("VIDEO_VIEW_")) {
            String tag  = v.getTag().toString();
            String path = tag.substring(11, tag.length());

            if(!path.equals("")) {
                runOnUiThread(() -> {
                    videoPreviewLayout.setVisibility(View.VISIBLE);
                    runOnUiThread(() -> videoFileName.setText(new File(path).getName()));

                    videoPreview.setUp(path);
                    videoPreview.setControlPanel(new ControlPanel(this));
                    videoPreview.start();

                    isVideoPreviewShow = true;
                });
            }
        }
        // 修改
        if(v.getTag().toString().startsWith("OPERATION_TEXT_VIEW_")) {
            String tagStr = v.getTag().toString();

            int dataIndx  = Integer.parseInt(tagStr.substring(20, tagStr.length()));
            System.out.println("dataIndx: " + dataIndx);

            Intent intent = new Intent(ReportActivity.this, MainActivity.class);
            intent.putExtra("FLAG", "2");
            intent.putExtra("DATA_INDEX", dataIndx);

            startActivity(intent);
            overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

            db.close();
            ReportActivity.this.finish();
        }
    }

    // Report Data 上报(发送) 数据
    @SuppressLint("SetTextI18n")
    private void reportData() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": reportData()");

        runOnUiThread(() -> {
            reportProgressNote.setText("已完成1%");
            reportProgressBar.setProgress(1);
            reportProgressBlock.setVisibility(View.VISIBLE);
        });

        // 时间戳
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        Date date            = new Date(System.currentTimeMillis());
        String time_str      = sdf.format(date);

        String dateDir = myApplication.APP_DATA_PATH + File.separator + time_str;
        dateDirF       = new File(dateDir);

        if(dateDirF.mkdir()) { System.out.println("数据上报日期文件夹创建成功"); }

        // 更新 reports表 时间
        updateReportTime(time_str);

        new Thread(() -> {
            boolean isSuccess        = false;
            boolean isDataProblem    = false;
            boolean isNetWorkProblem = false;

            // 生成 json 文件
            if(createJson(dateDirF.getAbsolutePath())) {
                runOnUiThread(() -> {
                    reportProgressNote.setText("已完成30%");
                    reportProgressBar.setProgress(30);
                });

                // 打包 zip 文件
                if(zipPack(dateDirF.getAbsolutePath())) {
                    runOnUiThread(() -> {
                        reportProgressNote.setText("已完成60%");
                        reportProgressBar.setProgress(60);
                    });

                    // 发送 zip 文件
                    if(sendZip(dateDirF.getAbsolutePath())) {
                        isSuccess = true;
                        // 发送成功 跳转到 登录页
                        goLogin(true);
                    } else { isNetWorkProblem = true; }
                } else { isDataProblem = true; }
            } else { isDataProblem = true; }

            if(!isSuccess) {
                String note = "";
                String mesg = "";

                if(isDataProblem) {
                    note = "数据错误，上报失败";
                    mesg = note + "，是否重新打包上报？";
                }
                if(isNetWorkProblem) {
                    note = "蓝牙连接异常(请检查客户端、服务端[服务端程序可能需要重启]蓝牙连接是否正常)，上报失败";
                    mesg = note + "，是否重新上报？";
                }

                final String noteStr = note;
                runOnUiThread(() -> reportProgressNote.setText(noteStr));

                final String mesgStr   = mesg;
                final boolean dataBool = isDataProblem;
                final boolean netBool  = isNetWorkProblem;

                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    runOnUiThread(() -> reportProgressBlock.setVisibility(View.GONE));

                    AlertDialog dialog = new AlertDialog.Builder(ReportActivity.this).setTitle("上报失败").create();;
                    dialog.setMessage(mesgStr);
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, w) -> {
                        if(dataBool) { reportData(); }
                        if(netBool) {
                            // TODO.判断 本地蓝牙异常断开 还是 服务端蓝牙异常断开
                            // todo sth.

                            // 断开连接，重新 搜索 并 连接
                            bluetoothConnectThread.disconnect();

                            // 蓝牙是否开启
                            if(!bluetoothAdapter.isEnabled()) {
                                loadingAction(10, "蓝牙未开启，正在开启蓝牙...");

                                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                                startActivityForResult(intent, REQUEST_ENABLE_BT);
                            } else {
                                loadingAction(10, "蓝牙已开启，正在初始化...");
                                SetBlueToothShowForever();
                                BTInitial();
                            }
                        }
                    });
                    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, w) -> {
                        dialog.dismiss();
                        goLogin(false);
                    });
                    dialog.show();
                }, 500);
            }
        }).start();
    }

    // 重新发送 zip 文件
    @SuppressLint("SetTextI18n")
    private void reSendZip(String path) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": reSendZip()");

        RESEND_TIME ++;

        runOnUiThread(() -> {
            reportProgressNote.setText("正在重新上报(第" + String.valueOf(RESEND_TIME) +"次)，已完成60%");
            reportProgressBar.setProgress(60);
            reportProgressBlock.setVisibility(View.VISIBLE);
        });

        if(sendZip(path)) { goLogin(true); }
        else {
            runOnUiThread(() -> reportProgressNote.setText("上报失败"));
            new Handler(Looper.getMainLooper()).postDelayed(() -> runOnUiThread(() -> reportProgressBlock.setVisibility(View.GONE)), 500);

            AlertDialog dialog = new AlertDialog.Builder(ReportActivity.this).setTitle("上报失败").create();;
            dialog.setMessage("上报失败，是否重新上报？");
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, w) -> { reSendZip(path); });
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, w) -> {
                dialog.dismiss();
                goLogin(false);
            });
            dialog.show();
        }
    }
    // 发送成功 跳转到 登录页
    @SuppressLint("SetTextI18n")
    private void goLogin(boolean isSuccess) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": goLogin()");

        // 关闭蓝牙连接
        bluetoothConnectThread.disconnect();
        // 更新 is_reported
        if(isSuccess) { updateReports(); }

        if(isSuccess) {
            runOnUiThread(() -> {
                reportProgressNote.setText("已完成100%，上报成功");
                reportProgressBar.setProgress(100);
            });

            runOnUiThread(() -> new Handler(Looper.getMainLooper()).postDelayed(() -> {
                reportProgressBlock.setVisibility(View.GONE);
            }, 1000));
        }

        runOnUiThread(() -> {
            loadingAction(100, isSuccess ? "上报数据成功，即将跳转到登录页..." : "正在跳转到登录页...");
        });

        // 清空 未上报 信息
        myApplication.DATALST.clear();
        myApplication.UNREPORT_ID_LST.clear();
        myApplication.UNREPORT_DATA_ID_LST.clear();

        // 重置 report_id(initialed)
        myApplication.REPORT_ID                   = -1;
        myApplication.REPORT_ID_INITIALED         = false;
        myApplication.UNREPORT_NEW_DATA_INITIALED = false;

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            // 跳转到 登录页
            Intent intent = new Intent(ReportActivity.this, LoginActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

            loadingLayout.setVisibility(View.GONE);
            db.close();
            ReportActivity.this.finish();
        }, 3000);
    }

    // 发送 zip 文件
    private boolean sendZip(String dateDirPath) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": sendZip()");

        try {
            /* 一次性写入baos
            File zip               = new File(dateDirPath + File.separator + "data.zip");
            FileInputStream zipFis = new FileInputStream(zip);

            OutputStream os            = bluetoothConnectThread.socket.getOutputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] bs = new byte[1024];
            int len   = -1;

            while((len = zipFis.read(bs, 0, bs.length)) != -1) {
                if(len == 0 || len < 0) {
                    runOnUiThread(() -> Toast.makeText(ReportActivity.this, "Null Data", Toast.LENGTH_SHORT).show());
                }
                baos.write(bs, 0, len);
                // TODO.至关重要，每写一次都要flush一次，否则会出现数据丢失问题，原因暂时不明
                baos.flush();
            }
            zipFis.close();

            os.write(baos.toByteArray(), 0, baos.toByteArray().length);

            baos.flush();
            baos.close();

            os.flush();
            os.close();
            */

            // 一份一份的发送 bytes(1024)
            File zip               = new File(dateDirPath + File.separator + "data.zip");
            FileInputStream zipFis = new FileInputStream(zip);

            byte[] bs       = new byte[1024];
            int len         = 0;
            OutputStream os = bluetoothConnectThread.socket.getOutputStream();

            while((len = zipFis.read(bs, 0, bs.length)) != -1) {
                if(len == 0 || len < 0) {
                    runOnUiThread(() -> Toast.makeText(ReportActivity.this, "Null Data", Toast.LENGTH_SHORT).show());
                }
                os.write(bs, 0, len);
                os.flush();
            }
            zipFis.close();

            os.flush();
            os.close();

            return true;
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

            ToastUtils.show("发送zip文件失败(Exception:" + e.getMessage() + ")");
        }

        return false;
    }

    // 打包 zip 文件
    private boolean zipPack(String dateDirPath) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": zipPack()");

        try {
            // 打包Zip文件并保存
            File zip              = new File(dateDirPath + File.separator + "data.zip");
            ZipOutputStream zipOS = new ZipOutputStream(new FileOutputStream(zip));

            List<File> fLst = new ArrayList<File>();

            // json.txt文件
            File txtF       = new File(dateDirPath + File.separator + "data.txt");
            fLst.add(txtF);

            // 文件
            for(int j = 0; j < myApplication.DATALST.size(); ++j) {
                // 图片
                if(!myApplication.DATALST.get(j).getPhoto().equals("")) {
                    File photoF = new File(myApplication.DATALST.get(j).getPhoto());
                    fLst.add(photoF);
                }
                // 视频
                if(!myApplication.DATALST.get(j).getVideo().equals("")) {
                    File videoF = new File(myApplication.DATALST.get(j).getVideo());
                    fLst.add(videoF);
                }
            }

            // 压缩
            for(int i = 0; i < fLst.size(); ++i) {
                FileInputStream fis = new FileInputStream(fLst.get(i));
                ZipEntry entry      = new ZipEntry(fLst.get(i).getName());

                zipOS.putNextEntry(entry);

                byte[] buffer = new byte[1024];
                int byte_read = -1;

                while((byte_read = fis.read(buffer)) != -1) { zipOS.write(buffer, 0, byte_read); }

                zipOS.closeEntry();
                fis.close();
            }

            zipOS.flush();
            zipOS.close();

            return true;
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

            ToastUtils.show("打包zip文件失败(Exception:" + e.getMessage() + ")");
        }

        return false;
    }
    // 生成 json 文件
    private boolean createJson(String dateDirPath) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": createJson()");

        // 生成json.txt文件并保存
        JSONArray array    = new JSONArray();
        JSONObject rootObj = new JSONObject();

        try {
            // 数据填报类型 标识(1-安全填报，2-点检填报)
            rootObj.put("type", 2);

            for(int i = 0; i < myApplication.DATALST.size(); ++i) {
                JSONObject obj  = new JSONObject();

                StringBuffer deviceIdStr   = new StringBuffer("");
                StringBuffer deviceNameStr = new StringBuffer("");
                StringBuffer pointIdStr    = new StringBuffer("");

                Cursor c = db.rawQuery("SELECT id FROM areas WHERE name = '" + myApplication.DATALST.get(i).getArea() + "';", null);

                int areaId = -1;
                if(c.getCount() != 0 && c.moveToNext()) {
                    areaId = c.getInt(c.getColumnIndex("id"));

                    if(areaId != -1) {
                        // 获取 device_id
                        getDeviceId(areaId, myApplication.DATALST.get(i).getDeviceType(),
                                myApplication.DATALST.get(i).getPositionName(),
                                deviceIdStr, deviceNameStr, pointIdStr);

                        obj.put("area", myApplication.DATALST.get(i).getArea());                 // area

                        // 使用real_id
                        Cursor c2 = db.rawQuery("SELECT real_id FROM device_type WHERE area_id = " + String.valueOf(areaId) + " AND name = '" + myApplication.DATALST.get(i).getDeviceType() + "';", null);
                        if(c2.getCount() != 0 && c2.moveToNext()) {
                            obj.put("deviceId", c2.getString(c2.getColumnIndex("real_id")));
                        } else { ToastUtils.show("获取设备类型real_id错误");   }
                        c2.close();

                        if(!deviceNameStr.toString().equals("")) {
                            obj.put("deviceName", deviceNameStr.toString());                     // device_name
                        }

                        Cursor c3 = db.rawQuery("SELECT real_id FROM points WHERE id = " + pointIdStr + ";", null);
                        if(c3.getCount() != 0 && c3.moveToNext()) {
                            obj.put("pointId", c3.getString(c3.getColumnIndex("real_id")));
                        } else { ToastUtils.show("获取点位real_id错误"); }
                        c3.close();

                        obj.put("pointName", myApplication.DATALST.get(i).getPositionName());    // point_name
                        obj.put("reconType", myApplication.DATALST.get(i).getRecognitionType()); // recon_type
                        obj.put("value", myApplication.DATALST.get(i).getResult());              // value
                        obj.put("unit", myApplication.DATALST.get(i).getUnit());                 // unit
                        // 更换时间 格式
                        String timeStr = myApplication.DATALST.get(i).getRecognitionTime();
                        String regEx   = "[^0-9]";
                        Pattern p      = Pattern.compile(regEx);
                        Matcher m      = p.matcher(timeStr);

                        String t = m.replaceAll("").trim();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat t_old_sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat t_new_sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        Date d = t_old_sdf.parse(t);
                        obj.put("reconTime", t_new_sdf.format(d));                               // recon_time
                        obj.put("alarmStatus", myApplication.DATALST.get(i).getHaveAlarm());     // alarm_status
                        obj.put("alarmType", myApplication.DATALST.get(i).getAlarmType());       // alarm_type
                        obj.put("alarmDesc", myApplication.DATALST.get(i).getAlarmDesc());       // alarm_desc
                        /// 采集信息
                        // 照片
                        File pf = new File(myApplication.DATALST.get(i).getPhoto());
                        obj.put("filePath", pf.getName());                                       // file_path
                        // 视频
                        if(!myApplication.DATALST.get(i).getVideo().equals("")) {
                            File vf = new File(myApplication.DATALST.get(i).getVideo());
                            obj.put("videoPath", vf.getName());                                  // video_path
                        }

                        array.put(obj);
                    } else { ToastUtils.show("获取area_id(-1)错误"); }
                } else { ToastUtils.show("获取area_id错误"); }

                c.close();
            }

            rootObj.put("data", array);

            String data = rootObj.toString();
            System.out.println(data);

            String jsonTxt = dateDirPath + File.separator + "data.txt";

            FileOutputStream fos = new FileOutputStream(jsonTxt);
            fos.write(data.getBytes());

            fos.flush();
            fos.close();

            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();

            ToastUtils.show("生成json文件失败(Exception:" + e.getMessage() + ")");
        }

        return false;
    }
    // 更新 report 时间
    private void updateReportTime(String t) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": updateReportTime()");

        ContentValues cv = new ContentValues();
        cv.put("time", t);

        // 更新 reports表 时间
        // 未上报
        if(myApplication.UNREPORT_ID_LST.size() > 0) {
            for(int i = 0; i < myApplication.UNREPORT_ID_LST.size(); ++i) {
                int updateResult = db.update("reports", cv, "id=?", new String[]{ String.valueOf(myApplication.UNREPORT_ID_LST.get(i)) });

                if(updateResult != -1) { System.out.println("reports(id=" + String.valueOf(i + 1) + ")更新时间成功"); }
                else { ToastUtils.show("reports(id=" + String.valueOf(i + 1) + ")更新时间失败(" + String.valueOf(updateResult) + ")"); }
            }
        }
        // 正常上报
        else {
            int updateResult = db.update("reports", cv, "id=?", new String[]{ String.valueOf(myApplication.REPORT_ID) });

            if(updateResult != -1) { System.out.println("reports更新时间成功"); }
            else { ToastUtils.show("reports更新时间失败(" + String.valueOf(updateResult) + ")"); }
        }
    }
    // 更新 is_reported
    private void updateReports() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": updateReports()");

        ContentValues cv = new ContentValues();
        cv.put("is_reported", 1);

        if(myApplication.UNREPORT_ID_LST.size() > 0) {
            for(int i = 0; i < myApplication.UNREPORT_ID_LST.size(); ++i) {
                int updateResult = db.update("reports", cv, "id=?", new String[]{ String.valueOf(myApplication.UNREPORT_ID_LST.get(i)) });

                if(updateResult != -1) {
                    System.out.println("reports(id=" + String.valueOf(i + 1) + ")更新上报标识成功");
                } else { ToastUtils.show("reports(id=" + String.valueOf(i + 1) + ")更新上报标识失败(" + String.valueOf(updateResult) + ")"); }
            }
        } else {
            int updateResult = db.update("reports", cv, "id=?", new String[]{ String.valueOf(myApplication.REPORT_ID) });

            if(updateResult != -1) { System.out.println("reports更新上报标识成功"); }
            else { ToastUtils.show("reports更新上报标识失败(" + String.valueOf(updateResult) + ")"); }
        }
    }
    // 获取 device_id 和其他相关项
    private void getDeviceId(int areaId, String deviceTypeStr, String positionNameStr,
                             StringBuffer deviceIdStr, StringBuffer deviceNameStr, StringBuffer pointIdStr) {

        Cursor cr = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(areaId) + " AND name = '" + deviceTypeStr + "';", null);

        if(cr.getCount() != 0 && cr.moveToNext()) {
            int ctd = -1;
            ctd     = cr.getInt(cr.getColumnIndex("id"));
            cr.close();

            // 根据 device_type_id 和 point_name 获取 point_id、device_name
            if(ctd != -1) {
                Cursor cr2 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(ctd) + " AND name = '" + positionNameStr + "';", null);

                if(cr2.getCount() != 0 && cr2.moveToNext()) {
                    int cpd = -1;

                    cpd           = cr2.getInt(cr2.getColumnIndex("id"));
                    deviceNameStr = deviceNameStr.append(cr2.getString(cr2.getColumnIndex("device_name")));

                    cr2.close();

                    if(cpd != -1) {
                        deviceIdStr = deviceIdStr.append(String.valueOf(cpd)).append(String.valueOf(ctd));
                        pointIdStr  = pointIdStr.append(String.valueOf(cpd));
                    } else { ToastUtils.show("获取point_id错误"); }
                } else { ToastUtils.show("获取点位错误"); }
            } else { ToastUtils.show("获取设备类型id错误"); }
        } else { ToastUtils.show("获取设备类型错误"); }
    }

    // LoadingLayout显示与隐藏
    private void loadingAction(int step, String note) {
        loadingLayout.setProgressStep(step);
        loadingLayout.setProgressNote(note);
        loadingLayout.setVisibility(View.VISIBLE);
    }

    /***********************************Activity Override Start***********************************/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onActivityResult(" + requestCode + "," + " " + requestCode + "," + " " + data + ")");

        // 蓝牙开启请求
        if(requestCode == REQUEST_ENABLE_BT) {
            if(resultCode == RESULT_OK) {
                loadingAction(20, "蓝牙已开启，正在初始化，请稍候...");

                SetBlueToothShowForever(); // JAVA反射机制，设置 蓝牙永久可见
                BTInitial();               // 蓝牙初始化
            } else if(resultCode == RESULT_CANCELED) {
                loadingAction(20, "未知错误，蓝牙开启失败");
                new Handler().postDelayed(() -> loadingLayout.setVisibility(View.GONE), 500);
            }
        }
        // 蓝牙永久可见请求
        if(requestCode == REQUEST_ENABLE_ACTIVE_FOREVER) {
            if(resultCode == RESULT_OK) { System.out.println("设置蓝牙永久可见性成功"); }
            else { ToastUtils.show("设置蓝牙永久可见性失败"); }
        }
    }
    @Override
    protected void onStart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStart()");
        super.onStart();
    }
    @Override
    protected void onResume() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onResume()");
        super.onResume();
    }
    @Override
    protected void onPause() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onPause()");
        super.onPause();

        MediaPlayerManager.instance().pause();
    }
    @Override
    protected void onStop() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStop()");
        super.onStop();
    }
    @Override
    protected void onRestart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onRestart()");
        super.onRestart();
    }
    @Override
    protected void onDestroy() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDestroy()");
        super.onDestroy();

        // 释放MediaPlayer
        MediaPlayerManager.instance().releasePlayerAndView(this);

        // 注销 蓝牙搜索广播
        if(btfReceiver != null) {
            if(isBroadcastRegistered) {
                unregisterReceiver(btfReceiver);
                isBroadcastRegistered = false;
            }
        }
        // 关闭数据库
        db.close();
    }
    /***********************************Activity Override End***********************************/

    // 返回键 退出程序
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(isVideoPreviewShow) {
                MediaPlayerManager.instance().releasePlayerAndView(this);

                runOnUiThread(() -> {
                    videoFileName.setText("");
                    videoPreviewLayout.setVisibility(View.GONE);
                });

                isVideoPreviewShow = false;
            }
        }

        return false;
    }
}
