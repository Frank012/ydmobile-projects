package com.demo.devicesreport;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.LayoutDirection;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;

import com.bigkoo.pickerview.TimePickerView;
import com.hjq.toast.ToastUtils;

import org.salient.artplayer.MediaPlayerManager;
import org.salient.artplayer.VideoView;
import org.salient.artplayer.ui.ControlPanel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import photoview.PhotoView;
import utils.MyApplication;
import utils.SqliteUtil;

public class MainActivity extends Activity {
    private float density = 0f;

    private static String FLAG     = null; // Intent标识
    private static int DATA_INDEX  = -1;   // 数据data index
    private static int SELECT_TYPE = -1;   // 选择value标识

    private static int CUR_AREA_ID        = -1;
    private static int CUR_DEVICE_TYPE_ID = -1;
    private static int CUR_POINT_ID       = -1;

    // Utils
    private SqliteUtil sqliteUtil = null;

    // 请求码
    private static final int CAMERA_CAPTURE = 5;
    private static final int CAMERA_VIDEO   = 6;

    // Variables
    private MyApplication myApplication = null;
    private static final String Log_Tag = "MainActivity";

    private boolean isDBOpened = false;
    private SQLiteDatabase db  = null;

    private String cachePhotoTime = null;
    private String cacheVideoTime = null;
    private boolean isMenuClick   = false;

    private boolean isHaveAlarm = false;

    // 数据
    public List<String> areasLst           = new ArrayList<String>();
    public List<String> deviceTypeLst      = new ArrayList<String>();
    public List<String> positionNameLst    = new ArrayList<String>();
    public List<String> recognitionTypeLst = new ArrayList<String>();
    public List<String> unitLst            = new ArrayList<String>();
    public List<String> alarmTypeLst       = new ArrayList<String>();

    public String photoName = "";
    public String videoPath = "";

    // 拍照、视频
    private Uri captureImageUri = null;
    private Uri captureVideoUri = null;

    private Bitmap photoBitmap = null;

    private boolean isCaptured = false;
    private boolean isVideoed  = false;

    // UI
    private RelativeLayout rootLayout = null;

    private TextView areaValue            = null;
    private TextView deviceTypeValue      = null;
    private TextView positionNameValue    = null;
    private TextView recognitionTypeValue = null;
    private EditText resultValue          = null;
    private TextView unitValue            = null;
    private TextView recognitionTimeValue = null;
    private TextView alarmTypeValue       = null;
    private EditText alarmDescValue       = null;

    private RelativeLayout selectItemLayout = null;

    public int selectIndex                  = -1;
    private ListView selectItemList         = null;

    private TextView selectTitle = null;

    // 异常 Switch
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private Switch alarmSelectSwitch      = null;
    private RelativeLayout alarmInfoBlock = null;

    // 对话框
    private RelativeLayout dialogBlock = null;
    private TextView dialogTitle       = null;
    private TextView dialogMessage     = null;
    private TextView dialogOKBtn       = null;
    private TextView dialogCancelBtn   = null;

    // Capture & Video
    private RelativeLayout captureLayout = null;
    private RelativeLayout videoLayout   = null;

    private ImageView captureImageView = null;
    private ImageView videoImageView   = null;

    private TextView captureNote = null;
    private TextView videoNote   = null;

    private ImageView photoDeleteIcon = null;
    private ImageView videoDeleteIcon = null;

    // 预览
    private RelativeLayout imagePreviewLayout = null;
    private RelativeLayout videoPreviewLayout = null;

    private ImageView videoPreviewBack = null;
    private TextView videoFileName     = null;

    private boolean isVideoPreviewShow = false;

    private PhotoView photoPreview = null;
    private VideoView videoPreview = null;

    // Save Layout
    private Button nextBtn         = null;
    private Button saveOrCancelBtn = null;

    public LoadingLayout loadingLayout = null;

    // 区域选择
    private final List<RadioButton> radioBtnLst = new ArrayList<>();
    // 不使用 ViewHolder
    private final List<View> convertViewLst = new ArrayList<View>();

    private RelativeLayout areaSelectItemLayout = null;
    private LinearLayout areaSelectList         = null;

    private RelativeLayout alarmLevelSelectItemLayout = null;

    // Select List Adapter
    private SelectListAdapter selectListAdapter = null;

    public class SelectListAdapter extends BaseAdapter {
        private LayoutInflater inflater = null;
        private List<String> valuesLst  = null;

        // Constructor
        public SelectListAdapter(Context context, List<String> list) {
            this.inflater  = LayoutInflater.from(context);
            this.valuesLst = list;
        }

        @Override
        public int getCount() { return valuesLst.size(); }
        @Override
        public Object getItem(int position) { return position; }
        @Override
        public long getItemId(int id) { return id; }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(position + 1 > convertViewLst.size()) {
                convertView = inflater.inflate(R.layout.select_list_item, null);
            } else { convertView = convertViewLst.get(position); }

            if(position % 2 == 0) { convertView.setBackgroundColor(Color.WHITE); }

            AbsListView.LayoutParams param = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);
            convertView.setLayoutParams(param);

            RadioButton radioBtn = convertView.findViewById(R.id.radio_btn);
            radioBtn.setClickable(false);

            if(selectIndex == position) { radioBtn.setChecked(true); }
            else { radioBtn.setChecked(false); }

            TextView valueTextView = convertView.findViewById(R.id.value_text_view);
            valueTextView.setText(valuesLst.get(position));

            return convertView;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApplication = new MyApplication();
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onCreate()");
        setContentView(R.layout.activity_main);

        // 获取屏幕 密度
        density = getResources().getDisplayMetrics().density;

        sqliteUtil = new SqliteUtil();

        // 数据库初始化
        db = SQLiteDatabase.openDatabase(MyApplication.DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
        if(db != null) { isDBOpened = true; }
        else { ToastUtils.show("数据库打开失败"); }

        // 获取数据
        DataInit();

        // 页面操作标识
        FLAG = getIntent().getStringExtra("FLAG");
        System.out.println("FLAG: " + FLAG);

        // 数据操作标识
        DATA_INDEX = getIntent().getIntExtra("DATA_INDEX", -1);
        System.out.println("DATA_INDEX: " + DATA_INDEX);

        // 全屏
        MainActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View view = this.getWindow().getDecorView();
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        // UI初始化
        UIInitial();

        // 修改 操作，根据 data_index 数据 初始化
        if(DATA_INDEX != -1) {
            areaValue.setText(myApplication.DATALST.get(DATA_INDEX).getArea());
            deviceTypeValue.setText(myApplication.DATALST.get(DATA_INDEX).getDeviceType());
            positionNameValue.setText(myApplication.DATALST.get(DATA_INDEX).getPositionName());
            recognitionTypeValue.setText(myApplication.DATALST.get(DATA_INDEX).getRecognitionType());
            resultValue.setText(myApplication.DATALST.get(DATA_INDEX).getResult());
            unitValue.setText(myApplication.DATALST.get(DATA_INDEX).getUnit());
            recognitionTimeValue.setText(myApplication.DATALST.get(DATA_INDEX).getRecognitionTime());

            if(myApplication.DATALST.get(DATA_INDEX).getHaveAlarm() == 0) { alarmSelectSwitch.setChecked(false); }
            else {
                alarmSelectSwitch.setChecked(true);

                alarmInfoBlock.setVisibility(View.VISIBLE);
                ValueAnimator ani = createExpandAni(alarmInfoBlock, 0, (int)(density * 281 + 0.5));
                ani.start();

                alarmTypeValue.setText(myApplication.DATALST.get(DATA_INDEX).getAlarmType());
                alarmDescValue.setText(myApplication.DATALST.get(DATA_INDEX).getAlarmDesc());

                // 照片、视频
                if(!myApplication.DATALST.get(DATA_INDEX).getPhoto().equals("")) {
                    captureImageView.setImageBitmap(BitmapFactory.decodeFile(myApplication.DATALST.get(DATA_INDEX).getPhoto()));
                    photoDeleteIcon.setVisibility(View.VISIBLE);
                    isCaptured = true;
                }
                if(!myApplication.DATALST.get(DATA_INDEX).getVideo().equals("")) {
                    videoImageView.setImageBitmap(getVideoThumbnailBmp());
                    videoDeleteIcon.setVisibility(View.VISIBLE);
                    isVideoed = true;
                }
            }
        }
    }

    // Data Init
    private void DataInit() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": DataInit()");

        if(isDBOpened) {
            // 获取区域Areas
            Cursor cr = db.rawQuery("SELECT name FROM areas;", null);
            if(cr.getCount() != 0) {
                while(cr.moveToNext()) { areasLst.add(cr.getString(cr.getColumnIndex("name"))); }
                cr.close();
            } else { ToastUtils.show("获取区域失败"); }

            // 获取 默认(区域一)区域下的 设备类型
            Cursor cr2 = db.rawQuery("SELECT name FROM device_type WHERE area_id = 1;", null);
            if(cr2.getCount() != 0) {
                while(cr2.moveToNext()) { deviceTypeLst.add(cr2.getString(cr2.getColumnIndex("name"))); }
                cr2.close();
            } else { ToastUtils.show("获取设备类型失败"); }

            // 获取 默认(区域一)区域下的 点位
            Cursor cr3 = db.rawQuery("SELECT name FROM points WHERE device_type_id = 1;", null);
            if(cr3.getCount() != 0) {
                while(cr3.moveToNext()) { positionNameLst.add(cr3.getString(cr3.getColumnIndex("name"))); }
                cr3.close();
            } else { ToastUtils.show("获取点位失败"); }

            // 获取识别类型
            Cursor cr4 = db.rawQuery("SELECT name FROM recognition_type;", null);
            if(cr4.getCount() != 0) {
                while(cr4.moveToNext()) { recognitionTypeLst.add(cr4.getString(cr3.getColumnIndex("name"))); }
                cr4.close();
            } else { ToastUtils.show("获取识别类型失败");   }

            // 获取单位
            Cursor cr5 = db.rawQuery("SELECT name FROM unit;", null);
            if(cr5.getCount() != 0) {
                while(cr5.moveToNext()) { unitLst.add(cr5.getString(cr5.getColumnIndex("name"))); }
                cr5.close();
            } else { ToastUtils.show("获取单位失败");   }

            // 获取 告警类型
            Cursor cr6 = db.rawQuery("SELECT name FROM alarm_type;", null);
            if(cr6.getCount() != 0) {
                while(cr6.moveToNext()) { alarmTypeLst.add(cr6.getString(cr6.getColumnIndex("name"))); }
                cr6.close();
            } else { ToastUtils.show("获取告警类型失败");   }
        }
    }

    // 设备异常 警告填报 展开动画效果
    private ValueAnimator createExpandAni(final View v, int start, int end) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": createExpandAni()");

        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(valueAnimator -> {
            int value = (int)valueAnimator.getAnimatedValue();

            ViewGroup.LayoutParams params = v.getLayoutParams();
            params.height = value;
            v.setLayoutParams(params);
        });

        return animator;
    }

    // UI Initial
    @SuppressLint({"UseCompatLoadingForDrawables", "ClickableViewAccessibility"})
    private void UIInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": UIInitial()");

        rootLayout = findViewById(R.id.root);

        // Title Layout
        ImageView menu = findViewById(R.id.menu);
        if(FLAG.equals("1")) { menu.setVisibility(View.VISIBLE); }
        else { menu.setVisibility(View.GONE); }

        menu.setOnClickListener(v -> {
            runOnUiThread(() -> {
                dialogTitle.setText("保存");
                dialogMessage.setText("需要保存已添加信息吗？");
                dialogBlock.setVisibility(View.VISIBLE);
            });

            isMenuClick = true;
        });

        // Operation Note
        TextView operationNote = findViewById(R.id.operation_note);
        if(FLAG.equals("2")) { operationNote.setText("修改"); }

        // Select Layout
        areaValue            = findViewById(R.id.area_value);
        deviceTypeValue      = findViewById(R.id.device_type_value);
        positionNameValue    = findViewById(R.id.position_name_value);
        recognitionTypeValue = findViewById(R.id.recognition_type_value);
        resultValue          = findViewById(R.id.result_value);
        unitValue            = findViewById(R.id.unit_value);
        recognitionTimeValue = findViewById(R.id.recognition_time_value);
        alarmTypeValue       = findViewById(R.id.alarm_type_value);
        alarmDescValue       = findViewById(R.id.alarm_desc_value);

        // 从上报页 跳转到 添加 时，清空所有选项
        if(FLAG.equals("1")) {
            areaValue.setText("");
            deviceTypeValue.setText("");
            positionNameValue.setText("");
            recognitionTypeValue.setText("");
            unitValue.setText("℃");
        }

        // 区域选择
        areaSelectItemLayout    = findViewById(R.id.area_select_item_layout);
        RelativeLayout areaSelectItemContainer = findViewById(R.id.area_select_item_container);
        areaSelectList          = findViewById(R.id.area_select_list);

        areaSelectItemLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            areaSelectItemLayout.setVisibility(View.GONE);
            return true;
        });
        areaSelectItemContainer.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // 区域选择初始化
        areaSelectListInitial();

        // 违章行为类别选择
        alarmLevelSelectItemLayout    = findViewById(R.id.alarm_level_select_item_layout);
        RelativeLayout alarmLevelSelectItemContainer = findViewById(R.id.alarm_level_select_item_container);

        alarmLevelSelectItemLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            alarmLevelSelectItemLayout.setVisibility(View.GONE);
            return true;
        });
        alarmLevelSelectItemContainer.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // Select Item Layout
        selectTitle = findViewById(R.id.select_title);

        selectItemLayout = findViewById(R.id.select_item_layout);
        selectItemLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            selectItemLayout.setVisibility(View.GONE);
            return true;
        });

        selectItemList   = findViewById(R.id.select_item_list);
        selectItemList.setOnItemClickListener((adapterView, view, position, id) -> {
            selectIndex = position;
            selectItemLayout.setVisibility(View.GONE);

            // 设备类型、点位名称、识别类型、单位、告警类型 Select
            switch(SELECT_TYPE) {
                case 1:
                    deviceTypeValue.setText(selectListAdapter.valuesLst.get(position));
                    // 更新 点位 并过滤
                    updatePositionNameLst(CUR_AREA_ID);
                    break;

                case 2: positionNameValue.setText(selectListAdapter.valuesLst.get(position)); break;
                case 3: recognitionTypeValue.setText(selectListAdapter.valuesLst.get(position)); break;
                case 4: unitValue.setText(selectListAdapter.valuesLst.get(position)); break;
                case 5: alarmTypeValue.setText(selectListAdapter.valuesLst.get(position)); break;

                default: break;
            }
        });

        RelativeLayout selectItemContainer = findViewById(R.id.select_item_container);
        selectItemContainer.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // Alarm Switch
        alarmSelectSwitch = findViewById(R.id.alarm_select_switch);
        alarmSelectSwitch.setOnCheckedChangeListener((btn, checked) -> {
            isHaveAlarm = checked;

            if(checked) {
                alarmInfoBlock.setVisibility(View.VISIBLE);
                ValueAnimator ani = createExpandAni(alarmInfoBlock, 0, (int)(density * 281 + 0.5));
                ani.start();
            } else {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm.isActive()) {
                    // 强制隐藏
                    View focv = MainActivity.this.getCurrentFocus();
                    if(focv != null) { imm.hideSoftInputFromWindow(focv.getWindowToken(), 0); }
                }

                ValueAnimator ani = createExpandAni(alarmInfoBlock, (int)(density * 281 + 0.5), 0);
                ani.start();
                alarmInfoBlock.setVisibility(View.GONE);
            }
        });
        alarmInfoBlock = findViewById(R.id.alarm_info_block);

        // Capture & Video
        captureImageView = findViewById(R.id.capture_icon);
        videoImageView   = findViewById(R.id.video_icon);

        captureNote = findViewById(R.id.capture_note);
        videoNote   = findViewById(R.id.video_note);

        photoDeleteIcon = findViewById(R.id.photo_delete_icon);
        videoDeleteIcon = findViewById(R.id.video_delete_icon);

        // 拍照
        captureLayout = findViewById(R.id.capture_layout);
        captureLayout.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        captureNote.setTextColor(Color.parseColor("#1e90ff"));
                        captureLayout.setBackgroundColor(Color.parseColor("#dcdcdc"));
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        captureNote.setTextColor(Color.BLACK);
                        captureLayout.setBackgroundColor(Color.TRANSPARENT);
                    });

                    // 未拍照，则 前往拍照页面
                    if(!isCaptured) {
                        ToastUtils.show("拍照");

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                        Date date            = new Date(System.currentTimeMillis());

                        cachePhotoTime    = sdf.format(date);
                        File captureImage = new File(getExternalCacheDir(), cachePhotoTime + ".jpg");

                        if(!captureImage.exists()) {
                            try { if(captureImage.createNewFile()) { System.out.println("创建图片Uri成功"); } }
                            catch(IOException ex) {
                                System.out.println(ex.getMessage());
                                ex.printStackTrace();
                                ToastUtils.show("创建图片Uri失败:" + ex.getMessage());
                            }
                        }

                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            captureImageUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.concat(".provider"), captureImage);
                        } else {
                            captureImageUri = Uri.fromFile(captureImage);
                        }

                        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureImageUri);

                        startActivityForResult(captureIntent, CAMERA_CAPTURE);
                    }
                    // 已拍照，则 查看预览图
                    else {
                        if(DATA_INDEX != -1) {
                            photoPreview.setImageDrawable(new BitmapDrawable(BitmapFactory.decodeFile(myApplication.DATALST.get(DATA_INDEX).getPhoto())));
                        } else { photoPreview.setImageDrawable(new BitmapDrawable(photoBitmap)); }

                        imagePreviewLayout.setVisibility(View.VISIBLE);
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;

                default: break;
            }

            return true;
        });
        // 拍摄视频
        videoLayout = findViewById(R.id.video_layout);
        videoLayout.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> {
                        videoNote.setTextColor(Color.parseColor("#1e90ff"));
                        videoLayout.setBackgroundColor(Color.parseColor("#dcdcdc"));
                    });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> {
                        videoNote.setTextColor(Color.BLACK);
                        videoLayout.setBackgroundColor(Color.TRANSPARENT);
                    });

                    if(!isVideoed) {
                        ToastUtils.show("拍摄");

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                        Date date            = new Date(System.currentTimeMillis());

                        cacheVideoTime    = sdf.format(date);
                        File captureVideo = new File(getExternalCacheDir(), cacheVideoTime + ".mp4");

                        if(!captureVideo.exists()) {
                            try { if(captureVideo.createNewFile()) { System.out.println("创建视频Uri成功"); } }
                            catch(IOException ex) {
                                System.out.println(ex.getMessage());
                                ex.printStackTrace();
                                ToastUtils.show("创建视频Uri失败:" + ex.getMessage());
                            }
                        }

                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            captureVideoUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.concat(".provider"), captureVideo);
                        } else {
                            captureVideoUri = Uri.fromFile(captureVideo);
                        }

                        Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        videoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10); // 限制录制时间10秒
                        videoIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureVideoUri);

                        startActivityForResult(videoIntent, CAMERA_VIDEO);
                    } else {
                        // ①调用系统播放器播放视频有两个问题：
                        //   1.必须使用getContentResolver()，播放uri路径下的视频，但是会报没有权限的错误；视频不在sdcard上
                        //   2.使用sdcard路径，会有exposed uri的问题(两者好像互相矛盾)
                        // ②使用videoview的问题，屏幕一直黑屏，暂时找不到原因
                        // ③使用MediaPlayer + SurfaceView 再次点击预览，surfaceview无法清空

                        // 使用 ArtPlayer插件
                        videoPreviewLayout.setVisibility(View.VISIBLE);

                        if(DATA_INDEX != -1) {
                            videoPreview.setUp(myApplication.DATALST.get(DATA_INDEX).getVideo());
                            runOnUiThread(() -> videoFileName.setText(new File(myApplication.DATALST.get(DATA_INDEX).getVideo()).getName()));
                        } else {
                            videoPreview.setUp(videoPath);
                            runOnUiThread(() -> videoFileName.setText(new File(videoPath).getName()));
                        }

                        videoPreview.setControlPanel(new ControlPanel(this));
                        videoPreview.start();

                        isVideoPreviewShow = true;
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;

                default: break;
            }

            return true;
        });

        /// 预览
        // 图片
        imagePreviewLayout = findViewById(R.id.image_preview_layout);
        imagePreviewLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            imagePreviewLayout.setVisibility(View.GONE);
            return true;
        });
        photoPreview = findViewById(R.id.photo_preview);

        // 视频
        videoPreviewLayout = findViewById(R.id.video_preview_layout);
        videoPreview       = findViewById(R.id.video_preview);

        videoPreviewBack   = findViewById(R.id.video_preview_back);
        videoFileName      = findViewById(R.id.video_file_name);

        videoPreviewBack.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> videoPreviewBack.setBackgroundColor(Color.parseColor("#dcdcdc")));
                    break;
                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> videoPreviewBack.setBackgroundColor(Color.TRANSPARENT));

                    MediaPlayerManager.instance().releasePlayerAndView(this);

                    runOnUiThread(() -> {
                        videoFileName.setText("");
                        videoPreviewLayout.setVisibility(View.GONE);
                    });

                    isVideoPreviewShow = false;

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 下一个(添加)-修改(修改)
        nextBtn = findViewById(R.id.next_btn);
        if(!FLAG.equals("2")) { nextBtn.setText("下一个"); }
        else { nextBtn.setText("修改"); }

        nextBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                        runOnUiThread(() -> { nextBtn.setBackgroundColor(Color.parseColor("#498bdd")); });
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> { nextBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_btn_style)); });

                    if(nextBtn.getText().toString().equals("下一个")) {
                        // 必填项判断
                        if(checkKeyInfoComplete()) {
                            loadingAction(50, "正在保存数据，请稍候...");

                            saveDataToDB();              // 数据库保存数据(datas表)
                            saveDataToReportDataClass(); // ReportData 保存数据

                            Map<String, Object> m = new HashMap<String, Object>();
                            m = checkNextPoint();

                            final String areaStr       = (String)m.get("area");
                            final String deviceTypeStr = (String)m.get("device_type");
                            final String pointNameStr  = (String)m.get("point_name");

                            runOnUiThread(() -> { loadingAction(100, "数据保存成功，开始填报下一项..."); });

                            // 默认 填好下一个点位 相关信息
                            new Handler().postDelayed(() -> {
                                if(areaStr != null && deviceTypeStr != null && pointNameStr != null) {
                                    areaValue.setText(areaStr);
                                    deviceTypeValue.setText(deviceTypeStr);
                                    positionNameValue.setText(pointNameStr);

                                    recognitionTypeValue.setText("");
                                    resultValue.setText("");
                                    unitValue.setText("℃");
                                    recognitionTimeValue.setText("");

                                    if(alarmSelectSwitch.isChecked()) {
                                        alarmSelectSwitch.setChecked(false);

                                        ValueAnimator ani = createExpandAni(alarmInfoBlock, (int)(density * 281 + 0.5), 0);
                                        ani.start();
                                        alarmInfoBlock.setVisibility(View.GONE);

                                        alarmTypeValue.setText("告警类型一");
                                        alarmDescValue.setText("");

                                        photoDeleteIcon.setVisibility(View.GONE);
                                        captureImageView.setImageBitmap(null);
                                        captureImageView.setBackgroundResource(R.drawable.capture_icon);
                                        isCaptured = false;

                                        videoDeleteIcon.setVisibility(View.GONE);
                                        videoImageView.setImageBitmap(null);
                                        videoImageView.setBackgroundResource(R.drawable.video_icon);
                                        isVideoed = false;
                                    }
                                } else { ToastUtils.show("获取下一个点位错误"); }

                                loadingLayout.setVisibility(View.GONE);
                            }, 500);
                        } else { Toast.makeText(MainActivity.this, "请填报完整信息", Toast.LENGTH_SHORT).show(); }
                    }
                    // 修改(保存)
                    else {
                        if(checkKeyInfoComplete()) {
                            loadingAction(50, "正在保存数据，请稍候...");

                            // 更新数据库数据
                            ContentValues cv = new ContentValues();
                            int updateResult = -1;

                            StringBuffer deviceIdStr          = new StringBuffer("");
                            StringBuffer deviceNameStr        = new StringBuffer("");
                            StringBuffer pointIdStr           = new StringBuffer("");
                            StringBuffer recognitionTypeIdStr = new StringBuffer("");

                            getDeviceId(deviceIdStr, deviceNameStr, pointIdStr, recognitionTypeIdStr);

                            cv.put("area", areasLst.indexOf(areaValue.getText().toString()) + 1);

                            if(!deviceIdStr.toString().equals("")) { cv.put("device_id", Integer.parseInt(deviceIdStr.toString())); }
                            if(!deviceNameStr.toString().equals("")) { cv.put("device_name", deviceNameStr.toString()); }
                            if(!pointIdStr.toString().equals("")) { cv.put("point_id", Integer.parseInt(pointIdStr.toString())); }

                            cv.put("point_name", positionNameValue.getText().toString());

                            if(!recognitionTypeIdStr.toString().equals("")) { cv.put("recognition_type_id", Integer.parseInt(recognitionTypeIdStr.toString())); }

                            cv.put("result", resultValue.getText().toString());
                            cv.put("unit_id", unitLst.indexOf(unitValue.getText().toString()) + 1);
                            cv.put("recognition_time", recognitionTimeValue.getText().toString());
                            cv.put("alarm_status", isHaveAlarm ? 1 : 0);

                            if(isHaveAlarm) {
                                cv.put("alarm_type_id", alarmTypeLst.indexOf(alarmTypeValue.getText().toString()) + 1);
                                cv.put("alarm_description", alarmDescValue.getText().toString());

                                if(!photoName.equals("")) { cv.put("image", photoName); }
                                if(!videoPath.equals("")) { cv.put("video", videoPath); }
                            } else {
                                cv.put("alarm_type_id", "");
                                cv.put("alarm_description", "");
                                cv.put("image", "");
                                cv.put("video", "");
                            }

                            // 未上报数据 DATA_INDEX 为实际未上报 数据 data_id
                            if(myApplication.UNREPORT_ID_LST.size() > 0) {
                                updateResult = db.update("datas", cv, "id=?", new String[]{ String.valueOf(myApplication.UNREPORT_DATA_ID_LST.get(DATA_INDEX)) });
                            } else {
                                updateResult = db.update("datas", cv, "id=?", new String[]{ String.valueOf(DATA_INDEX + 1) });
                            }

                            if(updateResult != -1) {
                                cv.clear();
                                cv = null;
                            } else { ToastUtils.show("datas数据更新失败(" + String.valueOf(updateResult) + ")"); }

                            // 更新 DATALST
                            myApplication.DATALST.get(DATA_INDEX).setArea(areaValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setDeviceType(deviceTypeValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setPositionName(positionNameValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setRecognitionType(recognitionTypeValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setResult(resultValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setUnit(unitValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setRecognitionTime(recognitionTimeValue.getText().toString());
                            myApplication.DATALST.get(DATA_INDEX).setHaveAlarm(isHaveAlarm ? 1 : 0);

                            if(isHaveAlarm) {
                                myApplication.DATALST.get(DATA_INDEX).setAlarmType(alarmTypeValue.getText().toString());
                                myApplication.DATALST.get(DATA_INDEX).setAlarmDesc(alarmDescValue.getText().toString());

                                if(!photoName.equals("")) {
                                    myApplication.DATALST.get(DATA_INDEX).setPhoto(photoName);
                                    photoName = "";
                                }
                                if(!videoPath.equals("")) {
                                    myApplication.DATALST.get(DATA_INDEX).setVideo(videoPath);
                                    videoPath = "";
                                }
                            } else {
                                myApplication.DATALST.get(DATA_INDEX).setAlarmType("");
                                myApplication.DATALST.get(DATA_INDEX).setAlarmDesc("");
                                myApplication.DATALST.get(DATA_INDEX).setPhoto("");
                                myApplication.DATALST.get(DATA_INDEX).setVideo("");
                            }

                            runOnUiThread(() -> { loadingAction(100, "数据修改成功"); });

                            new Handler().postDelayed(() -> {
                                Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                                startActivity(intent);
                                overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                                db.close();
                                loadingLayout.setVisibility(View.GONE);
                                MainActivity.this.finish();
                            }, 500);
                        } else { ToastUtils.show("请填报完整信息"); }
                    }
                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });
        // 保存(添加)-取消(修改)
        saveOrCancelBtn = findViewById(R.id.save_or_cancel_btn);
        if(!FLAG.equals("2")) { saveOrCancelBtn.setText("保存"); }
        else { saveOrCancelBtn.setText("取消"); }

        saveOrCancelBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> { saveOrCancelBtn.setBackgroundColor(Color.parseColor("#498bdd")); });
                break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> { saveOrCancelBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.cancel_btn_normal_style)); });

                    if(saveOrCancelBtn.getText().toString().equals("保存")) {
                        if(checkKeyInfoComplete()) {
                            loadingAction(50, "正在保存数据，请稍候...");

                            saveDataToDB();
                            saveDataToReportDataClass();

                            runOnUiThread(() -> { loadingAction(100, "数据保存成功，即将跳转到上报页..."); });

                            // 跳转到 上报页
                            new Handler().postDelayed(() -> {
                                Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                                startActivity(intent);
                                overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                                db.close();
                                loadingLayout.setVisibility(View.GONE);
                                MainActivity.this.finish();
                            }, 500);
                        } else { ToastUtils.show("请填报完整信息"); }
                    }
                    // 修改(取消)
                    else {
                        /* 弹窗 是否确定取消保存
                        dialogTitle.setText("修改取消");
                        dialogMessage.setText("确定放弃修改吗？");
                        dialogBlock.setVisibility(View.VISIBLE);
                        */
                        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setTitle("放弃修改").create();
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                            dialog.dismiss();

                            Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.activity_slide_out_left, R.anim.activity_slide_in_right);

                            db.close();
                            MainActivity.this.finish();
                        });
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {dialog.dismiss();});
                        dialog.setMessage("确定放弃当前修改吗？");
                        dialog.show();
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // 对话框
        dialogBlock = findViewById(R.id.dialog_block);
        dialogBlock.setOnTouchListener((v, e) -> {
            v.performClick();
            //dialogBlock.setVisibility(View.GONE);
            return true;
        });
        RelativeLayout dialogLayout = findViewById(R.id.dialog_layout);
        dialogLayout.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });
        ImageView dialogClose = findViewById(R.id.dialog_close);
        dialogClose.setOnClickListener(v -> dialogBlock.setVisibility(View.GONE));

        dialogTitle   = findViewById(R.id.dialog_title);
        dialogMessage = findViewById(R.id.dialog_message);

        dialogOKBtn   = findViewById(R.id.dialog_ok_btn);
        dialogOKBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> dialogOKBtn.setTextColor(Color.parseColor("#498bdd")));
                    break;

                case MotionEvent.ACTION_UP:
                    runOnUiThread(() -> { dialogOKBtn.setTextColor(Color.parseColor("#1e90ff")); });

                    if(isMenuClick) {
                        if(checkKeyInfoComplete()) {
                            loadingAction(50, "正在保存数据，请稍候...");

                            saveDataToDB();
                            saveDataToReportDataClass();

                            new Handler().postDelayed(() -> {
                                loadingAction(100, "数据保存成功，即将跳转到上报页...");

                                new Handler().postDelayed(() -> {
                                    Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                                    startActivity(intent);
                                    overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

                                    db.close();
                                    loadingLayout.setVisibility(View.GONE);
                                    MainActivity.this.finish();
                                }, 500);
                            }, 1000);
                        } else { ToastUtils.show("请填报完整信息"); }
                    } else {
                        Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);

                        db.close();
                        dialogBlock.setVisibility(View.GONE);
                        MainActivity.this.finish();
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });
        dialogCancelBtn = findViewById(R.id.dialog_cancel_btn);
        dialogCancelBtn.setOnTouchListener((v, e) -> {
            v.performClick();

            switch(e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    runOnUiThread(() -> dialogCancelBtn.setTextColor(Color.parseColor("#498bdd")));
                    break;

                case MotionEvent.ACTION_UP:
                    if(isMenuClick) {
                        Intent intent = new Intent(MainActivity.this, ReportActivity.class);

                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);

                        db.close();
                        MainActivity.this.finish();
                    } else {
                        runOnUiThread(() -> {
                            dialogCancelBtn.setTextColor(Color.parseColor("#1e90ff"));
                            dialogBlock.setVisibility(View.GONE);
                        });
                    }

                    break;

                case MotionEvent.ACTION_MOVE: break;
                default: break;
            }

            return true;
        });

        // Loading Layout
        loadingLayout = super.findViewById(R.id.loading_layout);
        loadingLayout.setElevation(1);
        loadingLayout.setClickCallback(() -> { /*to do sth.*/ });
    }

    // 检查 必填项 信息是否填报 完整
    private boolean checkKeyInfoComplete() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": checkKeyInfoComplete()");

        boolean isInfoComplete = false;
        boolean go_next        = false;

        if(!areaValue.getText().toString().equals("") &&
           !deviceTypeValue.getText().toString().equals("") &&
           !positionNameValue.getText().toString().equals("") &&
           !recognitionTypeValue.getText().toString().equals("") &&
           !resultValue.getText().toString().equals("") &&
           !unitValue.getText().toString().equals("") &&
           !recognitionTimeValue.getText().toString().equals("")) {
            isInfoComplete = true;
        }

        if(isHaveAlarm) {
            if(!alarmTypeValue.getText().toString().equals("") &&
               !alarmDescValue.getText().toString().equals("")) {

                if(DATA_INDEX != -1) {
                    if(!myApplication.DATALST.get(DATA_INDEX).getPhoto().equals("")) {
                        go_next = true;
                    } else { if(!photoName.equals("")) { go_next = true; } }
                } else { if(!photoName.equals("")) { go_next = true; } }
            }
        } else { if(isInfoComplete) { go_next = true; } }

        return go_next;
    }

    // 将数据 保存到 ReportData Class
    private void saveDataToReportDataClass() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": saveDataToReportDataClass()");

        ReportData data = new ReportData();

        data.setArea(areaValue.getText().toString());
        data.setDeviceType(deviceTypeValue.getText().toString());
        data.setPositionName(positionNameValue.getText().toString());
        data.setRecognitionType(recognitionTypeValue.getText().toString());
        data.setResult(resultValue.getText().toString());
        data.setUnit(unitValue.getText().toString());
        data.setRecognitionTime(recognitionTimeValue.getText().toString());
        data.setHaveAlarm(isHaveAlarm ? 1 : 0);
        data.setAlarmType(isHaveAlarm ? alarmTypeValue.getText().toString() : "");
        data.setAlarmDesc(isHaveAlarm ? alarmDescValue.getText().toString() : "");
        data.setPhoto(isHaveAlarm ? photoName : "");
        data.setVideo(isHaveAlarm ? videoPath : "");

        myApplication.DATALST.add(data);

        photoName = "";
        videoPath = "";
    }
    // 将数据 保存到 datas 表
    private void saveDataToDB() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": saveDataToDB()");

        ContentValues cv  = new ContentValues();
        long insertResult = -1;

        CUR_AREA_ID = areasLst.indexOf(areaValue.getText().toString()) + 1;

        cv.put("area", CUR_AREA_ID);

        StringBuffer deviceIdStr          = new StringBuffer("");
        StringBuffer deviceNameStr        = new StringBuffer("");
        StringBuffer pointIdStr           = new StringBuffer("");
        StringBuffer recognitionTypeIdStr = new StringBuffer("");

        // 获取 device_id
        getDeviceId(deviceIdStr, deviceNameStr, pointIdStr, recognitionTypeIdStr);

        if(!deviceIdStr.toString().equals("")) { cv.put("device_id", Integer.parseInt(deviceIdStr.toString())); }
        if(!deviceNameStr.toString().equals("")) { cv.put("device_name", deviceNameStr.toString()); }
        if(!pointIdStr.toString().equals("")) { cv.put("point_id", Integer.parseInt(pointIdStr.toString())); }

        cv.put("point_name", positionNameValue.getText().toString());

        if(!recognitionTypeIdStr.toString().equals("")) { cv.put("recognition_type_id", Integer.parseInt(recognitionTypeIdStr.toString())); }

        cv.put("result", resultValue.getText().toString());
        cv.put("unit_id", unitLst.indexOf(unitValue.getText().toString()) + 1);
        cv.put("recognition_time", recognitionTimeValue.getText().toString());
        cv.put("alarm_status", isHaveAlarm ? 1 : 0);

        if(isHaveAlarm) {
            cv.put("alarm_type_id", alarmTypeLst.indexOf(alarmTypeValue.getText().toString()) + 1);
            cv.put("alarm_description", alarmDescValue.getText().toString());
            cv.put("image", photoName);
            cv.put("video", videoPath);
        }

        insertResult = db.insert("datas", null, cv);
        if(insertResult != -1) {
            // 初始化reports
            if(FLAG.equals("0")) {
                if(!myApplication.REPORT_ID_INITIALED) {
                    myApplication.REPORT_ID_INITIALED = true;

                    // 为当前用户创建 report_id(reports表)
                    cv.clear();
                    insertResult = -1;

                    cv.put("time", "");
                    cv.put("is_reported", 0);

                    insertResult = db.insert("reports", null, cv);
                    if(insertResult != -1) {
                        // 创建user_report 关联数据
                        // 获取 report_id
                        Cursor cr3 = sqliteUtil.GetLastData(db, "reports");
                        if(cr3.getCount() != 0 && cr3.moveToNext()) {
                            myApplication.REPORT_ID = cr3.getInt(cr3.getColumnIndex("id"));
                            cr3.close();

                            cv.clear();
                            insertResult = -1;

                            cv.put("user_id", myApplication.USER_ID);
                            cv.put("report_id", myApplication.REPORT_ID);

                            insertResult = db.insert("user_report", null, cv);
                            if(insertResult != -1) {
                                // 创建data_report 关联数据
                                // 获取data_id
                                Cursor cr4 = sqliteUtil.GetLastData(db, "datas");
                                if(cr4.getCount() != 0 && cr4.moveToNext()) {
                                    int data_id = cr4.getInt(cr4.getColumnIndex("id"));
                                    cr4.close();

                                    cv.clear();
                                    insertResult = -1;

                                    cv.put("data_id", data_id);
                                    cv.put("report_id", myApplication.REPORT_ID);

                                    insertResult = db.insert("data_report", null, cv);
                                    if(insertResult != -1) {
                                        cv.clear();
                                        cv = null;
                                    } else { ToastUtils.show("data_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                                } else { ToastUtils.show("获取data_id失败"); }
                            } else { ToastUtils.show("user_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                        } else { ToastUtils.show("获取report_id失败"); }
                    } else { ToastUtils.show("reports插入数据失败(" + String.valueOf(insertResult) + ")"); }
                } else {
                    Cursor cursor = sqliteUtil.GetLastData(db, "datas");

                    if(cursor.getCount() != 0 && cursor.moveToNext()) {
                        int data_id = cursor.getInt(cursor.getColumnIndex("id"));
                        cursor.close();

                        cv.clear();
                        insertResult = -1;

                        cv.put("data_id", data_id);
                        cv.put("report_id", myApplication.REPORT_ID);

                        insertResult = db.insert("data_report", null, cv);
                        if(insertResult != -1) {
                            cv.clear();
                            cv = null;
                        } else { ToastUtils.show("data_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                    } else { ToastUtils.show("获取last_data_id错误"); }
                }
            }
            // 新添加 数据
            else if(FLAG.equals("1")) {
                // 新添data_report 关联
                Cursor cr5 = sqliteUtil.GetLastData(db, "datas");
                if(cr5.getCount() != 0 && cr5.moveToNext()) {
                    int data_id = cr5.getInt(cr5.getColumnIndex("id"));
                    cr5.close();

                    cv.clear();
                    insertResult = -1;

                    cv.put("data_id", data_id);

                    // TODO.用户有未上报 数据，且 新 添加 数据(初始化并获得 myApplication.REPORT_ID)
                    if(myApplication.UNREPORT_DATA_ID_LST.size() > 0) {
                        if(!myApplication.UNREPORT_NEW_DATA_INITIALED) {
                            myApplication.UNREPORT_NEW_DATA_INITIALED = true;
                            System.out.println("用户有未上报数据，且新'添加'数据(初始化myApplication.REPORT_ID)");

                            // 初始化reports表
                            ContentValues cv2 = new ContentValues();
                            cv2.put("time", "");
                            cv2.put("is_reported", 0);

                            long insr = -1;
                            insr = db.insert("reports", null, cv2);
                            if(insr != -1) {
                                Cursor cr = sqliteUtil.GetLastData(db, "reports");

                                if(cr.getCount() != 0 && cr.moveToNext()) {
                                    myApplication.REPORT_ID = cr.getInt(cr.getColumnIndex("id"));
                                    System.out.println("REPORT_ID: " + String.valueOf(myApplication.REPORT_ID));
                                    cr.close();

                                    cv2.clear();
                                    insr = -1;

                                    cv2.put("user_id", myApplication.USER_ID);
                                    cv2.put("report_id", myApplication.REPORT_ID);

                                    // 关联 user_report表
                                    insr = db.insert("user_report", null, cv2);
                                    if(insr != -1) {
                                        cv2.clear();
                                        insr = -1;

                                        // TODO.user_report关联成功之后，将REPORT_ID插入到data_report表，及 UNREPORT_ID_LST
                                        cv.put("report_id", myApplication.REPORT_ID);

                                        myApplication.UNREPORT_ID_LST.add(myApplication.REPORT_ID);
                                        myApplication.UNREPORT_DATA_ID_LST.add(data_id);

                                    } else { ToastUtils.show("user_report插入数据失败(" + String.valueOf(insr) + ")"); }
                                } else { ToastUtils.show("获取report_id错误");   }
                            } else { ToastUtils.show("reports插入数据失败(" + String.valueOf(insr) + ")"); }
                        }
                        // myApplication.REPORT_ID已初始化，正常添加即可
                        else {
                            cv.put("report_id", myApplication.REPORT_ID);
                            myApplication.UNREPORT_DATA_ID_LST.add(data_id);
                        }
                    }
                    // 正常新添加 数据
                    else { cv.put("report_id", myApplication.REPORT_ID); }

                    insertResult = db.insert("data_report", null, cv);
                    if(insertResult != -1) {
                        cv.clear();
                        cv = null;
                    } else { ToastUtils.show("data_report插入数据失败(" + String.valueOf(insertResult) + ")"); }
                } else { ToastUtils.show("获取last_data_id错误"); }
            }
        } else { ToastUtils.show("数据库保存数据失败(" + String.valueOf(insertResult) + ")"); }
    }
    // 获取 device_id 和其他相关项
    private void getDeviceId(StringBuffer deviceIdStr, StringBuffer deviceNameStr, StringBuffer pointIdStr, StringBuffer recognitionTypeIdStr) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": getDeviceId()");

        // 根据 area_id 和 name 获取 device_type_id
        int areaId = -1;

        areaId      = areasLst.indexOf((String)areaValue.getText().toString()) + 1;
        CUR_AREA_ID = areaId;

        Cursor cr = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(areaId) + " AND name = '" + deviceTypeValue.getText().toString() + "';", null);
        if(cr.getCount() != 0 && cr.moveToNext()) {
            int deviceTypeId = -1;
            deviceTypeId     = cr.getInt(cr.getColumnIndex("id"));
            cr.close();

            // 根据 device_type_id 和 point_name 获取 point_id、device_name
            if(deviceTypeId != -1) {
                CUR_DEVICE_TYPE_ID = deviceTypeId;

                Cursor cr2 = db.rawQuery("SELECT * FROM points WHERE device_type_id = " + String.valueOf(CUR_DEVICE_TYPE_ID) + " AND name = '" + positionNameValue.getText().toString() + "';", null);

                if(cr2.getCount() != 0 && cr2.moveToNext()) {
                    int pointId           = -1;
                    int recognitionTypeId = -1;

                    pointId           = cr2.getInt(cr2.getColumnIndex("id"));
                    deviceNameStr     = deviceNameStr.append(cr2.getString(cr2.getColumnIndex("device_name")));

                    Cursor c = db.rawQuery("SELECT id FROM recognition_type WHERE name = '" + recognitionTypeValue.getText().toString() + "';", null);
                    if(c.getCount() != 0 && c.moveToNext()) {
                        recognitionTypeId = c.getInt(c.getColumnIndex("id"));
                    } else { ToastUtils.show("获取识别类型id错误"); }
                    c.close();

                    cr2.close();

                    if(pointId != -1) {
                        CUR_POINT_ID = pointId;

                        deviceIdStr = deviceIdStr.append(String.valueOf(deviceTypeId)).append(String.valueOf(pointId));
                        pointIdStr  = pointIdStr.append(String.valueOf(pointId));
                    } else { ToastUtils.show("获取点位id错误");                        }

                    if(recognitionTypeId != -1) {
                        recognitionTypeIdStr = recognitionTypeIdStr.append(String.valueOf(recognitionTypeId));
                    } else { ToastUtils.show("获取识别类型id错误");   }
                } else { ToastUtils.show("获取点位错误"); }
            } else { ToastUtils.show("获取设备类型id错误"); }
        } else { ToastUtils.show("获取设备类型错误"); }
    }

    // 获取 下一个 点位
    private Map<String, Object> checkNextPoint() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": checkNextPoint()");

        Map<String, Object> m = new HashMap<String, Object>();

        Cursor c = sqliteUtil.GetLastData(db, "points");
        if(c.getCount() != 0 && c.moveToNext()) {
            int last_cpd = c.getInt(c.getColumnIndex("id"));
            c.close();

            // 当前点位为 电站最后一个点位
            if(CUR_POINT_ID + 1 > last_cpd) {
                m.put("area", "1#机组零米室内");
                m.put("device_type", "空压机A");
                m.put("point_name", "空压机A");
            } else {
                int area_id        = -1;
                int device_type_id = -1;

                Cursor c2 = db.rawQuery("SELECT device_type_id FROM points WHERE id = " + String.valueOf(CUR_POINT_ID + 1) + ";", null);
                if(c2.getCount() != 0 && c2.moveToNext()) {
                    int ntd = c2.getInt(c2.getColumnIndex("device_type_id"));
                    c2.close();

                    // 设备类型改变，当前点位为 当前区域 当前设备类型 最后一个点位
                    if(ntd != CUR_DEVICE_TYPE_ID) {
                        System.out.println("设备类型改变");

                        Cursor c3 = db.rawQuery("SELECT area_id FROM device_type WHERE id = " + String.valueOf(ntd) + ";", null);

                        if(c3.getCount() != 0 && c3.moveToNext()) {
                            int ned = c3.getInt(c3.getColumnIndex("area_id"));
                            c3.close();

                            // 区域改变
                            if(ned != CUR_AREA_ID) {
                                System.out.println("区域改变");
                                area_id = ned;
                            }
                            // 未改变
                            else {
                                System.out.println("区域未改变");
                                area_id = CUR_AREA_ID;
                            }

                            device_type_id = ntd;
                        } else { ToastUtils.show("获取ned错误"); }
                    }
                    // 未改变，下一个 点位
                    else {
                        System.out.println("设备类型未改变");

                        area_id        = CUR_AREA_ID;
                        device_type_id = CUR_DEVICE_TYPE_ID;
                    }
                } else { ToastUtils.show("获取ntd错误"); }

                // 根据id获取 下一个点位信息
                if(area_id != -1 && device_type_id != -1) {
                    System.out.println(String.valueOf(area_id) + " " + String.valueOf(device_type_id) + " " + String.valueOf(CUR_POINT_ID + 1));
                    getNextDataInfo(area_id, device_type_id, CUR_POINT_ID + 1, m);
                }
            }
        } else { ToastUtils.show("获取last_point_id错误"); }

        return m;
    }
    // 获取 下一项 填报信息
    private void getNextDataInfo(int area_id, int device_type_id, int point_id, Map<String, Object> m) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": getNextDataInfo()");

        // 获取 设备类型、点位
        String deviceName = null;
        String pointName  = null;

        Cursor c4 = db.rawQuery("SELECT name FROM device_type WHERE id = " + String.valueOf(device_type_id) + ";", null);
        if(c4.getCount() != 0 && c4.moveToNext()) {
            deviceName = c4.getString(c4.getColumnIndex("name"));
            c4.close();
        }
        Cursor c5 = db.rawQuery("SELECT name FROM points WHERE id = " + String.valueOf(point_id) + ";", null);
        if(c5.getCount() != 0 && c5.moveToNext()) {
            pointName = c5.getString(c5.getColumnIndex("name"));
            c5.close();
        }

        if(deviceName != null && pointName != null) {
            m.put("area", areasLst.get(area_id - 1));
            m.put("device_type", deviceName);
            m.put("point_name", pointName);
        } else { ToastUtils.show("获取设备类型、点位错误");   }
    }

    // 区域选择
    private void onDeviceTypeSelect() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDeviceTypeSelect()");

        SELECT_TYPE = 1;
        deviceTypeLst.clear();
        loadingAction(50, "正在加载数据，请稍候...");

        // 根据当前 area_id 刷新该区域下的 所有设备类型
        Cursor cr = db.rawQuery("SELECT name FROM device_type WHERE area_id = " + String.valueOf(areasLst.indexOf(areaValue.getText().toString()) + 1) + ";", null);

        if(cr.getCount() != 0) {
            while(cr.moveToNext()) { deviceTypeLst.add(cr.getString(cr.getColumnIndex("name"))); }
            cr.close();

            if(deviceTypeLst.size() > 0) {
                new Handler().postDelayed(() -> {
                    selectIndex = deviceTypeLst.indexOf(deviceTypeValue.getText().toString());

                    SelectBlockItemListInitial(deviceTypeLst, "选择设备类型", 1);
                    loadingLayout.setVisibility(View.GONE);
                }, 500);
            } else {
                ToastUtils.show("加载设备类型数据异常");
                loadingLayout.setVisibility(View.GONE);
            }
        } else { ToastUtils.show("获取设备类型错误"); }
    }
    // 点位选择
    private void onPositionNameSelect() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onPositionNameSelect()");

        SELECT_TYPE = 2;
        positionNameLst.clear();
        loadingAction(50, "正在加载数据，请稍候...");

        // 根据 area_id 与 device_type_name 确定唯一的 device_tye_id，然后从points表 筛选出 点位
        Cursor cr = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(areasLst.indexOf(areaValue.getText().toString()) + 1) + " AND name = '" + deviceTypeValue.getText().toString() + "';" , null);

        int ctd = -1;
        if(cr.getCount() != 0 && cr.moveToNext()) {
            ctd = cr.getInt(cr.getColumnIndex("id"));
            cr.close();

            if(ctd != -1) {
                Cursor cr2 = db.rawQuery("SELECT name FROM points WHERE device_type_id = " + String.valueOf(ctd) + ";", null);
                if(cr2.getCount() != 0) {
                    while(cr2.moveToNext()) { positionNameLst.add(cr2.getString(cr2.getColumnIndex("name"))); }
                    cr2.close();

                    if(positionNameLst.size() > 0) {
                        new Handler().postDelayed(() -> {
                            selectIndex = positionNameLst.indexOf(positionNameValue.getText().toString());

                            SelectBlockItemListInitial(positionNameLst, "选择点位名称", 2);
                            loadingLayout.setVisibility(View.GONE);
                        }, 500);
                    } else {
                        ToastUtils.show("加载点位数据异常");
                        loadingLayout.setVisibility(View.GONE);
                    }
                } else { ToastUtils.show("获取点位错误"); }

            } else { ToastUtils.show("获取设备类型id错误"); }
        } else { ToastUtils.show("获取设备类型错误"); }
    }
    // Table 栏点击监听
    @SuppressLint("NonConstantResourceId")
    public void onColumnClick(View view) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onColumnClick()");

        switch(view.getId()) {
            case R.id.area_select_layout:
                areaSelectItemLayout.setVisibility(View.VISIBLE);
                break;

            case R.id.device_type_select_layout:
                // 重新进入页面 添加 新数据时，不能先选 设备类型
                if(FLAG.equals("1")) {
                    if(!areaValue.getText().toString().equals("")) { onDeviceTypeSelect(); }
                    else { ToastUtils.show("请先选择区域"); }
                } else { onDeviceTypeSelect(); }

                break;

            case R.id.position_name_select_layout:
                // 重新进入页面 添加 新数据时，不能先选 点位
                if(FLAG.equals("1")) {
                    if(!positionNameValue.getText().toString().equals("")) { onPositionNameSelect(); }
                    else { ToastUtils.show("清先选择区域"); }
                } else { onPositionNameSelect(); }

                break;

            case R.id.recognition_type_select_layout:
                SELECT_TYPE = 3;

                selectListAdapter = new SelectListAdapter(this, recognitionTypeLst);
                selectItemList.setAdapter(selectListAdapter);

                selectTitle.setText("选择识别类型");

                selectIndex = recognitionTypeLst.indexOf(recognitionTypeValue.getText().toString());
                selectItemLayout.setVisibility(View.VISIBLE);

                break;

            case R.id.unit_select_layout:
                SELECT_TYPE = 4;

                selectListAdapter = new SelectListAdapter(this, unitLst);
                selectItemList.setAdapter(selectListAdapter);

                selectTitle.setText("选择单位");
                selectIndex = unitLst.indexOf(unitValue.getText().toString());

                selectItemLayout.setVisibility(View.VISIBLE);

                break;

            case R.id.recognition_time_select_layout:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm.isActive()) {
                    // 强制隐藏 软键盘
                    View focv = MainActivity.this.getCurrentFocus();
                    if(focv != null) { imm.hideSoftInputFromWindow(focv.getWindowToken(), 0); }
                }

                @SuppressLint("SetTextI18n") TimePickerView v = new TimePickerView.Builder(this, (date, view1) -> runOnUiThread(() -> {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                String timeStr = sdf.format(date);
                String[] data  = timeStr.split("-");

                    recognitionTimeValue.setText(data[0] + "年" + data[1] + "月" + data[2] + "日" + data[3] + "时" + data[4] + "分" + data[5] + "秒");
                })).setType(new boolean[]{true, true, true, true, true, true})
                   .setDate(Calendar.getInstance())
                   .build();

                v.show();

                break;

            case R.id.alarm_type_select_layout:
                SELECT_TYPE = 5;

                selectListAdapter = new SelectListAdapter(this, alarmTypeLst);
                selectItemList.setAdapter(selectListAdapter);

                selectTitle.setText("选择告警类型");
                selectIndex = alarmTypeLst.indexOf(alarmTypeValue.getText().toString());
                selectItemLayout.setVisibility(View.VISIBLE);

                break;

            default: break;
        }
    }

    // Update deviceTypeLst
    private void updateDeviceTypeLst(int ced) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": updateDeviceTypeLst(" + String.valueOf(ced) +")");

        if(ced != CUR_AREA_ID) { CUR_AREA_ID = ced; }

        deviceTypeLst.clear();

        Cursor c = db.rawQuery("SELECT name FROM device_type WHERE area_id = " + String.valueOf(ced) + ";", null);
        if(c.getCount() != 0) {
            while(c.moveToNext()) { deviceTypeLst.add(c.getString(c.getColumnIndex("name"))); }
            c.close();

            runOnUiThread(() -> deviceTypeValue.setText(deviceTypeLst.get(0)));
        } else { ToastUtils.show("获取设备类型错误"); }
    }
    // Update positionNameLst
    private void updatePositionNameLst(int ced) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": updatePositionNameLst(" + String.valueOf(ced) + ")");

        // 更新 点位 并过滤
        positionNameLst.clear();

        Cursor c = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(ced) + " AND name = '" + deviceTypeValue.getText().toString() + "';", null);
        if(c.getCount() != 0 && c.moveToNext()) {
            int ctd = c.getInt(c.getColumnIndex("id"));
            c.close();

            Cursor c2 = db.rawQuery("SELECT name FROM points WHERE device_type_id = " + String.valueOf(ctd) + ";", null);
            if (c2.getCount() != 0) {
                while (c2.moveToNext()) { positionNameLst.add(c2.getString(c2.getColumnIndex("name"))); }
                c2.close();

                runOnUiThread(() -> positionNameValue.setText(positionNameLst.get(0)));
            } else { ToastUtils.show("获取ctd点位集错误"); }
        } else { ToastUtils.show("获取设备类型id错误"); }
    }
    // 区域选择Layout初始化
    @SuppressLint("UseCompatLoadingForDrawables")
    private void areaSelectListInitial() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": areaSelectListInitial()");

        // 6大区域
        //for(int i = 0; i < 6; ++i) {
        for(int i = 0; i < areasLst.size(); ++i) {
            LinearLayout linearLayout        = new LinearLayout(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);

            linearLayout.setLayoutParams(params);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setTag("AREA_ITEM_" + String.valueOf(i));

            if(i % 2 == 0) { linearLayout.setBackgroundColor(Color.WHITE); }

            // RadioButton
            RadioButton radioBtn                     = new RadioButton(this);
            LinearLayout.LayoutParams radioBtnParams = new LinearLayout.LayoutParams(32, 32);
            radioBtnParams.setMarginStart(24);
            radioBtnParams.gravity = Gravity.CENTER;
            radioBtn.setLayoutParams(radioBtnParams);

            radioBtn.setTextSize(0);
            radioBtn.setButtonDrawable(null);
            radioBtn.setButtonDrawable(getResources().getDrawable(R.drawable.radio_btn_selector));
            radioBtn.setGravity(Gravity.CENTER_VERTICAL);
            radioBtn.setChecked(false);

            radioBtnLst.add(radioBtn);

            // TextView
            TextView textView                        = new TextView(this);
            LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            textViewParams.setMarginStart(12);
            textViewParams.gravity = Gravity.CENTER;
            textViewParams.setLayoutDirection(LayoutDirection.INHERIT);
            textView.setLayoutParams(textViewParams);

            textView.setText(areasLst.get(i).toString());
            textView.setTextSize(16);
            textView.setTextColor(Color.BLACK);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            textView.setGravity(Gravity.CENTER_VERTICAL);

            linearLayout.addView(radioBtn);
            linearLayout.addView(textView);

            linearLayout.setOnClickListener(view -> {
                String tag = view.getTag().toString();
                int curIdx = Integer.parseInt(tag.substring(10, tag.length()));

                runOnUiThread(() -> {
                    radioBtnLst.get(curIdx).setChecked(true);

                    for(int j = 0; j < radioBtnLst.size(); ++j) {
                        if(curIdx != j) { radioBtnLst.get(j).setChecked(false); }
                    }

                    areaValue.setText(areasLst.get(curIdx).toString());

                    /// 根据当前 区域 更新并过滤 设备类型 与 点位
                    updateDeviceTypeLst(curIdx + 1);   // 设备类型
                    updatePositionNameLst(curIdx + 1); // 点位

                    areaSelectItemLayout.setVisibility(View.GONE);
                });
            });

            areaSelectList.addView(linearLayout);
        }
    }
    // Select Block选项列表动态初始化
    @SuppressLint({"ClickableViewAccessibility", "UseCompatLoadingForDrawables"})
    private void SelectBlockItemListInitial(List<String> lst, String title, int flag) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": selectBlockItemListInitial()");

        // 每项item 70，默认 10[marginTop] + 70[title] + 350[lst显示5个可见项] + 10[footer]
        // Root
        RelativeLayout rl              = new RelativeLayout(this);
        RelativeLayout.LayoutParams rp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        rl.setLayoutParams(rp);
        rl.setElevation(0);
        rl.setBackgroundColor(Color.parseColor("#55000000"));
        rl.setOnTouchListener((v, e) -> {
            v.performClick();
            rl.setVisibility(View.GONE);
            return true;
        });

        // Container
        RelativeLayout cl              = new RelativeLayout(this);
        RelativeLayout.LayoutParams cp = new RelativeLayout.LayoutParams(640, lst.size() >= 5 ? 500 : 150 + 70 * lst.size());
        cp.addRule(RelativeLayout.CENTER_IN_PARENT, rl.getId());

        cl.setLayoutParams(cp);
        cl.setBackground(getResources().getDrawable(R.drawable.select_block_style));
        cl.setOnTouchListener((v, e) -> {
            v.performClick();
            return true;
        });

        // Title
        TextView tv                    = new TextView(this);
        RelativeLayout.LayoutParams tp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);
        tp.addRule(RelativeLayout.ALIGN_PARENT_TOP, cl.getId());
        tp.addRule(RelativeLayout.CENTER_HORIZONTAL, cl.getId());
        tp.topMargin = 10;

        tv.setId(tv.getId());
        tv.setLayoutParams(tp);
        tv.setGravity(Gravity.CENTER);
        tv.setText(title);
        tv.setTextSize(17);
        tv.setTextColor(Color.BLACK);
        tv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // ListView
        ListView lv                     = new ListView(this);
        RelativeLayout.LayoutParams lvp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, lst.size() >= 5 ? 350 : 70 * lst.size());
        lvp.addRule(RelativeLayout.BELOW, tv.getId());
        lvp.topMargin = 80;

        lv.setId(lv.getId());
        lv.setLayoutParams(lvp);
        lv.setDivider(null);
        lv.setBackgroundColor(Color.parseColor("#00ffffff"));

        selectListAdapter = new SelectListAdapter(MainActivity.this, lst);
        lv.setAdapter(selectListAdapter);

        // Item Click
        lv.setOnItemClickListener((adapterView, v, position, id) -> runOnUiThread(() -> {
            selectIndex = position;

            switch(flag) {
                // 选择 设备类型
                case 1:
                    deviceTypeValue.setText(selectListAdapter.valuesLst.get(position));

                    // 更新 点位
                    positionNameLst.clear();

                    int ced = areasLst.indexOf(areaValue.getText().toString()) + 1;
                    int ctd = -1;

                    Cursor cr = db.rawQuery("SELECT id FROM device_type WHERE area_id = " + String.valueOf(ced) + " AND name = '" + deviceTypeValue.getText().toString() + "';", null);

                    if(cr.getCount() != 0 && cr.moveToNext()) {
                        ctd = cr.getInt(cr.getColumnIndex("id"));
                        cr.close();

                        if(ctd != -1) {
                            if(ctd != CUR_DEVICE_TYPE_ID) { CUR_DEVICE_TYPE_ID = ctd; }

                            Cursor cr2 = db.rawQuery("SELECT name FROM points WHERE device_type_id = " + String.valueOf(ctd) + ";", null);
                            if(cr2.getCount() != 0) {
                                while(cr2.moveToNext()) { positionNameLst.add(cr2.getString(cr2.getColumnIndex("name"))); }
                                cr2.close();

                                if(positionNameLst.size() > 0) {
                                    // 更新 点位显示(默认显示第一个)
                                    positionNameValue.setText(positionNameLst.get(0));
                                } else { ToastUtils.show("获取点位错误pointNameLst(size=0)"); }
                            } else { ToastUtils.show("获取点位错误");   }
                        }
                    }
                    break;
                // 选择点位
                case 2:
                    positionNameValue.setText(selectListAdapter.valuesLst.get(position));
                    break;

                default: break;
            }
            rl.setVisibility(View.GONE);
        }));

        // Footer
        TextView fv                    = new TextView(this);
        RelativeLayout.LayoutParams fp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70);
        fp.addRule(RelativeLayout.BELOW, lv.getId());
        fp.topMargin = 430;

        fv.setLayoutParams(fp);
        fv.setBackground(getResources().getDrawable(R.drawable.select_block_footer_style));

        // Add
        cl.addView(tv);
        cl.addView(lv);
        cl.addView(fv);

        rl.addView(cl);

        rootLayout.addView(rl);
    }

    // 删除 照片或视频
    @SuppressLint("NonConstantResourceId")
    public void onDelete(View v) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDelete()");

        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setTitle("删除").create();

        switch(v.getId()) {
            case R.id.photo_delete_icon:
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                    loadingAction(50, "正在删除照片，请稍候...");

                    // 删除图片并更新信息
                    File f = null;
                    if(DATA_INDEX != -1) {
                        if(!myApplication.DATALST.get(DATA_INDEX).getPhoto().equals("")) {
                            f = new File(myApplication.DATALST.get(DATA_INDEX).getPhoto());
                        } else { f = new File(photoName); }
                    } else { f = new File(photoName); }

                    if(f.delete()) {
                        new Handler().postDelayed(() -> {
                            runOnUiThread(() -> {
                                captureImageView.setImageBitmap(null);
                                captureImageView.setBackgroundResource(R.drawable.capture_icon);

                                photoDeleteIcon.setVisibility(View.GONE);

                                if(DATA_INDEX != -1) { myApplication.DATALST.get(DATA_INDEX).setPhoto(""); }
                                else { photoName = ""; }
                            });

                            isCaptured = false;
                            loadingAction(100, "照片已删除");

                            new Handler().postDelayed(() -> {
                                loadingLayout.setVisibility(View.GONE);
                            }, 500);
                        }, 1000);
                    } else {
                        ToastUtils.show("未知错误，应用文件夹照片删除失败");
                        // TODO.
                    }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {dialog.dismiss();});
                dialog.setMessage("确定删除当前照片吗？");
                dialog.show();

                break;

            case R.id.video_delete_icon:
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", (d, which) -> {
                    loadingAction(50, "正在删除视频，请稍候...");

                    File f = null;
                    if(DATA_INDEX != -1) {
                        if(!myApplication.DATALST.get(DATA_INDEX).getVideo().equals("")) {
                            f = new File(myApplication.DATALST.get(DATA_INDEX).getVideo());
                        } else { f = new File(videoPath); }
                    } else { f = new File(videoPath); }

                    if(f.delete()) {
                        new Handler().postDelayed(() -> {
                            runOnUiThread(() -> {
                                videoImageView.setImageBitmap(null);
                                videoImageView.setBackgroundResource(R.drawable.video_icon);

                                videoDeleteIcon.setVisibility(View.GONE);

                                if(DATA_INDEX != -1) { myApplication.DATALST.get(DATA_INDEX).setVideo(""); }
                                else { videoPath = ""; }
                            });

                            isVideoed = false;
                            loadingAction(100, "视频已删除");

                            new Handler().postDelayed(() -> {
                                loadingLayout.setVisibility(View.GONE);
                            }, 500);
                        }, 1000);
                    } else {
                        ToastUtils.show("未知错误，应用文件夹视频删除失败");
                        // TODO.
                    }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", (d, which) -> {dialog.dismiss();});
                dialog.setMessage("确定删除当前视频吗？");
                dialog.show();

                break;

            default: break;
        }
    }

    // 获取视频缩略图
    private Bitmap getVideoThumbnailBmp() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": getVideoThumbnailBmp()");

        Bitmap bmp = null;
        MediaMetadataRetriever mr = new MediaMetadataRetriever();

        try {
            if(DATA_INDEX != -1) {
                if(!myApplication.DATALST.get(DATA_INDEX).getVideo().equals("")) {
                    mr.setDataSource(myApplication.DATALST.get(DATA_INDEX).getVideo());
                } else { mr.setDataSource(videoPath); }
            } else { mr.setDataSource(videoPath); }

            bmp = mr.getFrameAtTime(-1);
        } catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            ToastUtils.show("获取视频缩略图:" + e.getMessage());
        } finally {
            try { mr.release(); }
            catch(RuntimeException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                ToastUtils.show("获取视频缩略图:" + e.getMessage());
            }
        }

        if(bmp == null) { return null; }

        if(MediaStore.Images.Thumbnails.MINI_KIND == MediaStore.Images.Thumbnails.MINI_KIND) {
            int w   = bmp.getWidth();
            int h   = bmp.getHeight();
            int max = Math.max(w, h);

            if(max > 512) {
                float scale = 512f / max;
                int w_      = Math.round(scale * w);
                int h_      = Math.round(scale * h);
                bmp         = Bitmap.createScaledBitmap(bmp, w_, h_, true);
            }
        } else if(MediaStore.Images.Thumbnails.MINI_KIND == MediaStore.Images.Thumbnails.MICRO_KIND) {
            bmp = ThumbnailUtils.extractThumbnail(bmp, 85, 60, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
        }

        return bmp;
    }

    // LoadingLayout显示与隐藏
    private void loadingAction(int step, String note) {
        loadingLayout.setProgressStep(step);
        loadingLayout.setProgressNote(note);
        loadingLayout.setVisibility(View.VISIBLE);
    }

    /***********************************Activity Override Start***********************************/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onActivityResult(" + requestCode + "," + " " + requestCode + "," + " " + data + ")");
        // 打开 相机 拍照或录像
        if(requestCode == CAMERA_CAPTURE) {
            // 保存照片
            if(resultCode == RESULT_OK) {
                runOnUiThread(() -> loadingAction(50, "拍照成功，正在保存图片，请稍候..."));

                new Thread(() -> {
                    try {
                        photoBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(captureImageUri));

                        if(photoBitmap != null) {
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

                            Date date            = new Date(System.currentTimeMillis());
                            String timeFlag      = sdf.format(date);

                            photoName      = myApplication.APP_IMAGES_PATH + File.separator + timeFlag + ".jpg";
                            File photoFile = new File(photoName);

                            try {
                                FileOutputStream fos = new FileOutputStream(photoFile);

                                photoBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fos);
                                fos.flush();
                                fos.close();

                                new Handler(Looper.getMainLooper()).postDelayed(() -> loadingAction(100, "照片保存成功"), 500);

                                runOnUiThread(() -> {
                                    captureImageView.setImageBitmap(photoBitmap);
                                    photoDeleteIcon.setVisibility(View.VISIBLE);
                                });
                                isCaptured = true;

                                // 删除 Android/data/packagename/cache/ 下缓存照片
                                System.out.println(captureImageUri.getPath());

                                String cfPath = "/storage/emulated/0/Android/data/" + getPackageName() + "/cache/" + cachePhotoTime + ".jpg";
                                System.out.println(cfPath);
                                File f = new File(cfPath);

                                if(f.exists()) {
                                    if(f.delete()) { System.out.println("缓存照片删除成功"); }
                                    else { ToastUtils.show("缓存照片删除失败"); }
                                } else { ToastUtils.show("缓存照片文件出错"); }

                                new Handler(Looper.getMainLooper()).postDelayed(() -> loadingLayout.setVisibility(View.GONE), 500);
                            } catch(IOException e) {
                                System.out.println(e.getMessage());
                                e.printStackTrace();

                                runOnUiThread(() -> loadingLayout.setVisibility(View.GONE));
                                Looper.prepare();
                                ToastUtils.show("照片保存到应用文件夹失败");
                            }
                        } else {
                            runOnUiThread(() -> loadingLayout.setVisibility(View.GONE));
                            Looper.prepare();

                            ToastUtils.show("bitmap=null，照片保存失败");
                        }
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        e.printStackTrace();

                        runOnUiThread(() -> loadingLayout.setVisibility(View.GONE));
                        Looper.prepare();

                        ToastUtils.show("照片保存失败:" + e.getMessage());
                    }
                }).start();
            } else { ToastUtils.show("拍照取消或失败"); }
        }
        if(requestCode == CAMERA_VIDEO) {
            // 保存视频
            if(resultCode == RESULT_OK) {
                runOnUiThread(() -> loadingAction(50, "拍摄成功，正在保存视频，请稍候..."));

                new Thread(() -> {
                    // 视频文件拷贝至data目录下
                    try {
                        FileInputStream fis  = new FileInputStream("/storage/emulated/0/Android/data/" + getPackageName() + "/cache/" + cacheVideoTime + ".mp4");

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                        Date date            = new Date(System.currentTimeMillis());
                        String timeFlag      = sdf.format(date);

                        videoPath      = myApplication.APP_VIDEOS_PATH + File.separator + timeFlag + ".mp4";
                        File videoFile = new File(videoPath);

                        FileOutputStream fos = new FileOutputStream(videoFile);

                        byte[] buf = new byte[1024];
                        int len    = -1;

                        while(-1 != (len = fis.read(buf))) { fos.write(buf, 0, len); }

                        fis.close();
                        fos.flush();
                        fos.close();

                        // 获取视频缩略图
                        Bitmap videoThumbnailBmp = getVideoThumbnailBmp();

                        if(videoThumbnailBmp != null) {
                            new Handler(Looper.getMainLooper()).postDelayed(() -> loadingAction(100, "视频保存成功"), 500);

                            runOnUiThread(() -> {
                                videoImageView.setImageBitmap(videoThumbnailBmp);
                                videoDeleteIcon.setVisibility(View.VISIBLE);
                            });
                            isVideoed = true;

                            // 删除 Android/data/packagename/cache/ 下缓存视频
                            System.out.println(captureVideoUri.getPath());

                            String cfPath = "/storage/emulated/0/Android/data/" + getPackageName() + "/cache/" + cacheVideoTime + ".mp4";
                            System.out.println(cfPath);
                            File f = new File(cfPath);

                            if(f.exists()) {
                                if(f.delete()) { System.out.println("缓存视频删除成功"); }
                                else { ToastUtils.show("缓存视频删除失败"); }
                            } else { ToastUtils.show("缓存视频文件出错"); }

                            new Handler(Looper.getMainLooper()).postDelayed(() -> loadingLayout.setVisibility(View.GONE), 500);
                        } else {
                            ToastUtils.show("获取视频缩略图失败");
                            // todo.
                        }
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        e.printStackTrace();

                        ToastUtils.show("保存视频到应用文件夹失败:" + e.getMessage());
                    }
                }).start();

                ToastUtils.show("拍摄完成");
            } else { ToastUtils.show("未知错误，拍摄失败"); }
        }
    }
    @Override
    protected void onStart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStart()");
        super.onStart();
    }
    @Override
    protected void onResume() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onResume()");
        super.onResume();
    }
    @Override
    protected void onPause() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onPause()");
        super.onPause();

        MediaPlayerManager.instance().pause();
    }
    @Override
    protected void onStop() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onStop()");
        super.onStop();
    }
    @Override
    protected void onRestart() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onRestart()");
        super.onRestart();
    }
    @Override
    protected void onDestroy() {
        System.out.println(MyApplication.LOG_OUTPUT_HEADER + Log_Tag + ": onDestroy()");

        MediaPlayerManager.instance().releasePlayerAndView(this);
        // 关闭数据库
        if(db != null && db.isOpen()) { db.close(); }

        super.onDestroy();
    }
    /***********************************Activity Override End***********************************/

    // 返回键 监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(isVideoPreviewShow) {
                MediaPlayerManager.instance().releasePlayerAndView(this);

                runOnUiThread(() -> {
                    videoFileName.setText("");
                    videoPreviewLayout.setVisibility(View.GONE);
                });

                isVideoPreviewShow = false;
            }
        }

        return false;
    }
}
